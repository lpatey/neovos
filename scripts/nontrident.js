
var WebBrowser = {

	init : function() {
		
	},

	itemContainer : {

		init : function() {

			this.ondragover = function( e ) {
				e.preventDefault();
				return true;
			}

			this.ondrop = function( e ) {

				e.preventDefault();

				var context = System.context.local;
				var process = System.process.create( context );
				var pid = "" + process.pid;
				var dt = e.dataTransfer;
				if( !dt ) {
					return;
				}

				var files = [];
				var flags = {};

				var data = dt.getData( 'text/plain' );	

				if( !data ) {
					var data = dt.getData( 'text/x-moz-url' );
				}
		
				if( !data ) {
					var data = dt.getData( 'text/uri-list' );
				}

				if( data.substr( 0, 7 ) == 'file://' ) {
					fileArray = data.split( "\n" );
					var file;
					while( file = fileArray.shift() ) {
						if( flags[file] ) {
							continue;
						}
						flags[ file ] = true;
						file = file.substr( 7 );
						if( file.substr( 0, 9 ) == 'localhost' ) {
							file = file.substr( 9 );
						}
						files.push( decodeURI( file ) );
					}
				}

				if( !files.length ) {
					return;
				}

				$(this).itemContainer( 'process.start', process );

				var dest = $(this).itemContainer( 'data' );
				var query = { 
					files : files,
					inode : '' + dest.inode,
					path : '',
					pid : pid,
					dest : $(dest).item( 'data.path' )
				};

				$(this).tryJSON( context.protocol + 'Action_Fs_Upload', query, System.process.finish );
		
				$(context).setTimeout( System.process.refresh, 500 );

				return true;

			}
		
		}

	}

}

