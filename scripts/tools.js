
var Tools = {

	setSelectionRange : function( input, start, end ) {
	
		if (input.setSelectionRange) {
			input.focus();
			input.setSelectionRange(start, end);
		}
		else if (input.createTextRange) {
			var range = input.createTextRange();
			range.collapse(true);
			range.moveEnd('character', end);
			range.moveStart('character', start);
			range.select();
		} 
	},

	getData : function() {

		var o = {};
		$('input', this).each( function() { 
			if( !$(this).is( '[disabled]' ) 
				&& (!$(this).is( ':checkbox' ) || $(this).is( ':checked' )) ) {
				
				o[ $(this).attr( 'name' ) ] = $(this).attr( 'value' );
			}
		} );
		return o;
	},
	
	strings : {
		
		removeAccents : function(s) {
		 	var r=s.toLowerCase();
            r = r.replace(new RegExp("\\s", 'g'),"");
            r = r.replace(new RegExp("[àáâãäå]", 'g'),"a");
            r = r.replace(new RegExp("æ", 'g'),"ae");
            r = r.replace(new RegExp("ç", 'g'),"c");
            r = r.replace(new RegExp("[èéêë]", 'g'),"e");
            r = r.replace(new RegExp("[ìíîï]", 'g'),"i");
            r = r.replace(new RegExp("ñ", 'g'),"n");
            r = r.replace(new RegExp("[òóôõö]", 'g'),"o");
            r = r.replace(new RegExp("œ", 'g'),"oe");
            r = r.replace(new RegExp("[ùúûü]", 'g'),"u");
            r = r.replace(new RegExp("[ýÿ]", 'g'),"y");
            return r;
		}
	
	},

	sync : 0,

	setTime : function( time ) {
		this.sync = time - new Date().getTime();
	},

	getTime : function() {
		return new Date().getTime() + this.sync;
	}

}
