
var System = {

	window : {
		container : 'body > .WindowContainer'
	},

	context : {
		local : {
			refreshing : false,
			protocol : 'local:',
			selector : 'local',
			request : false,
			process : 0
		},
		remote : {
			refreshing : false,
			protocol : '',
			selector : 'remote',
			request : false,
			process : 0
		}
	},

	i18n : {

		data : {},

		get : function()  {
			var args = new Array();			
			for( var i=0; i<arguments.length; i++ ) {
				args.push( arguments[i] );
			}
			var key = args.shift();
			if( !key ) {
				return key;
			}
			var paths = key.split( '.' );
			var target = System.i18n.data;
			while( paths.length ) {
				var path = paths.shift();
				if( typeof target[ path ] == 'undefined' ) {
					return key;
				}
				target = target[ path ];
			}
			if( typeof target == 'object' ) {
				return key;
			}
			return target.replace( /%([0-9]+)/g, function( data ) {
				return args[parseInt(data[1])];
			} );
		},

		merge : function( o, args ) {
			$(o).addClass( 'I18n' );
			$(o).data( 'i18n', args );
			$(o).html( System.i18n.get.apply( this, args ) );
		},

		init : function() {
			$('.I18n').each( function() {
				if( $(this).is( 'input' ) ) {
					var key = $(this).attr( 'value' );
					$(this).attr( 'value', System.i18n.get( key ) );
				}
				else {
					var key = $(this).html();
					$(this).html( System.i18n.get( key ) );
				}
				$(this).data( 'i18n', new Array( key ) );
			} );
		},

		getHTML : function() {
			var value = System.i18n.get.apply( this, arguments );
			var html = $('<span class="I18n"></span>' );
			html.html( value );
			html.data( 'i18n', arguments );
			return html;
		},

		refresh : function() {
			$('.I18n').each( function() {
				$(this).html( System.i18n.get.apply( this, $(this).data( 'i18n' ) ) );
			} );
		}

	},

	init : function() {
		System.process.window = $(System.window.container).processWindow( 'create' );
		$(System.context.remote).each( System.synchronization.refresh );
	},
	
	clipboard : {
	
		COPY : 1,
		CUT : 2,
	
		action : null,
		data : {},
		
		set : function( action, data ) {
			this.action = action;
			this.data = data;
		},
		
		get : function() {
			return {
				action : this.action,
				data : this.data
			};
		}
	},

	process : {

		window : null,
		processes : {},

		create : function( context ) {
			var process = {
				pid : '' + Tools.getTime(), 
				context : context
			};
			this.processes[ process.pid ] = process;
			return process;
		},

		refresh : function() {

			var o = this
			if( o.refreshing ) {
				o.request = true;
				return;
			}
			o.refreshing = true;
			o.request = false;
			$(o).tryJSON( o.protocol + 'Action_Ps_GetProcesses', System.process.handle );

		},

		handle : function( json ) {
			var o = this;
			o.refreshing = false;
			if( json.processes.length || o.request ) {
				$(o).setTimeout( System.process.refresh, 500 );
			}

			if( json.time > this.process ) {
				$(System.process.window).processWindow( 'refresh', json.processes, o );
				this.process = json.time;
			}
		},

		finish : function( json ) {
			var process = System.process.processes[ json.pid ];
			if( typeof process == 'undefined' ) {
				return;
			}
			process.time = json.time;
			$(process).triggerHandler( 'die' );
			delete System.process.processes[ json.pid ];
		}

	},

	synchronization : {

		refresh : function() {
			var inodes = {};
			var list = {
				contents : []
			};
			var o = this;
			
			$('.ItemContainer[inode][context=' + o.selector + ']:not(.Loading)' ).each( function() {
				var inode = $(this).attr( 'inode' );
				var data = $(this).itemContainer( 'data' );
				if( !data.loaded ) {
					return;
				}
				if( !inodes[ inode ] ) {
					list.contents.push( $(this).itemContainer('query') );
					inodes[ inode ] = true;
				}
			} );

			$(o).tryJSON( o.protocol + 'Action_System_GetSynchronisationData', list, 
				 System.synchronization.handle );

		},
		
		handlers : {
		
			processes : function( data ) {
				System.process.handle.call( this, data );
			},
			
			contents : function( data ) {
				// Update paths
				for( var inode in data ) {
					$({ inode : inode }).item( 'data.children', data[ inode ] );
				}
			},

			time : function( time ) {
				Tools.setTime( time );
			},
			
			user : function( user ) {
				Cache.user = user;
			},
			
			trash : function( data ) {
				var empty = Desktop.trash.item.empty;
				Desktop.trash.item.empty = data.empty;
				if( empty != data.empty ) {
					$('.Item[inode=' + Desktop.trash.item.inode + ']').item( 'update', { 
						empty : data.empty,
						inode : Desktop.trash.item.inode
					} );
					$('.ItemContainer[inode=' + Desktop.trash.item.inode + ']').itemContainer( 'trigger', 'refresh' );
				}
			}
		},

		handle : function( data ) {
			var o = this;
			$(o).setTimeout( System.synchronization.refresh, 2000 );
			
			for( var i in data ) {
				System.synchronization.handlers[ i ].call( this, data[i] );
			}
		}
	}

}


