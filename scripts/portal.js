
(function($) {

	var LogInForm = {
	
		init : function() {
		
			$('input[type=text][name=password]', this).focus( function() {
				var input = $('<input type="password" name="password" />');
				$(this).replaceWith( input );
				input.focus();
			} );
			
			$('input[name=login]', this).focus( function() {
				$(this).attr( 'value', '' );
				$(this).unbind( 'focus' );
			} );
		}
	}
	
	var SignInForm = {
	
		init : function() {
		
			var inputImg = $('input[name=login]', this).closest( 'dd' ).find( 'img' );
			var signIn = $('input[type=submit]', this);
		
			$('input[name=login]', this).bind( 'change, mousedown, focus, keydown, keyup', function() {
				var value = $(this).attr( 'value' );
				value = value.replace( /[^A-Za-z0-9\-._]/g, '' );
				$(this).attr( 'value', value );
				if( value != inputImg.data( 'login' ) ) {
					inputImg.hide();
				}
			} );
			
			
			$('input[name=login]', this).blur( function() {
				var login = $(this).attr( 'value' );
				
				if( !login ) {
					inputImg.hide();
					return;
				}
				
				if( login.length < 4 ) {
					inputImg.data( 'login', login );
					inputImg.attr( 'title', 'Login too short' );
					inputImg.attr( 'src', '/portal/images/portal/invalid.png' );
					inputImg.show();
					return;
				}
				
				if( login.length > 50 ) {
					inputImg.data( 'login', login );
					inputImg.attr( 'title', 'Login too long' );
					inputImg.attr( 'src', '/portal/images/portal/invalid.png' );
					inputImg.show();
					return;
				}
				
				$.post( '/Action_Portal_CheckLogin', {
					login : login
				}, function( json ) {
					var valid = json.valid ? 'valid' : 'invalid';
					inputImg.data( 'login', login );
					inputImg.attr( 'title', json.valid ? 'Login available' : 'Login already used' );
					inputImg.attr( 'src', '/portal/images/portal/' + valid + '.png' );
					inputImg.show();
				}, 'json' );
			} );
			
			$('input[name=email]', this).bind( 'change, mousedown, focus, keydown, keyup', function() {
				var value = $(this).attr( 'value' );
				var img = $(this).closest( 'dd' ).find( 'img' );
				if( value != img.data( 'login' ) ) {
					img.hide();
				}
			} );
			
			$('input[name=email]', this).blur( function() {
				var value = $(this).attr( 'value' );
				var img = $(this).closest( 'dd' ).find( 'img' );
				
				if( !value ) {
					img.hide();
					return;
				}
				
				
				var valid = value.match( /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/ );
				img.attr( 'title', valid ? 'Valid email' : 'Invalid email' );
				img.data( 'value', value );
				img.attr( 'src', '/portal/images/portal/' + (valid ? 'valid' : 'invalid' )+ '.png' );
				img.show();
			} );
		}
	
	}
	
	$(function() {
		$( '.LogInForm' ).each( LogInForm.init );
	} );
	
	$(function() {
		$( '.SignInForm' ).each( SignInForm.init );
	} );
	
	$(function() {
		$('.Button').click( function() {
			$(this).closest( 'form' ).submit();
		} );
	} );

})(jQuery);
