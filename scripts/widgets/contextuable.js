(function($) {

	var $this = $.neovos.plugin.add( 'contextuable', {
	
		/**
		* @type { action : Function, label : String } list
		*/
		choices : []
	},
	{
		init : function( options ) {

			$(this).bind( 'contextmenu', function( e ) {

				var choices = options.choices;
				if( $.isFunction( options.choices ) ) {
					choices = options.choices.call( this, e );
				}
				
				if( choices.length > 0 ) {
					var menu = $(this).contextuable( 'choices', choices );
					menu.css( {
						left : e.clientX + 'px',
						top : e.clientY + 'px'
					} );
					menu.mousedown( function() {
						return false;
					} );
					return false;
				}
				
				return true;
			} );
		},
		
		/**
		* @type setter
		* @return menu
		*/
		choices : function( choices ) {
		
			var o = this;
			var separator = true;
			var menu = $( '<ol class="ContextMenu"></ol>' );
			for( var i=0; i<choices.length; i++ ) {
				if( choices[i].action == undefined ) {
					if( !separator ) {
						separator = true;
						menu.append( '<li class="Separator"></li>' );
					}
					continue;
				}
				
				var choice = $('<li>' + System.i18n.get(choices[i].label) + '</li>');
				choice.get(0).action = choices[i].action;
				choice.get(0).menu = menu;
				choice.hover( function() {
					$(this).addClass( 'Hover' );
				}, function() {
					$(this).removeClass( 'Hover' );
				} );
				choice.mouseup( function() { 
					this.menu.remove();
					this.action.call( o ); 
				} );
				menu.append( choice );
				separator = false;
			}
			if( separator ) {
				menu.find( '.Separator:last' ).remove();
			}
			$('body').append( menu );
			
			return menu;
		}

	} );

	$(window).mousedown( function() {
		$('.ContextMenu' ).remove();
	} );

} )(jQuery);
