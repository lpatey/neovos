(function($) {

	var $this = $.neovos.plugin.add( 'navigation', {
		items : '> div',
		select : new Function(),
		unselect : new Function()
	},
	{
		init : function( options ) {
			$(this).keydown( $this.keyDown );
			return this;
		},

		keyDown : function( e ) {
			if( e.keyCode == $.ui.keyCode.UP ) {
				$(this).navigation( 'trigger', 'keydown' );
				e.stopImmediatePropagation();
				$(this).navigation( 'up', e );
				$(this).navigation( 'refresh' );
				return false;
			}
			if( e.keyCode == $.ui.keyCode.DOWN ) {
				$(this).navigation( 'trigger', 'keydown' );
				e.stopImmediatePropagation();
				$(this).navigation( 'down', e );
				$(this).navigation( 'refresh' );
				return false;
			}
			if( e.keyCode == $.ui.keyCode.LEFT ) {
				$(this).navigation( 'trigger', 'keydown' );
				e.stopImmediatePropagation();
				$(this).navigation( 'left', e );
				$(this).navigation( 'refresh' );
				return false;
			}
			if( e.keyCode == $.ui.keyCode.RIGHT ) {
				$(this).navigation( 'trigger', 'keydown' );
				e.stopImmediatePropagation();
				$(this).navigation( 'right', e );
				$(this).navigation( 'refresh' );
				return false;
			}
		},

		down : function() {
			$(this).each( function() {
				var options = $(this).navigation( 'options' );
				var found = false;
				var selected = $(options.items + '.Selected', this);
				if( !selected.length ) {
					$(options.items + ':first', this).each( options.select );
					return;
				}
				var offsetLeft = selected.attr( 'offsetLeft' );
				selected.nextAll().each( function() {
					if( $(this).attr( 'offsetLeft' ) == offsetLeft ) {
						$(this).each( options.select );
						found = true;
						return false;
					}
				} );
				selected.each( options.unselect );
				if( !found ) {
					$(options.items, this).each( function() {
						if( $(this).attr( 'offsetLeft' ) == offsetLeft ) {
							$(this).each( options.select );
							return false;
						}
					} );
				}
			} );
			return this;
		},

		up : function() {
			$(this).each( function() {
				var options = $(this).navigation( 'options' );
				var found = false;
				var selected = $(options.items + '.Selected', this);
				if( !selected.length ) {
					$(options.items + ':last', this).each( options.select );
					return;
				}
				var offsetLeft = selected.attr( 'offsetLeft' );
				selected.prevAll().each( function() {
					if( $(this).attr( 'offsetLeft' ) == offsetLeft ) {
						$(this).each( options.select );
						found = true;
						return false;
					}
				} );
				selected.each( options.unselect );
				if( !found ) {
					var last = $(options.items + ':last', this);
					if( $(last).attr( 'offsetLeft' ) == offsetLeft ) {
						$(last).each( options.select );
						return false;
					}
					last.prevAll().each( function() {
						if( $(this).attr( 'offsetLeft' ) == offsetLeft ) {
							$(this).each( options.select );
							return false;
						}
					} );
				}
			} );
			return this;
		},

		right : function() {
			$(this).each( function() {
				var options = $(this).navigation( 'options' );
				var found = false;
				var selected = $(options.items + '.Selected', this);
				if( !selected.length ) {
					$(options.items + ':first', this).each( options.select );
					return;
				}
				var offsetTop = selected.attr( 'offsetTop' );
				selected.nextAll().each( function() {
					if( $(this).attr( 'offsetTop' ) == offsetTop ) {
						$(this).each( options.select );
						found = true;
						return false;
					}
				} );
				selected.each( options.unselect );
				if( !found ) {
					$(options.items, this).each( function() {
						if( $(this).attr( 'offsetTop' ) == offsetTop ) {
							$(this).each( options.select );
							return false;
						}
					} );
				}
			} );
		},

		left : function() {
			$(this).each( function() {
				var options = $(this).navigation( 'options' );
				var found = false;
				var selected = $(options.items + '.Selected', this);
				if( !selected.length ) {
					$(options.items + ':last', this).each( options.select );
					return;
				}
				var offsetTop = selected.attr( 'offsetTop' );
				selected.prevAll().each( function() {
					if( $(this).attr( 'offsetTop' ) == offsetTop ) {
						$(this).each( options.select );
						found = true;
						return false;
					}
				} );
				selected.each( options.unselect );
				if( !found ) {
					var last = $(options.items + ':last', this);
					if( $(last).attr( 'offsetTop' ) == offsetTop ) {
						$(last).each( options.select );
						return false;
					}
					last.prevAll().each( function() {
						if( $(this).attr( 'offsetTop' ) == offsetTop ) {
							$(this).each( options.select );
							return false;
						}
					} );
				}
			} );
		},

		refresh : function( flag ) {
			var options = $(this).navigation( 'options' );
			var selected = $(options.items + '.Focus', this);
			if( !selected.length ) {
				return;
			}
			var scrollable = selected.closest( '.Scrollable' );
			if( !scrollable.length ) {
				return;
			}

			scrollable = scrollable.get(0);
			selected = selected.get(0);
			var pHeight = scrollable.clientHeight;
			var pTop = scrollable.scrollTop;
			var sHeight = selected.clientHeight;
			var sTop = selected.offsetTop;

			if( sTop < pTop ) {
				scrollable.scrollTop = sTop;
			}
			else if( (sTop + sHeight) > (pTop + pHeight) ) {
				scrollable.scrollTop = sTop + sHeight - pHeight;
			}
		},

		destroy : function() {
			$(this).unbind( $this.keyDown );
			$(this).unbind( 'neovos-navigation' );
			$(this).removeClass( 'Navigation' );
			return this;
		}

	});

} )(jQuery);
