
(function($) {

	var $this = $.neovos.plugin.add( 'processWindow', {
		window : {
			height : 'auto',
			closable : false,
			resizable : false
		},
		templates : {
			progressGroup : 'body > .Template > .Progress.Group'
		}
	},
	{

		/**
		* @context ProcessWindow
		* @return ProcessWindow
		*/
		init : function( options ) {
			$(this).addClass( 'Process' );
		},

		/**
		* @context WindowContainer
		* @return ProcessWindow
		*/
		create : function( options ) {
			options.window.title = System.i18n.get( 'processWindow.processes' );
			var window =  $(this).window( 'create', options.window );
			$(window).processWindow( options );

			return window;
		},

		/**
		* @context ProcessWindow
		* @return ProcessWindow
		*/
		process : function( process ) {
			this.each( function() {
				var options = $(this).processWindow( 'options' );
				var progressGroup = $(options.templates.progressGroup).clone();
				$(this).window( 'content' ).append( progressGroup );
				progressGroup.attr( 'pid', process.pid );
				progressGroup.attr( 'context', process.context.selector );
				progressGroup.slideDown( 'fast' );
				$(this).window( 'show' );
				$(this).window( 'target', true );
			} );
			return this;
		},

		/**
		* @context ProcessWindow
		* @return ProcessWindow
		*/
		refresh : function( processes, context ) {
			this.each( function() {
				var processWindow = this;
				$('.Window .Progress.Group[context=' + context.selector + ']' ).addClass( 'Flagged' );

				$(processes).each( function() {

					var progressGroup = $('.Progress.Group[pid=' + this.pid + ']');
			
					if( !progressGroup.length ) {
						var progressGroup = $('body > .Template > .Progress.Group').clone();
						$(processWindow).processWindow( 'process', { pid : this.pid, context : context } );
						/*$(processWindow).window( 'content' ).append( progressGroup );
						progressGroup.attr( 'pid', this.pid );
						progressGroup.attr( 'context', context.selector );
						progressGroup.slideDown( 'fast' );*/
					}
					progressGroup.removeClass( 'Flagged' );
					if( this.file ) {
						progressGroup.find( 'h5' ).html( System.i18n.get( 'processWindow.fileMessage', '<span class="Path">' + this.file + '</span>' ) );
					}
					else if( this.message ) {
						progressGroup.find( 'h5' ).html( this.message );
					}
					if( this.progress ) {
						progressGroup.find( '.Loading' ).animate( {
							 width : Math.min(100, (this.progress*100/this.totalSize) ) + '%'
						} );
					}
				} );

				$('.Window .Progress.Group:not(.Flagged)[context=' + context.selector + ']').each( function() {
					$(this).closest( '.Window,.SubWindow' ).show();
					//$(this).closest( '.Window' ).window( 'target', true );
				} );

				$('.Window .Progress.Group.Flagged[context=' + context.selector + ']' ).slideUp( 'fast', function() {
			
					var window = $(this).closest( '.Window,.SubWindow' );
					var pid = $(this).attr( 'pid' );
					$('[pid=' + pid + ']').removeAttr( 'pid' );
					$(this).remove();
					if( !$('.Progress.Group',window).length ) {
						$(window).window( 'hide' );
						if( window.hasClass( '.Window' ) ) {
							setTimeout( $.neovos.window.selectAnyWindow, 0 );
						}
					}
					
			
				} );
			} );
			return this;
		}

	} );

})(jQuery);
