
(function($) {

	var $this = $.neovos.plugin.add( 'publishWindow', {
		window : {
			width : 350,
			height : 'auto',
			resizable : false,
			reduceable : false,
			maximizable : false,
			title : 'publishWindow.title'
		},
		
		data : null,
		share : null,
		template : '.Template .PublishWindowContent'
	},
	{
		init : function( options ) {

			var o = this;
			if( !options.data ) {
				options.data = Cache.data[ options.share.dataInode ];
			}
			
			var item = $(options.data).item( 'create', { 
				selectable : false,
				draggable : false
			} );
			$('.ItemContainer', this).append( item );
			
			$('form', this).bind( 'submit', function() {
				return false;
			} );
			
			$(this).publishWindow( 'buttons.init' );
			
			$(this).publishWindow( 'shareName', options.share ? options.share.name : options.data.name );
			
			if( options.share ) {
				$(this).publishWindow( 'password', options.share.password );
				$(this).publishWindow( 'subWindow', options.share );
			}

		},

		/**
		* @context WindowContainer
		* @return PublishWindow
		*/
		create : function( options ) {

			options.window.content = $( options.template ).clone();
			var window = $(this).window( 'create', options.window );
			return $(window).publishWindow( options );
		},
		
		/**
		* @context PublishWindow
		* @type getter|setter
		* @return PublishWindow|string
		*/
		shareName : function( value ) {
			if( typeof value != 'undefined' ) {
				$('input[type=text]', this).attr( 'value', $this.tools.nameToShareName( value ) );
				return this;
			}
			return $('input[type=text]', this).attr( 'value' );
		},
		
		/**
		* @context PublishWindow
		* @type getter|setter|toggler
		* @return PublishWindow|bool
		*/
		cancelable : function( value ) {
			return $(this).toggler( function() {
				$('.Cancel', this).removeAttr( 'disabled' );
			},
			function() {
				$('.Cancel', this).attr( 'disabled', 'disabled' );
			},
			function() {
				return !$('.Cancel', this).is( '[disabled]' );
			}, value );
		},
		
		/**
		* @context PublishWindow
		* @type getter|setter|toggler
		* @return PublishWindow|bool
		*/
		unpublishable : function( value ) {
			return $(this).toggler( function() {
				$('.Unpublish', this).removeAttr( 'disabled' );
			},
			function() {
				$('.Unpublish', this).attr( 'disabled', 'disabled' );
			},
			function() {
				return !$('.Unpublish', this).is( '[disabled]' );
			}, value );
		},
		
		/**
		* @context PublishWindow
		* @type getter|setter
		* @return PublishWindow|string
		*/
		password : function( value ) {
			if( typeof value != 'undefined' ) {
				$('input[type=checkbox]')[ value ? 'attr' : 'removeAttr' ]( 'checked', 'checked' ).trigger( 'change' );
				$('input[type=password]', this).attr( 'value', value ? 'keep-password' : '' );
				return this;
			}
			return $('input[type=password]', this).attr( 'value' );
		},
		
		/**
		* @context PublishWindow
		* @type getter|setter
		* @return PublishWindow|boolean
		*/
		subWindow : function( value ) {
		
			if( typeof value != 'undefined' ) {
				if( !value ) {
					var sub = $(this).window( 'subWindow' );
					$('input', sub).slideUp( 'fast', function() {
						$(this).remove();
						sub.hide();
					} );
				}
				else {
					var sub = $(this).window( 'subWindow' );
					var url = location.href + value.share_id + '/' + $('input[type=text]', this).attr( 'value' );
					var p = $( '<p><a target="_blank" href="' + url + '">' + url + '</a></p>' );
				
					sub.find( '.Content' ).html( p );
					sub.show();
					p.slideDown( 'fast' );
				}
				return this;
			}
			
			return $('.SubWindow', this).is( ':visible' );
		},
		
		buttons : {
		
			/**
			* @context PublishWindow
			*/
			init : function() {
			
				var o = this;
			
				$('input[type=checkbox]', this).bind( 'click, change', function() {
					if( $(this).is( '[checked]' ) ) {
						$('input[type=password]', o).removeAttr( 'disabled' );
					}
					else {
						$('input[type=password]', o).attr( 'disabled', 'disabled' );
					}
				} );
			
				$('.Cancel', this).click( $this.handlers.cancel );
			
				$('.Publish', this).click( $this.handlers.publish );

				$(this).publishWindow( 'unpublishable', $(this).publishWindow( 'option', 'share' ) );
				
				$('.Unpublish', this).click( $this.handlers.unpublish );
			
				$('input[type=text], input[type=password]', this).keydown( function( e ) {
					e.stopPropagation();
				} );
			
				}
		},
		
		handlers : {
		
			cancel : function( e ) {
				var w = $(this).closest( '.Window' );
				w.window( 'close' );
			},
		
			publish : function( e ) {
				var w = $(this).closest( '.Window' );
				var data = $(w).publishWindow( 'option', 'data' );
				var share = $(w).publishWindow( 'option', 'share' );
				var query = Tools.getData.call( $('form', w) );
				var action = 'Action_Fs_Share_Create';
				
				$(w).publishWindow( 'cancelable', false );
				$(w).publishWindow( 'unpublishable', true );
				
				query.path = $(data).item( 'data.path' );
				
				if( share ) {
					action = 'Action_Fs_Share_Modify';
					query.share_id = share.share_id;
				}
				
				$({}).tryJSON( action, query, function( json ) {
					data = Cache.data[ data.inode ];
					if( share ) {
						share.name = query.name;
						share.password = 'keep-password' ? share.password : (query.password ? 'yes' : '');
						$(share).item( 'data.trigger', 'change', {
							name : share.name,
							password : share.password
						} );
					}
					else {
						if( !data.attributes ) {
							data.attributes = [];
						}
						if( !data.attributes.shares ) {
							data.attributes.shares = {};
						}
						data.attributes.shares[ json.share_id ] = json;
						json.dataInode = data.inode;
						if( data.directory ) {
							data.mime = 'application/x-share';
							data.time = Tools.getTime();
							$(data).item( 'data.trigger', 'change', {
								mime : 'application/x-share',
								inode : data.inode,
								time : data.time,
								loaded : true
							} );
						}
						$(w).publishWindow( 'option', 'share', json );
					}
					w.publishWindow( 'subWindow', json );
				} );
			},
			
			unpublish : function() {

				var w = $(this).closest( '.Window' );
				var data = w.publishWindow( 'option', 'data' );
				var share = w.publishWindow( 'option', 'share' );
				var share_id = share.share_id;
				
				var message = System.i18n.get( 'publishWindow.messages.areYouSureYouWantToUnpublish', share.name );
				if( !confirm( message ) ) {
					return;
				}
				
				w.publishWindow( 'cancelable', false );
				
				var query = {
					share_id : share_id
				};
				
				$({}).tryJSON( 'Action_Fs_Share_Delete', query, function() {
					data = Cache.data[ data.inode ];
					delete data.attributes.shares[ share_id ];
					data.time = Tools.getTime();
					var modifs = {
						inode : data.inode,
						time : data.time,
						loaded : true
					};
					var shared = false;
					for( var i in data.attributes.shares ) {
						shared = true;
					}
					if( !shared ) {
						data.mime = 'application/x-directory';
						modifs.mime = data.mime;
					}
					$(data).item( 'data.trigger', 'change', modifs );
				} );
				
				w.window( 'close' );
			}
		},
		
		tools : {
		
			nameToShareName : function( name ) {
				var n = '';
				name = Tools.strings.removeAccents(name).replace( /[^\w\s\.,_\-\/]/g, '' );
				var stop = false;
				name.replace( /(^|\s|\.|_|,|-|\/)([^\s\.,_\-\/]*)/g, function( a, sep, ch ) {
					if( !stop && (!n.length || (n.length + ch.length <= 22 )) ) {
						if( ch.replace(/\s/g, '') ) {
							n += '.' + ch;
						}
					}
					else {
						stop = true;
					}
				} );
				return n.substr( 1, 22 );
			}
		}
		

	} );

})(jQuery);
