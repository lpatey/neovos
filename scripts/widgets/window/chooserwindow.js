
(function($) {

	var $this = $.neovos.plugin.add( 'chooserWindow', {
		window : {
			width : 250,
			height : 'auto',
			resizable : false,
			reduceable : false,
			maximizable : false
		},
		options : [],
		remember : {
			checked : false,
			message : 'Remember my choice'
		},
		message : 'Choose one of the following options',
		templates : {
			content : '.Template > .ChooserWindowContent',
			option : '.Template > .ChooserWindowOption'
		}
	},
	{
		init : function( options ) {

			var o = this;
			$(this).chooserWindow( 'message', options.message );
			

			$(options.options).each( function() {
				$(o).chooserWindow( 'add', this );
			} );

			var remember = options.remember;
			var checked = remember.checked;
			remember.checked = !checked;
			$(this).chooserWindow( 'remember.message', remember.message );
			$(this).chooserWindow( 'remember', checked );
			
		},

		/**
		* @context WindowContainer
		* @return chooserWindow
		*/
		create : function( options ) {

			options.window.content = $( options.templates.content ).clone();
			var window = $(this).window( 'create', options.window );
			$('.Feedback', window).remove();
			return $(window).chooserWindow( options );
		},

		/**
		* @context chooserWindow
		* @return chooserWindow
		*/
		add : function( option ) {
			var options = $(this).chooserWindow( 'options' );
			var object = $(options.templates.option).clone();
			object.find( 'img' ).attr( 'src', 'images/chooserwindow/' + option.icon + '.png' );
			object.find( 'h4' ).html( System.i18n.get( option.name ) );
			object.find( 'p' ).html( System.i18n.get( option.description ) );
			if( option.action ) {
				object.bind( 'click', option.action );
			}
			$(this).chooserWindow( 'list' ).append( object );
		},

		/**
		* @context chooserWindow
		* @type getter|setter
		* @return chooserWindow|chooserWindow.message
		*/
		message : function( value ) {
			if( typeof value != 'undefined' ) {
				$(this).chooserWindow( 'options' ).message = value;
				$(this).window( 'content' ).find( '> .Message' ).html( value );
			}
			return $(this).chooserWindow( 'options' ).message;
		},

		/**
		* @context chooserWindow
		* @type getter
		* @return chooserWindow.list
		*/
		list : function() {
			return $(this).window( 'content' ).find( '> ul' );
		},

		remember : {

			/**
			* @context chooserWindow
			* @type getter|setter|toggler
			* @return chooserWindow|boolean
			*/
			init : function( value ) {
				return $(this).toggler(
					function() {
						$(this).chooserWindow( 'options' ).remember.checked = true;
						$('input[type=checkbox]', this).attr( 'checked', 'checked' );
					},
					function() {
						$(this).chooserWindow( 'options' ).remember.checked = false;
						$('input[type=checkbox]', this).removeAttr( 'checked' );
					},
					function() {
						return $(this).chooserWindow( 'options' ).remember.checked;
					},
					value );
			},

			message : function( value ) {
				if( typeof value != 'undefined' ) {
					if( !value ) {
						$('.Remember', this).remove();
					}
					else {
						var remember = $(document.createElement('label'));
						remember.append( '<input type="checkbox" />' );
						remember.append( value );
						$(this).window( 'content' ).append( remember );
					}
				}
				return $(this).chooserWindow( 'options' ).remember.message;
			}
		}

	} );

})(jQuery);
