
(function($) {

	var $this = $.neovos.plugin.add( 'window', {
		offset : {
			left : 30,
			top : 40
		},
		width : 610,
		height : 325,
		resizable : true,
		draggable : true,
		closable : true,
		reduceable : true,
		maximizable : true,
		template : '.Template > .Window',
		preview : '<div class="PreviewItem"><h2></h2></div>',
		title : 'Untitled',
		content : '<div></div>'
		
	},
	{
		init : function( options ) {

			$(this).droppable( {
				greedy : true,
				hoverClass : 'AcceptingItem'
			} );

			$(this).window( 'offset', options.offset );
			$(this).css( 'width', options.width + (typeof options.width == 'string' ? '' : 'px') );
			$(this).css( 'height',  options.height + (typeof options.height == 'string' ? '' : 'px') );

			$(this).mousedown( $this.handlers.mouseDown );

			$(this).window( 'draggable', options.draggable );
			$(this).window( 'resizable', options.resizable );
			$(this).window( 'reduceable', options.reduceable );
			$(this).window( 'closable', options.closable );
			$(this).window( 'maximizable', options.maximizable );

			$(this).click( function( e ) { e.stopPropagation(); } );

			$(this).window( 'preview', options.preview );

			$(this).window( 'title', options.title );
			$(this).window( 'content', options.content );

			$(this).window( 'trigger', 'init' );
	
			return this;
		},

		/**
		* @context : Window
		* @type getter|setter|toggler
		* @return : Window|boolean
		*/
		maximizable : function( value ) {
			return $(this).window( 'titleBar.tools.able', 'Maximize', 'maximizable', value );
		},

		/**
		* @context : Window
		* @type getter|setter|toggler
		* @return : Window|boolean
		*/
		closable : function( value ) {
			return $(this).window( 'titleBar.tools.able', 'Close', 'closable', value );
		},

		/**
		* @context : Window
		* @type getter|setter|toggler
		* @return : Window|boolean
		*/
		reduceable : function( value ) {
			return $(this).window( 'titleBar.tools.able', 'Reduce', 'reduceable', value );
		},

		/**
		* @internal
		**/
		titleBar : {

			tools : {

				able : function( selector, able, value ) {
					return $(this).toggler(
						function() {
							var button = $('> .TitleBar > .Tools > .' + selector, this);
							button.show().bind( 'click', $this.titleBar.tools[ 'trigger' + selector ] );
						},
						function() {
							var button = $('> .TitleBar > .Tools > .' + selector, this);
							button.hide().unbind( 'click', $this.titleBar.tools[ 'trigger' + selector ] );
						},
						function() {
							var button = $('> .TitleBar > .Tools > .' + selector, this);
							return button.css( 'display' ) != 'none';
						},
						value );
				},

				triggerReduce : function() {
					$(this).closest( '.Window' ).window( 'reduce', 'toggle' );
				},

				triggerMaximize : function() {
					$(this).closest( '.Window' ).window( 'maximize', 'toggle' );
				},

				triggerClose : function() {
					$(this).closest( '.Window' ).window( 'close' );
				}
			}
		},

		subWindow : {

			/**
			* @context : Window
			* @type getter
			* @return : Window.SubWindow
			*/
			init : function() {
				return $('.SubWindow', this );
			},

			/**
			* @context : Window
			* @type setter
			* @return : Window
			*/
			process : function( process ) {
				if( typeof process != 'undefined' ) {
					var subWindow = $(this).window( 'subWindow' );
					var progressGroup = $('body > .Template > .Progress.Group').clone();
					progressGroup.attr( 'pid', process.pid );
					progressGroup.attr( 'context', process.context.selector );
					subWindow.addClass( 'Process' );
					$('> .Content', subWindow).append( progressGroup );
					$(process).bind( 'die', function() {
						progressGroup.slideUp( 'fast', function() {
							$(this).remove();
							if( subWindow.find( '.Progress.Group' ).length == 0 ) {
								subWindow.hide();
							}
						} );
					} );
					subWindow.show();
					progressGroup.slideDown( 'fast' );
				}
				return this;
			}

		},

		/**
		* @context Window
		* @return Window
		*/
		center : function() {
			this.each( function() {
				$(this).css( {
					left : (($(window).width() - $(this).width())/2)+'px',
					top : (($(window).height() - $(this).height())/2)+'px'
				} );
			} );
			return this;
		},

		/**
		* @context Window
		* @type getter|setter
		* @return Window|Window.content
		*/
		content : function( value ) {
			if( typeof value != 'undefined' ) {
				value = $(value);
				value.addClass( 'Content' );
				$('> .Content', this).replaceWith( value );
				return this;
			}
			return $('> .Content', this);
		},

		/**
		* @context Window
		* @type setter
		* @return Window
		*/
		offset : function( value ) {
			$(this).each( function() {
				var defaults = $.fn.window.defaults;
				var nextOffset = { 
					left : (value.left%($(window).width()-defaults.width-60))+30,
					top : (value.top%($(window).height()-defaults.height-80))+40
				}
				$(this).css( 'left', value.left + 'px' );
				$(this).css( 'top', value.top + 'px' );
				$.fn.window.defaults.offset = nextOffset;
			} );
			return this;
		},
	
		/**
		* @context WindowContainer
		* @return Window
		*/
		create : function( options ) {
			var window = $(options.template).clone().hide();
			$(this).append( window );
			$(window).window( options );
			$(this).window( 'trigger', 'create', window );
			return window;
		},


		/**
		* @context Window
		* @type getter|setter|toggler
		* @return Window|boolean
		*/
		draggable : function( value ) {
			return $(this).toggler(
				function() {
					$(this).draggable( {
						handle : '> .Shadow, > .TitleBar',
						zIndex : 1000,
						start : function() {
							window.focus();
							if( $(this).window( 'maximize' ) ) {
								return false;
							}
							$('.SubWindow:has(.Content *)', this).andSelf().each( function() {
								$(this).data( 'neovos-window-draggable-height', $(this).css( 'height' ) );
								$(this).css( 'height', $(this).height() + 'px' );
							} );
							$('> .Content, .SubWindow > .Content', this).hide();	
						},
						stop : function() {
							$('> .Content, .SubWindow > .Content', this).show();
							$('.SubWindow:has(.Content *)', this).andSelf().each( function() {
								$(this).css( 'height', $(this).data( 'neovos-window-draggable-height' ) );
							} );
						}
					} );
				},
				function() {
					$(this).draggable( 'destroy' );
				},
				function() {
					return $(this).hasClass( 'ui-draggable' );
				},
				value );
		},

		/**
		* @context Window
		* @type getter|setter|toggler
		* @return Window|boolean
		*/
		resizable : function( value ) {
			return $(this).toggler(
				function() {
					$(this).resizable( {
						handles : 'e, s, se',
						start : function() {
							window.focus();
							if( $(this).window( 'maximize' ) ) {
								return false;
							}
							$('> .Content, .SubWindow > .Content', this).hide();
						},
						stop : function() {
							$('> .Content, .SubWindow > .Content', this).show();
						}
					} );
				},
				function() {
					$(this).resizable( 'destroy' );
				},
				function() {
					return $(this).hasClass( 'ui-resizable' );
				},
				value );			
		},

		/**
		* @context Window
		* @type getter|setter|toggler
		* @return Window|boolean
		*/
		target : function( value ) {
			return $(this).toggler(
				function() {
					$(this).window( 'select' );
				},
				function() {
					$(this).window( 'unselect' );
				},
				function() {
					return $(this).hasClass( 'Selected' );	
				},
				value );	
		},

		/**
		* @context Window
		* @type getter|setter
		* @return Window|string
		*/
		title : function( value ) {
			var title = $('> .TitleBar > .Title .Text', this);
			if( typeof value != 'undefined' ) {
				var value = System.i18n.get( value );
				title.html( value );
				$('h2', $(this).window( 'preview' ) ).html( value );
				return this;
			}
			return title.html();
		},

		handlers : {

			/**
			* @context Window
			* @return boolean
			*/
			mouseDown : function( e ) {
				window.focus();
				$(this).window( 'target', true );
				e.stopPropagation();
				$(window).trigger( 'mousedown' );
				return true;
			}
		},
	
		/**
		* @context Window
		* @type getter|setter|toggler
		* @return Window|boolean
		*/
		reduce : function( value ) {
			return $(this).toggler(
				function() {
					var data = {
						height : $(this).css( 'height' )
					}
					$(this).data( 'neovos.window.reduce', data );

					$(this).css( 'height', '0px' );
					$('> .Content', this).hide();
					$(this).addClass( 'Reduced' );
					$(this).window( 'trigger', 'reduce' );
				},
				function() {
					var data = $(this).data( 'neovos.window.reduce' );
					$(this).css( 'height', data.height );
					$('> .Content', this).show();
					$(this).removeClass( 'Reduced' );
					$(this).window( 'trigger', 'unreduce' );
				},
				function() {
					return $(this).hasClass( 'Reduced' );
				},
				value );
		},
	
		/**
		* @context Window
		* @type getter|setter|toggler
		* @return Window|boolean
		*/
		maximize : function( value ) {
			return $(this).toggler(
				function() {
					var desktop = $('body > .ItemContainer');
					var data = {
						offset : {
							left : $(this).css( 'left' ),
							top : $(this).css( 'top' )
						},
						width : $(this).css( 'width' ),
						height : $(this).css( 'height' )
					}
					$(this).data( 'neovos.window.maximize', data );
					$(this).css( 'left', '10px' );
					$(this).css( 'top', '' );
					$(this).css( 'width', $(desktop).width() -20 );
					$(this).css( 'height', $(desktop).height() -30 );
					$(this).addClass( 'Maximized' );
					$(this).window( 'trigger', 'maximize' );
				},
				function() {
					var data = $(this).data( 'neovos.window.maximize' );
					$(this).css( 'left', data.offset.left );
					$(this).css( 'top', data.offset.top );
					$(this).css( 'width', data.width );
					$(this).css( 'height', data.height );
					$(this).removeClass( 'Maximized' );
					$(this).window( 'trigger', 'unmaximize' );
				},
				function() {
					return $(this).hasClass( 'Maximized' );
				},
				value );
		},
	
		/**
		* @context Window
		* @return Window
		*/
		close : function() {
			$(this).window( 'trigger', 'close' );
			$(this).window( 'hide' );
			$(this).remove();
			setTimeout( $this.selectAnyWindow, 0 );
			return this;
		},
		
		selectAnyWindow : function() {
			var any = $('body > .WindowContainer > .Window:visible:last');
			if( any.length ) {
				any.window( 'select' );
			}
			else {
				$('body > .ItemContainer').itemContainer( 'target', true );
			}
		},
		
		/**
		* @context Window
		* @type setter|getter
		* @return Window|Window.Preview
		*/
		preview : function( preview ) {
			if( typeof preview != 'undefined' ) {
				var preview = $(preview);
				var window = this;
				preview.mouseover( function() {
					if( !window.window( 'target' ) ) {
						window.window( 'target', true );
					}
				} );
				$(this).window( 'option', 'preview', preview );
				return this;
			}
			return $(this).window( 'option', 'preview' );
		},


		/**
		* @context Window
		* @return Window
		*/
		select : function() {

			$(this).each( function() {

				$(this).window( 'refresh' );
				$(this).window( 'trigger', 'select' );

				if( $(this).window( 'target' ) ) {
					return;
				}

				$('.Window.Selected').removeClass( 'Selected' );
				$(this).addClass( 'Selected' );
		
				$('.Middle.Column .ItemContainer:not(.Selected)', this).itemContainer( 'select' );

				var zIndex = $(this).css( 'zIndex' );
		
				$('.WindowContainer > .Window').each( function() {
					var localZIndex = $(this).css( 'zIndex' );
					if( localZIndex > zIndex ) {
						$(this).css( 'zIndex', localZIndex-1 );
						$(this).window( 'unselect' );
					}
				} );
		
				$(this).css( 'zIndex', $('.Window').length );
				$(this).window( 'trigger', 'select' );
				$('body > .WindowContainer').append( this );

				$('body > .ItemContainer').itemContainer( 'target', false );

			} );
	
			return this;

		},

		/**
		* @context Window
		* @return Window
		*/
		unselect : function() {
			$(this).removeClass( 'Selected' );
			$(this).window( 'trigger', 'unselect' );
			return this;
		},

		/**
		* @context Window
		* @return Window
		*/
		refresh : function() {
			$(this).window( 'trigger', 'refresh' );
			return this;
		},
		
		/**
		* @context Window
		* @return Window
		*/
		hide : function() {
			$(this).hide();
			$(this).window( 'trigger', 'hide' );
			return this;
		},
		
		/**
		* @context Window
		* @return Window
		*/
		show : function() {
			$(this).show();
			$(this).window( 'trigger', 'show' );
			return this;
		}
		
	} );

} )(jQuery);
