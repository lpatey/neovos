
(function($) {

	var $options = {
		directory : {
			copy : [
				{
					name : 'Replace directory',
					description : 'You will loose destination data',
					icon : 'replace'
				},
				{
					name : 'Merge directories',
					description : 'You will be asked for each file',
					icon : 'merge'
				},
				{
					name : 'Don\'t replace',
					description : 'Won\'t copy the source file',
					icon : 'back'
				}
			],
			move : [

			],
			rename : [

			]
		},
		file : {
			copy : [
				{
					name : 'Replace file',
					description : 'You will loose destination data',
					icon : 'replace'
				},
				{
					name : 'Keep both files',
					description : 'The copied file will be renamed',
					icon : 'merge'
				},
				{
					name : 'Don\'t replace',
					description : 'Won\'t copy the source file',
					icon : 'back'
				}
			],
			move : [

			],
			rename : [

			]
		}
	};

	var $itemOptions = {
		renameable : false,
		draggable : false,
		selectable : false
	}

	var $this = $.neovos.plugin.add( 'itemConflictWindow', {
		chooserWindow : {
			window : {}
		},
		source : null,
		destination : null,
		type : null,
		templates : {
			message : '.Template > .ItemConflictWindowMessage'
		}
	},
	{

		types : {
			MOVE : 'move',
			COPY : 'copy',
			RENAME : 'rename'
		},

		init : function( options ) {
			$(this).chooserWindow( 'message', $(options.templates.message).clone() );
			$(this).itemConflictWindow( 'source', $(options.source).item( 'data.toCanonical' ) );
			$(this).itemConflictWindow( 'destination', $(options.destination).item( 'data.toCanonical' ) );
		},

		/**
		* @context WindowContainer
		* @return itemConflictWindow
		*/
		create : function( options ) {

			var name = $(options.source).item( 'data.toCanonical' ).name;
			var key = 'itemConflictWindow.title.' + options.type;
			options.chooserWindow.window.title = System.i18n.get( key, name );
			var type = options.source.directory ? 'directory' : 'file';
			options.chooserWindow.options = $options[type][options.type];
			var window = $(this).chooserWindow( 'create', options.chooserWindow );
			return $(window).itemConflictWindow( options );
		},

		/**
		* @context itemConflictWindow
		* @type getter|setter
		* @return itemConflictWindow|Item.data
		*/
		source : function( value ) {
			if( typeof value != 'undefined' ) {
				var item = $(value).item( 'create', $itemOptions );
				item.addClass( 'Source' );
				$('.Source', this).replaceWith( item );
			}
			return $(this).itemConflictWindow( 'options' ).source;
		},

		/**
		* @context itemConflictWindow
		* @type getter|setter
		* @return itemConflictWindow|Item.data
		*/
		destination : function( value ) {
			if( typeof value != 'undefined' ) {
				var item = $(value).item( 'create', $itemOptions );
				item.addClass( 'Destination' );
				$('.Destination', this).replaceWith( item );
			}
			return $(this).itemConflictWindow( 'options' ).destination;
		}

	} );

})(jQuery);
