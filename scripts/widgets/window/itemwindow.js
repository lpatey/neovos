
(function($) {

	var $this = $.neovos.plugin.add( 'itemWindow', {
		window : {},
		itemContainer : {
			data : null
		},
		template : '.Template .ItemWindowContent'
		
	},
	{
		init : function( options ) {

			var o = this;
			
			$('.Refresh.Button',this).click( $this.buttons.reload );
			$('.List.Button', this).click( $this.buttons.list );
			$('.Icons.Button', this).click( $this.buttons.icons );
			$('.Thumbs.Button', this).click( $this.buttons.thumbs );
			$('.Desktop.Button', this).click( $this.buttons.desktop );
			$('.Previous.Button', this).click( $this.buttons.previous );
			$('.Next.Button', this).click( $this.buttons.next );
			$('.Parent.Button', this).click( $this.buttons.parent );

			$(this).keydown( $this.handlers.keyDown );

			
			var preview = $(this).itemWindow( 'preview' );
			preview.bind( 'error', function() {
				if( typeof this.trials == 'undefined' ) {
					this.trials = 1;
				}
				this.trials++;
				if( this.trials < 5 ) {
					var o = this;
					setTimeout( function() {
						var src = $(o).attr( 'src' );
						if( src.indexOf( '?' ) >= 0 ) {
							$(o).attr( 'src', src + '&t=' + new Date().getTime() );
						}
					}, this.trials * 100 );
				}
			} );

			preview.bind( 'load', function() {
				this.trials = 0;
			} );

			var ic = $(this).itemWindow( 'itemContainer' );

			ic.itemContainer( 'bind', {
				'refresh' : function() {
					$(o).itemWindow( 'refresh' );
				},
				'load' : function() {
					$(o).itemWindow( 'refresh' ); 

				},
				'merge' : function() {
					$(o).itemWindow( 'refresh' ); 
				},
				'receive' : function() { 
					$(o).itemWindow( 'refresh' ); 
				},
				'select' : function() {
					$(o).window( 'select' );
				},
				'items-add' : function( e, item ) {
					$(o).itemWindow( 'columns.refresh' );
					$(item).item( 'bind', {
						'unselect' : function() {
							$(o).itemWindow( 'refresh' );
						},
						'select' : function() {
							$(o).itemWindow( 'refresh' );
						},
						'update' : function() {
							$(o).itemWindow( 'refresh' );
						}
					} );
				},
				'items-remove' : function() {
					$(o).itemWindow( 'columns.refresh' );
				},
				'view-change' : function() {
					$(o).itemWindow( 'buttons.refresh' );
				}
			} );

			$(this).window( 'bind', {
				'select' : function() {
					ic.itemContainer( 'target', true );
				},
				'unselect' : function() {
					ic.itemContainer( 'target', false );
				},
				'refresh' : function() {
					$(o).itemWindow( 'refresh' );
				}
			} );
			
			var leftColumn = $(this).itemWindow( 'columns.left' );
			var lic = leftColumn.find( '.ItemContainer:first' );
			lic.itemContainer( {
				contextuable : false,
				selectable : false,
				data : null,
				sorter : {
					comparator : function( item1, item2 ) {
						var data1 = $(item1).item( 'data' );
						var data2 = $(item2).item( 'data' );
						var parents = {};
						if( data2 && (data2.inode == Desktop.computer.item.inode) ) {
							return 1;
						}
						while( data1 && (typeof data1.parent != 'undefined') ) {
							if( parents[ data1.inode ] ) {
								break;
							}
							if( data1.inode == Desktop.computer.item.inode ) {
								return -1;
							}
							parents[ data1.inode ] = true;
							if( data1.parent == data2.inode ) {
								return 1;
							}
							data1 = Cache.data[ data1.parent ];
						}
						return -1;
					}
				}
			} );
			
			var sic = leftColumn.find( '.ItemContainer:last' );
			sic.itemContainer( {
				contextuable : false,
				selectable : false,
				data : null
			} );

			ic.itemContainer( options.itemContainer );
			
			var data = ic.itemContainer( 'data' );
			
			$(data).item( 'data.bind', {
			
				'process-start' : function( e, process ) {
					$(o).window( 'subWindow.process', process );
					$(o).itemWindow( 'refresh' );
				},
				
				'change' : function( e, value ) {
					$(o).itemWindow( 'refresh' );
				}
			} );
			
			var process = $(data).item( 'data.process' );
			if( process ) {
				$(this).window( 'subWindow.process', process );
				$(this).itemWindow( 'refresh' );
			}
			
			$(this).itemWindow( 'trigger', 'init' );

			return this;
		},

		/**
		* @context : WindowContainer
		* @return : ItemWindow
		*/
		create : function( options ) {

			options.window.title = options.itemContainer.data.name;
			options.window.content = $( options.template ).clone();
			var window = $(this).window( 'create', options.window );
			return $(window).itemWindow( options );
		},

		/**
		* @context ItemWindow
		* @type getter
		* @return Item.data
		*/
		data : function() {
			return $(this).itemWindow( 'itemContainer' ).itemContainer( 'data' );
		},

		/**
		* @context ItemWindow
		* @return ItemWindow
		*/
		refresh : function() {
			$(this).itemWindow( 'itemContainer' ).itemWindow( 'itemContainer.refresh' );
			$(this).itemWindow( 'columns.refresh' );
			$(this).itemWindow( 'trigger', 'refresh' );
			var data = $(this).itemWindow( 'data' );
			if( data ) {
				$(this).attr( 'context', $(data).item( 'data.context' ).selector );
			}
			return this;
		},
	
		itemContainer : {

			/**
			* @context ItemWindow
			* @type getter
			* @return ItemContainer
			*/
			init : function() {
				return $('.Middle.Column .ItemContainer', this);
			},

			/**
			* @context ItemContainer
			* @return ItemContainer
			*/
			refresh : function() {
				var window = $(this).closest( '.Window' );
				var toolBar = window.find( '.Tool.Bar' );
				var item = $($(this).itemContainer('data')).item( 'data.toCanonical' );
				
				$(window).itemWindow( 'columns.left.refresh' );

				$(window).itemWindow( 'buttons.refresh' );

				$(window).window( 'title', item.name );
				
				$(this).itemWindow( 'trigger', 'itemContainer-refresh' );
				return this;
	
			}

		},

		attributes : {


			/**
			* @context ItemWindow
			* @type getter
			* @return ItemWindow.columns.right.attributes
			*/
			init : function() {
				return this.itemWindow( 'columns.right' ).find( '> .Attributes' );
			},

			/**
			* @context ItemWindow
			* @return ItemWindow
			*/
			clean : function() {
				this.itemWindow( 'attributes' ).html( '' );
				return this;
			},


			/**
			* @context ItemWindow
			* @return ItemWindow
			*/
			add : function( key, value ) {
				var dt = $('<dt>: </td>').prepend( System.i18n.getHTML( key ) );
				var dd = $('<dd></dd>' ).html( value );
				var attributes = $(this).itemWindow( 'attributes' );
				attributes.append( dt ).append( dd );
				return this;
			}
		},

		/**
		* @context ItemWindow
		* @type getter|setter
		* @return ItemWindow
		*/
		preview : function( value ) {
			if( typeof value != 'undefined' ) {
				var preview = $(this).itemWindow( 'preview' );
				var src = preview.attr( 'src' );
				if( src.indexOf( value ) != 0 ) {
					preview.attr( 'src', value );
				}
				return this;
			}
			return $(this).itemWindow( 'columns.right' ).find( '.Preview img' );
		},

		columns : {


			/**
			* @context ItemWindow
			* @return ItemWindow
			*/
			left : {
			
				init : function() {
					return $( '> .Content .Left.Column', this );
				},
				
				refresh : function() {
				
					var leftColumn = $(this).itemWindow( 'columns.left' );
					var ic = leftColumn.find( '.ItemContainer:first' );
				
					var inodes = [];
					var parents = [];
					var target = $(this).itemWindow( 'data' );
					if( !target ) {
						return;
					}
					if( typeof target.inode != 'undefined' ) {
						inodes.push( target.inode );
						parents[ target.inode ] = true;
					}
					while( (typeof target.parent != 'undefined') 
						&& (target.inode != Desktop.computer.item.inode) ) {
						
						target = Cache.data[ target.parent ];
						if( typeof target.inode == 'undefined' ) {
							break;
						}
						if( parents[ target.inode ] ) {
							break;
						}
						parents[ target.inode ] = true;
						inodes.push( target.inode );
					}
					ic.itemContainer( 'merge', inodes, null, {} );
					
					var ic = $(this).itemWindow( 'itemContainer' );
					var items = ic.itemContainer( 'items.selected' );
					var sic = leftColumn.find( '.ItemContainer:last' );
					
					if( sic.data( 'merge' ) ) {
						clearTimeout( sic.data( 'merge' ) );
						sic.removeData( 'merge' );
					}

					if( items.length == 0 ) {
						var datas = [ ic.itemContainer( 'data' ) ];
					}
					else {
						var datas = [];
						items.each( function() {
							datas.push( $(this).item( 'data' ) );
						} );
					}

					var inodes = [];
					var shares = [];
					for( var i=0; i<datas.length; i++ ) {
						if( !datas[i].attributes
							|| !datas[i].attributes.shares ) {
							continue;	
						}
						var subshares = datas[i].attributes.shares;
						for( var j in subshares ) {
							Cache.data[ subshares[j].inode ] = subshares[j];
							subshares[j].dataInode = datas[i].inode;
							inodes.push( subshares[j].inode );
							shares[ subshares[j].inode ] = subshares
						}
					}
					sic.data( 'merge', 
						setTimeout( function() {
							sic.itemContainer( 'merge', inodes, null, shares );
						}, 100 ) );
				}
			},

			/**
			* @context ItemWindow
			* @return ItemWindow
			*/
			right : {
			
				init : function() {
					return $( '> .Content .Right.Column', this );
				},

				/**
				* @context ItemWindow
				* @return ItemWindow
				*/
				refresh : function() {
					var ic = $(this).itemWindow( 'itemContainer' );
					var items = ic.itemContainer( 'items.selected' );
					$(this).itemWindow( 'attributes.clean' );

					if( items.length > 1 ) {
						$(this).itemWindow( 'preview', 'images/mime/64/multiple.png' );
						$(this).itemWindow( 'attributes.add', 'fs.attributes.selectedItems', items.length );
						return;
					}

					if( items.length ) {
						var data = items.item( 'data' );
					}
					else {
						var data = ic.itemContainer( 'data' );
					}

					if( !data ) {
						return;
					}

					$(this).itemWindow( 'preview', $(data).item( 'data.icon' ) );

					if( typeof data['name'] != 'undefined' ) {
						$(this).itemWindow( 'attributes.add', 'fs.attributes.name', $.neovos.item.tools.toHTML( data['name'] ) );
					}
					if( typeof data['size'] != 'undefined' ) {
						$(this).itemWindow( 'attributes.add', 'fs.attributes.size', $.neovos.humanize.size.toReadable( data['size'] ) );
					}
					if( typeof data['date'] != 'undefined' ) {
						$(this).itemWindow( 'attributes.add', 'fs.attributes.lastModified', data['date'] );
					}
					for( var i in data['attributes'] ) {
						var value = data['attributes'][i];
						if( typeof value != 'object' ) {
							$(this).itemWindow( 'attributes.add', i, value );
						}
					}

					$(this).itemWindow( 'trigger', 'columns-refresh' );
		
				}
			},
			
			refresh : function() {
			
				$(this).itemWindow( 'columns.left.refresh' );
				$(this).itemWindow( 'columns.right.refresh' );
			}

		},

		buttons : {

			/**
			* @context ItemWindow
			* @return ItemWindow
			*/
			refresh : function() {
				$(this).each( function() {
					var ic = $(this).itemWindow( 'itemContainer' );
					$('.List.Button', this).toggleClass( 'Disabled', ic.itemContainer( 'view', 'list' ) );
					$('.Icons.Button', this).toggleClass( 'Disabled', ic.itemContainer( 'view', 'icon' ) );
					$('.Thumbs.Button', this).toggleClass( 'Disabled', ic.itemContainer( 'view', 'thumb' ) );
					$('.Previous.Button', this).toggleClass( 'Disabled', !ic.history( 'hasPrevious' ) );
					$('.Next.Button', this).toggleClass( 'Disabled', !ic.history( 'hasNext' ) );
					$(this).itemWindow( 'trigger', 'buttons-refresh' );
				} );
				return this;
			},

			reload : function() {
				$(this).closest( '.Window' ).itemWindow( 'itemContainer' ).itemContainer( 'refresh' );
			},

			icons : function() {
				var ic = $(this).closest( '.Window' ).itemWindow( 'itemContainer' );
				if( ic.itemContainer( 'view', 'icon' ) ) {
					return true;
				}
				ic.itemContainer( 'view', 'icon', true );
				if( !$(this).closest( '.Template' ).length ) {
					var ic = $('.Template > .ItemWindow').itemWindow( 'itemContainer' );
					ic.itemContainer( 'view', 'icon', true );
				}
				return true;
			},

			list : function() {
				var ic = $(this).closest( '.Window' ).itemWindow( 'itemContainer' );
				if( ic.itemContainer( 'view', 'list' ) ) {
					return true;
				}
				ic.itemContainer( 'view', 'list', true );

				if( !$(this).closest( '.Template' ).length ) {
					var ic = $('.Template > .ItemWindow').itemWindow( 'itemContainer' );
					ic.itemContainer( 'view', 'list', true );
				}
				return true;
			},

			thumbs : function() {
				var ic = $(this).closest( '.Window' ).itemWindow( 'itemContainer' );
				if( ic.itemContainer( 'view', 'thumb' ) ) {
					return true;
				}
				ic.itemContainer( 'view', 'thumb', true );
				
				if( !$(this).closest( '.Template' ).length ) {
					var ic = $('.Template > .ItemWindow').itemWindow( 'itemContainer' );
					ic.itemContainer( 'view', 'thumb', true );
				}
				return true;
			},

			desktop : function() {
				var ic = $(this).closest( '.Window' ).itemWindow( 'itemContainer' );
				ic.history( 'current', Desktop.item );
				return true;
			},

			previous : function() {
				var ic = $(this).closest( '.Window' ).itemWindow( 'itemContainer' );
				if( !ic.history( 'hasPrevious' ) ) {
					return;
				}
				ic.history( 'previous' );
			
			},

			next : function() {
				var ic = $(this).closest( '.Window' ).itemWindow( 'itemContainer' );
				if( !ic.history( 'hasNext' ) ) {
					return;
				}
				ic.history( 'next' );
			},

			parent : function() {
				var ic = $(this).closest( '.Window' ).itemWindow( 'itemContainer' );
				var item = $(ic.itemContainer( 'data' )).item( 'data.parent' );
				if( item ) {
					ic.history( 'current', item );
				}			
			}
		},

		handlers : {

			keyDown : function( e ) {

				if( (e.keyCode == $.ui.keyCode.BACKSPACE)
					&& !e.metaKey && !e.ctrlKey && !e.shiftKey ) {
					var ic = $(this).itemWindow( 'itemContainer' );
					if( ic.history( 'hasPrevious' ) ) {
						ic.history( 'previous' );
					}
					return false;
				}
			}
		}

	} );

} )(jQuery);
