(function($) {

	var CreateList = function() {
		var o = {
			stack : [],
			started : false,
			next : function() {
				o.started = true;
				if( !o.stack.length ) {
					o.started = false;
					return;
				}
				window.status = o.stack.length;
				var handler = o.stack.shift();
				handler.call( this );
				setTimeout( o.next, 1 );
			}
		}
		return o;
	}

	var FIFO = CreateList();
	var LIFO = CreateList();

	var $this = $.neovos.plugin.add( 'scheduler', {},
	{

		/**
		* @context ?
		* @return ?
		*/
		lifo : function( fn ) {
			this.each( function() {
				var o = this
				LIFO.stack.unshift( function() {
					//try {
						fn.call( o );
					/*}
					catch( e ) {
						console.log( e );
					}*/
				} );
				if( !LIFO.started ) {
					LIFO.next();
				}

			} );
			return this;
		},

		/**
		* @context ?
		* @return ?
		*/
		fifo : function( fn ) {
			this.each( function() {
				var o = this;
				FIFO.stack.push( function() {
					//try {
						fn.call( o );
					/*}
					catch( e ) {
						console.log( e );
					}*/
				} );
				if( !FIFO.started ) {
					FIFO.next();
				}

			} );
			return this;
		}

	} );

} )(jQuery);
