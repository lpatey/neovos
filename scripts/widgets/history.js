
(function($) {

	var $this = $.neovos.plugin.add( 'history', {
		prev : [],
		next : [],
		current : null,
		change : new Function()
	},
	{
		init : function( options ) {
			$(this).history( 'bind', 'change', options.change );
			return this;
		},

		/**
		* @context History
		* @return mixed
		*/
		previous : function() {
			var value;
			$(this).each( function() {
				var options = $(this).history( 'options' );
				if( !options.prev.length ) {
					return;
				}
				var value = options.prev.pop();
				if( !options.next.length || options.next[0] != options.current ) {
					options.next.unshift( options.current );
				}
				options.current = value;
				$(this).history( 'trigger', 'change', value );
				$(this).history( 'trigger', 'previous', value );
			} );
			return value;
		},

		/**
		* @context History
		* @return boolean
		*/
		hasPrevious : function() {
			var prev = $(this).history( 'option', 'prev' );
			return prev && prev.length > 0;
		},

		/**
		* @context History
		* @return boolean
		*/
		hasNext : function() {
			var next = $(this).history( 'option', 'next' );
			return next && next.length > 0;
		},

		/**
		* @context History
		* @return mixed
		*/
		next : function() {
			var value;
			$(this).each( function() {
				var current = $(this).history( 'current' );
				var options = $(this).history( 'options' );
				if( !options.next.length ) {
					return;
				}
				var value = options.next.shift();
				
				if( !options.prev.length || options.prev[ options.prev.length - 1 ] != options.current ) {
					options.prev.push( options.current );
				}
				options.current = value;
				$(this).history( 'trigger', 'change', value );
				$(this).history( 'trigger', 'next', value );
			} );
			return value;
		},

		/**
		* @context History
		* @return History
		*/
		clear : function() {
			$(this).each( function() {
				var options = $(this).history( 'options' );
				options.prev = [];
				options.next = [];
				options.current = null;
				$(this).history( 'trigger', 'clear' );
			} );
			return this;
		},

		/**
		* @context History
		* @return History
		*/
		current : function( value ) {
			if( typeof value != 'undefined' ) {
				$(this).each( function() {
					var options = $(this).history( 'options' );
					if( value == options.current ) {
						return;
					}
					options.next = [];
					if( options.current != null && (!options.prev.length || options.prev[ options.prev.length - 1 ] != options.current) ) {
						options.prev.push( options.current );
					}
					options.current = value;
					$(this).history( 'trigger', 'change', options.current );
					$(this).history( 'trigger', 'current', options.current );
				} );
				return this;
			}
			return $(this).history( 'option', 'current' );
		},

		/**
		* @context History
		* @return History
		*/
		destroy : function() {
			$(this).history( 'clear' );
			$(this).removeClass( 'History' );
			$(this).unbind( 'neovos-history' );
			return this;
		}
		

	} );

} )(jQuery);
