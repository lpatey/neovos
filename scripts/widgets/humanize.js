
(function($) {

	$.neovos = $.extend( { humanize : {
		
		size : {

			toReadable : function( n ) {
				if( n < 1024 ) {
					return Math.round(n,2) + ' octet';
				}
				n /= 1024;
				if( n < 1024 ) {
					return Math.round(n,2) + ' Ko';
				}
				n /= 1024;
				if( n < 1024 ) {
					return Math.round(n,2) + ' Mo';
				}
				n /= 1024;
				if( n < 1024 ) {
					return Math.round(n,2) + ' Go';
				}
			}
		}

		
	} }, $.neovos );

} )(jQuery);
