(function($) {

	var $this = $.neovos.plugin.add( 'sorter', {
		items : '> *:not(.ui-draggable-helper)',
		comparator : null
	},
	{
		init : function( options ) {
			$(this).sorter( 'comparator', options.comparator );
		},

		/**
		* @context Sorter
		* @return Sorter
		*/
		sort : function() {
			this.each( function() {
				var options = $(this).sorter( 'options' );
				var comparator = options.comparator;
				var array = $.makeArray($(options.items, this));
				var o = this;
				array.sort( comparator );
				options.itemsArray = array;
				$(array).each( function() {
					$(o).append( this );
				} );
				$(this).sorter( 'trigger', 'sort' );
			} );
			return this;
		},

		/**
		* @context Sorter
		* @return Sorter
		*/
		add : function( item ) {
			this.each( function() {
				var options = $(this).sorter( 'options' );
				var itemsArray = options.itemsArray;
				var inserted = false;
				for( var i=itemsArray.length-1; i>= 0; i-- ) {
					if( options.comparator( item, itemsArray[i] ) == 1 ) {
						inserted = true;
						itemsArray.splice( i+1, 0, item );
						$(item).insertAfter( itemsArray[i] );
						return false;
					}
				}
				if( !inserted ) {
					$(this).prepend( item );
					itemsArray.unshift( item );
				}
			} );
			return this;
		},
		
		/**
		* @context Sorter
		* @return Sorter
		*/
		cleanup : function() {
			this.each( function() {
				var options = $(this).sorter( 'options' );
				options.itemsArray = [];
				$(options.items, this).remove();
			} );
			return this;
		},

		/**
		* @context Sorter
		* @return Sorter
		*/
		comparator : function( value ) {
			if( typeof value != 'undefined' ) {
				$(this).sorter( 'option', 'comparator', value );
				$(this).sorter( 'sort' );
			}
			return $(this).sorter( 'option', 'comparator' );
		}

	} );

} )(jQuery);
