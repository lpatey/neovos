
(function($) {

	var $this = $.neovos.plugin.add( 'renameable', {
		input : true,
		startTimeout : function() { return true; },
		startEdit : function() { return true; },
		start : new Function(),
		stop : new Function(),
		update : new Function(),
		toValid : function() { return $.trim(name); },
		delay : 1000,
		autoResizable : {}
	},
	{
		initialized : false,

		init : function( options ) {

			if( !$.neovos.renameable.initialized ) {
				$.neovos.renameable.initialized = false;
				$(document).click( function() {
					$('.neovos-renameable-renaming').renameable( 'stop' );
				} );
			}

			$(this).renameable( 'bind', 'start', options.start );
			$(this).renameable( 'bind', 'stop', options.stop );
			$(this).renameable( 'bind', 'update', options.update );

			$(this).click( $this.handle );

			return this;
		},

		handle : function( e ) {
			if( $('input,textarea', this).length ) {
				e.stopImmediatePropagation();
				return true;
			}
			var options = $(this).renameable( 'options' );
			if( !options.startTimeout.call( this ) ) {
				return;
			}
			$(this).addClass('neovos-renameable-renaming');
			$(this).data( 'neovos.renameable.timeout', $(this).setTimeout( $.neovos.renameable.start, 1000 ) );
		},

		start : function() {
			$(this).each( function() {
			
				var options = $(this).renameable( 'options' );
				if( !options || !options.startEdit.call( this ) ) {
					return;
				}
				$(this).addClass('neovos-renameable-renaming');
				$(this).data( 'neovos.renameable.timeout', null );

				var options = $(this).renameable( 'options' );
				var input = true;
				if( $.isFunction( options.input ) ) {
					input = options.input.call( this );
				}
				else {
					input = options.input;
				}

				var input = input ? $('<input type="text" />') : $('<textarea></textarea>');
				$(input).click( function( e ) {
					e.stopPropagation();
					return true;
				} );

				$(input).keydown( function( e ) {
					if( e.keyCode == $.ui.keyCode.ENTER ) {
						$(this).parent().renameable( 'stop' );
					}
					e.stopImmediatePropagation();
					return true;
				} );
		
				$(input).mousedown( function( e ) {
					e.stopPropagation();
					return true;
				} );
		
				$(input).mouseup( function( e ) {
					e.stopPropagation();
					return true;
				} );

				var clone = $(this).clone();

				clone.find( 'span:not(.Extra)').remove()
		
				var name = clone.text();
				$(input).attr( 'value', name );
				$(this).data( 'neovos.renameable.name', name );
				$(this).data( 'neovos.renameable.default', $(this).html() );
				$(this).html( input );
				$(input).autoResizable( $(this).renameable( 'option', 'autoResizable' ) );
				$(this).renameable( 'trigger', 'start', name );
			} );
			return this;
		},

		stop : function() {

			$(this).each( function() {
				var options = $(this).renameable( 'options' );
				if( $('input,textarea', this).length ) {
					var name = $('input,textarea',this).attr( 'value' );
					name = options.toValid.call( this, name );
					if( !name ) {
						name = $(this).data( 'neovos.renameable.name' );
						$(this).html( $(this).data( 'neovos.renameable.default' ) );
					}
					else {
						name = name.replace( "\xC2", "" );
						name = name.replace( "\xA0", "\x20" );
						$(this).html( name );
						$(this).renameable( 'update', name );
					}
				}
				$(this).removeClass('neovos-renameable-renaming');
				if( $(this).data( 'neovos.renameable.timeout' ) ) {
					clearTimeout( $(this).data( 'neovos.renameable.timeout' ) );
					$(this).data( 'neovos.renameable.timeout', null );
					return;
				}

				$(this).renameable( 'trigger', 'stop', name );
			} );
			return this;
		},

		update : function( name ) {
			$(this).renameable( 'trigger', 'update', name );
			return this;

		},

		destroy : function() {
			$(this).renameable( 'stop' );
			$(this).removeClass( 'neovos-renameable' );
			$(this).unbind( $this.handle );
			$(this).unbind( 'neovos-renameable' );
			return this;
		}
		

	} );

} )(jQuery);
