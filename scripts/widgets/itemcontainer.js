(function($) {

	var $comparators = {

		byName : function( item1, item2 ) {
			var name1 = $(item1).item( 'data' ).name.toLowerCase();
			var name2 = $(item2).item( 'data' ).name.toLowerCase();
			var inode1 = $(item1).item( 'data' ).inode;
			var inode2 = $(item2).item( 'data' ).inode;
			if( inode1 == Desktop.computer.item.inode ) {
				return -1;
			}
			if( inode2 == Desktop.computer.item.inode ) {
				return 1;
			}
			if( inode1 == Desktop.shares.item.inode ) {
				return -1;
			}
			if( inode2 == Desktop.shares.item.inode ) {
				return 1;
			}
			if( inode1 == Desktop.trash.item.inode ) {
				return -1;
			}
			if( inode2 == Desktop.trash.item.inode ) {
				return 1;
			}
			if( name1 == name2 ) {
				return 0;
			}
			return name1 > name2 ? 1 : -1;
		}
	};

	var $this = $.neovos.plugin.add( 'itemContainer', {
		data : null,
		contextuable : true,
		selectable : true,
		sorter : {
			comparator : $comparators.byName
		}
	},
	{

		/**
		* @context ItemContainer
		* @return ItemContainer
		*/
		init : function( options ) {

			$(this).mousedown( function( e ) {
				$(this).itemContainer( 'items.unselect' );
				$(this).itemContainer( 'target', true );
				e.stopPropagation();
				$(window).trigger( e );
				$('input:focus').blur();
			} );

			$(this).itemContainer( 'contextuable', options.contextuable );

			$(this).history( {
				change : function( e, data ) {
					$(this).itemContainer( 'load', data );
				}
			} );

			var o = this;

			$(this).sorter( options.sorter );

			if( options.selectable ) {
				$(this).selectable( {
					filter : '.Item .Content',
					cancel : '.Item .Content',
					start : function( e ) {
						window.focus();
						if( !e.ctrlKey ) {
							$(o).itemContainer( 'items.unselect' );
						}
					},
					stop : function( e ) {
						$('.ui-newly-selected').removeClass( 'ui-newly-selected' );
					},
					unselecting : function( e, ui ) {
						$(ui.unselecting).closest( '.Item' ).item( 'unselect' );
					},
					selecting : function( e, ui ) {
						$(ui.selecting).closest( '.Item' ).item( 'select' );
					}
				} );
			}

			$(this).each( WebBrowser.itemContainer.init );

			$(this).navigation( {
				select : function() {
					$(this).item( 'select' );
				},
				unselect : function() {
					$(this).item( 'unselect' );
				}
			} );

			$(this).droppable( {
				accept : '.Item',
				greedy : true,
				hoverClass : 'AcceptingItem',
				drop : $this.handlers.drop
		
			} );
		
			$(this).keydown( $this.handlers.keyDown );
			$(this).history( 'current', options.data );

			return this;
		},

		/**
		* @context ItemContainer
		* @return ItemContainer
		*/
		load : function( data ) {
			$(this).addClass( 'Loading' );
			var context = $(data).item( 'data.context' );
			$(this).each( function() {
				var name = data['name'];
				if( $(this).attr( 'inode' ) != data.inode ) {
					$(this).sorter( 'cleanup' );
				}
				$(this).itemContainer( 'data', data );
				$(this).itemContainer( 'trigger', 'load', data );
			} );
			var query = $(this).itemContainer( 'query' );

			var inodes;
			if( inodes = $(data).item( 'data.children' ) ) {
				this.scheduler( 'fifo', function() {
					$(this).itemContainer( 'merge', inodes, null, $(data).item( 'data.status' ) );
				} );
			}
			if( !data.loading ) {
				data.loading = true;
				$(this).tryJSON( context.protocol + 'Action_Fs_GetContent', query, function( json ) {
					$(this).removeClass( 'Loading' );
					$(data).item( 'data.children', json );
					$({}).scheduler( 'fifo', function() {
						delete data.loading;
					} );
				} );
			}
			return this;
		},

		/**
		* @context ItemContainer
		* @return query
		*/
		query : function() {
			var data = this.itemContainer( 'data' );
			var query = $.extend( { 
				path : $(data).item( 'data.path' ),
				hash : '',
				attributes : false,
				thumbs : ''
			}, $(data).item( 'data.status' ) );
			return query
		},

		/**
		* @context ItemContainer
		* @type getter|setter
		* @return ItemContainer|Item.data
		*/
		data : function( value ) {
			if( typeof value != 'undefined' ) {
				this.each( function() {
					$(this).attr( 'inode', (typeof value.inode != 'undefined' ? value.inode : '') );
					var data = $(this).itemContainer( 'data' );
					$(this).itemContainer( 'option', 'data', value );
					if( value.context ) {
						$(this).attr( 'context', $(value).item( 'data.context' ).selector );
					}
				} );
				return this;
			}
			var data = $(this).itemContainer( 'option', 'data' );
			if( data && data.inode ) {
				return Cache.data[ data.inode ];
			}
			return data;
		},

		/**
		* @context ItemContainer
		* @return ItemContainer
		*/
		merge : function( inodes, children, status ) {

			this.each( function() {
				var container = this;
				var joinedInodes = ' ' + inodes.join( ' ' ) + ' ';

				// Remove items which shouldn't be any more in the folder
				$(this).scheduler( 'fifo', function() {
				
					// Foreach items which isn't processing
					$('.Item:not(.ui-draggable-helper)', this).each( function() {
					
						var previousData = $(this).item( 'data' );
						if( previousData && (previousData.time > status.time) ) {
							return true;
						}

						// If it isn't in the inodes' list
						if( joinedInodes.indexOf( ' ' + $(this).attr( 'inode' ) + ' ' ) == -1 ) {
						
							// If name corresponds to an existing items, it's ok
							var name = $(this).attr( 'name' );
							for( var i=0; i<inodes.length; i++ ) {
								var data = $({ inode : inodes[i]}).item( 'data.fromCache' );
								if( escape(data.name) == name ) {
									if( previousData.process ) {
										data.process = previousData.process;
										$(data.process).bind( 'die', function() {
											data.time = this.time;
											$(data).item( 'data.process', false );
										} );
									}
									$(this).item( 'data', data );
									$(this).attr( 'inode', data.inode );
									return true;
								}
							}

							// Else, remove item
							if( !$(this).is( '[pid]' ) ) {
								$(container).itemContainer( 'items.remove', this );
							}
						}
					} );
				} );

				// Add or update items
				$(this).scheduler( 'fifo', function() {
					for( var i=0; i<inodes.length; i++ ) {
						var data = children == undefined ? Cache.data[ inodes[i] ] : children[i];

						if( data ) {
							$(this).itemContainer( 'items.merge', data );
						}
					}
				} );
			} );
			return this;
		},

		/**
		* @context ItemContainer
		* @return ItemContainer
		*/
		refresh : function() {
			$(this).itemContainer( 'load', $(this).itemContainer( 'data' ) );
			$(this).itemContainer( 'trigger', 'refresh' );
			return this;
		},

		/**
		* @context : ItemContainer
		* @type getter|setter|toggler
		* @return : ItemContainer|boolean
		*/
		target : function( value ) {
			return $(this).toggler(
				function() {
					$(this).itemContainer( 'select' );
				},
				function() {
					$(this).itemContainer( 'unselect' );
				},
				function() {
					return $(this).hasClass( 'Selected' );
				},
				value );
		},

		/**
		* @context ItemContainer
		* @return ItemContainer
		*/
		unselect : function() {
			$(this).removeClass( 'Selected' );
			$('.neovos-renameable-renaming').renameable( 'stop' );
			$(this).itemContainer( 'trigger', 'unselect' );
			return this;
		},
	
		/**
		* @context ItemContainer
		* @return ItemContainer
		*/
		select : function() {
			$(this).each( function() {
				$('.ItemContainer.Selected').itemContainer( 'unselect' );
				$(this).addClass( 'Selected' );
				$(this).itemContainer( 'trigger', 'select' );
			} );
			return this;
		},


		/**
		* @context ItemContainer
		* @type getter|setter
		* @return ItemContainer|boolean
		*/
		view : function( kind, value ) {
			if( typeof value != 'undefined' ) {
				$(this).each( function() {
					switch( kind ) {
						case 'list' :
							return $(this).addClass( 'ListView' ).removeClass( 'Thumb IconView' );
							break;
						case 'icon' :
							return $(this).addClass( 'IconView' ).removeClass( 'ListView Thumb' );
							break;
						case 'thumb' :
							return $(this).addClass( 'Thumb IconView' ).removeClass( 'ListView' );
							break;
					}
				} );
				$(this).itemContainer( 'trigger', 'view-change', value );
				return this;
				
			}
			
			switch( kind ) {
				case 'list' :
					return $(this).is( '.ListView' );
					break;
				case 'icon' :
					return $(this).is( ':not(.Thumb).IconView' );
					break;
				case 'thumb' :
					return $(this).is( '.Thumb.IconView' );
					break;
			}
		},


		/**
		* @context ItemContainer
		* @type getter|setter|toggler
		* @return ItemContainer|boolean
		*/
		contextuable : function( value ) {
			return $(this).toggler(
				function() {
					$(this).contextuable( {
						choices : function() {
							var ic = $(this).closest( '.ItemContainer' );
							var data = ic.itemContainer( 'data' );
							var choices = [];

							if( $(data).item( 'data.context' ) == System.context.remote ) {
								
								choices.push( {
									label : 'fs.menu.paste',
									action : function() {
										$(data).item( 'data.actions.clipboard.paste' );
									}
								} );


								choices.push( {
									label : 'fs.menu.newFolder',
									action : function() {
										$(data).item( 'data.newFolder' );
									}
								} );
							}
							return choices;
						}
					} ).addClass( 'Contextuable' );
				},
				function() {
					$(this).contextuable( 'destroy' ).removeClass( 'Contextuable' );
				},
				function() {
					return $(this).hasClass( 'Contextuable' );
				},
				value );
		},

		items : {

			/**
			* @context ItemContainer
			* @return Item
			*/
			selected : function() {
				return $('> .Selected:not(.ui-draggable-helper):visible', this );
			},

			/**
			* @context ItemContainer
			* @return ItemContainer
			*/
			unselect : function() {
				this.each( function() {
					$('> .Selected:not(.ui-draggable-helper)', this).item( 'unselect' );
				} );
				return this;
			},

			/**
			* @context ItemContainer
			* @return ItemContainer
			*/
			unselectBut : function( item ) {
				this.each( function() {
					var o = $(item).get(0);
					$('> .Selected:not(.ui-draggable-helper)', this).each( function() {
						if( this != o ) {
							$(this).item( 'unselect' );
						}
					} );
				} );
				return this;
			},

			/**
			* @context ItemContainer
			* @return ItemContainer
			*/
			merge : function( data ) {

				$(this).scheduler( 'fifo', function() {
				
					if( !data ) {
						return this;
					}

					// If our version is older than current version, stop here
					if( data.inode && typeof Cache.data[ data.inode ] != 'undefined' ) {
						if( Cache.data[ data.inode ].time > data.time ) {
							// TODO
							return this;
						}
						var parentData = $(this).itemContainer( 'data' );
						if( parentData ) {
							var parent = Cache.data[ Cache.data[ data.inode ].parent ];
							if( parent.canonic != parentData.canonic ) {
								return this;
							}
						}
					}

					var item = $('.Item[inode=' + data.inode + ']', this );
					if( item.length ) {
						var previousData = item.item( 'data' );
						if( (previousData ? (previousData.time || 0) : 0) < data.time ) {
							if( !previousData ) {
								return this;
							}
							previousData.time = data.time;
							$(data).item( 'data.merge' );
							$(item).item( 'update', data );
						}
						return this;
					}
					if( $(data).item( 'data.process' ) ) {
						return this;
					}

					$(data).item( 'data.merge' );

					var item = $(data).item( 'create', { 
						selectable : $(this).itemContainer( 'options' ).selectable
					} );
					$(this).itemContainer( 'items.add', item );
				} );

				return this;
			
			},

			/**
			* @context ItemContainer
			* @return ItemContainer
			*/
			add : function( item ) {
				this.each( function() {
					$(this).sorter( 'add', item );
					var o = this;
					var name = $(item).item( 'data' ).name;
					$(item).item( 'bind', 'rename', function() {
						$(o).sorter( 'sort' );
					} );
					$(this).itemContainer( 'trigger', 'items-add', item );
				} );
				return this;
			},

			/**
			* @context ItemContainer
			* @return ItemContainer
			*/
			remove : function( item ) {
				this.each( function() {
					$(item).remove();
					$(this).itemContainer( 'trigger', 'items-remove', item );
				} );
				return this;
			}
		},

		actions : {


			/**
			* @context ItemContainer
			* @return ItemContainer
			*/
			suppress : function() {

				this.each( function() {
					var items = $(this).itemContainer( 'items.selected' ).filter( ':not([pid])' );
					items.item( 'data.actions.suppress' );
				} );
				return this;
			},


			/**
			* @context ItemContainer
			* @return ItemContainer
			*/
			move : function( source ) {
			
				if( $(source).is( '.ItemContainer' ) ) {
					source = $(source).itemContainer( 'items.selected' );
				}
				var items = $(source).filter( ':not([pid])' );

				this.each( function() {
				
					var data = $(this).itemContainer( 'data' );
					items.item( 'data.actions.move', data );
				} );
				return this;
			},
	
			/**
			* @context ItemContainer
			* @return ItemContainer
			*/
			copy : function( source ) {
			
				if( $(source).is( '.ItemContainer' ) ) {
					source = $(source).itemContainer( 'items.selected' );
				}

				var items = $(source).filter( ':not([pid])' );
				this.each( function() {
				
					var data = $(this).itemContainer( 'data' );
					items.item( 'data.actions.copy', data );

				} );
				return this;
			},

			/**
			* @context ItemContainer
			* @return ItemContainer
			*/
			download : function( source ) {
				var items = $(source).itemContainer( 'items.selected' ).filter( ':not([pid])' );
				this.each( function() {

					var data = $(this).itemContainer( 'data' );
					items.item( 'data.actions.download', data );

				} );
				return this;
			},

			/**
			* @context ItemContainer
			* @return ItemContainer
			*/
			upload : function( source ) {
				var items = $(source).itemContainer( 'items.selected' ).filter( ':not([pid])' );
				this.each( function() {

					var data = $(this).itemContainer( 'data' );
					items.item( 'data.actions.upload', data );

				} );
				return this;
			}
		},

		handlers : {

			keyDown : function( e ) {

				if( ( e.ctrlKey || e.metaKey ) && e.keyCode == 'A'.charCodeAt( 0 ) ) {
					$('> .Item:not(.Selected)', this).item( 'select' );
					return false;
				}

				var itemVisibleSelected = $(this).itemContainer( 'items.selected' ).filter( ':not([pid])' );

				if( ((e.keyCode == $.ui.keyCode.DELETE) 
						|| (e.metaKey && e.keyCode == $.ui.keyCode.BACKSPACE)) 
						&& itemVisibleSelected.length ) {

					$(this).itemContainer( 'actions.suppress' );
					return false;

				}
				
				if( (e.ctrlKey || e.metaKey) && (e.keyCode == 'C'.charCodeAt( 0 )) ) {
					itemVisibleSelected.item( 'data.actions.clipboard.copy' );
					return false;
				}
				
				if( (e.ctrlKey || e.metaKey) && (e.keyCode == 'X'.charCodeAt( 0 )) ) {
					itemVisibleSelected.item( 'data.actions.clipboard.cut' );
					return false;
				}
				
				if( (e.ctrlKey || e.metaKey) && (e.keyCode == 'V'.charCodeAt( 0 )) ) {

					var dest = $(this).itemContainer( 'data' );
					$(dest).item( 'data.actions.clipboard.paste' );
					return false;
				}
				
				// If F5
				if( e.keyCode == 116 ) {
					$(this).itemContainer( 'refresh' );
					return false;
				}

		
				if( e.keyCode == $.ui.keyCode.ENTER && itemVisibleSelected.length ) {
					itemVisibleSelected.find( '.Content' ).dblclick();
					return false;
				}

			},
	
			drop : function( e, ui ) {

				if( $('.WindowContainer .AcceptingItem').length 
						&& !$(this).closest( '.Window' ).length ) {
						
					return false;
				}

				var o = this;
				$(o).itemContainer( 'trigger', 'drop' );
				$(o).itemContainer( 'target', true );

				if( $(ui.draggable).closest( '.ItemContainer' ).get(0) == o ) {
					$(ui.draggable).draggable( 'option', 'revert', true );
					return false;
				}

				$(o).itemContainer( 'items.unselect' );

				var data = $(o).itemContainer( 'data' );

				var source = $(ui.draggable).closest( '.ItemContainer' );
				var sourceData = $(source).itemContainer( 'data' );

				// Prevent from copying/moving inside local FS
				// Prevent from copying/moving to special directory
				if( !$(source).item( 'data.isTransferable', data ) ) {
					$(ui.draggable).draggable( 'option', 'revert', true );
					return true;
				}

				if( data['context'] == 'local' ) {
					$(o).itemContainer( 'actions.download', source );
					return true;
				}

				if( sourceData['context'] == 'local' ) {
					$(o).itemContainer( 'actions.upload', source );
					return true;
				}

				if( e.ctrlKey ) {
					$(o).itemContainer( 'actions.copy', source );
					return true;
				}
		
				$(o).itemContainer( 'actions.move', source );
				return true;

			}

		}
	} );

})(jQuery);
