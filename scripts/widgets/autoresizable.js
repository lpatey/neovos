(function($) {

	var $this = $.neovos.plugin.add( 'autoResizable', {
		space : 15,
		refresh : new Function()
	},
	{

		/**
		* @context neovos-autoResizable
		* @return neovos-autoResizable
		*/
		init : function( options ) {

			$(this).each( $this.refresh );
			$(this).keyup( $this.refresh );

			$(this).autoResizable( 'bind', 'refresh', options.refresh );
			
			return this;
		},


		/**
		* @context neovos-autoResizable
		* @return neovos-autoResizable
		*/
		refresh : function( e ) {
			$(this).each( function() {
				var o = this;
				var value = $(o).attr( 'value' ).replace( / /g, '&nbsp;' );
				var horizontal = $(o).is( 'input' );
				var span = $('<span>' + value + '</span>' );
				span.css( 'visibility', 'hidden' );
				span.css( 'position', 'absolute' );
				span.css( 'display', 'inline' );

				if( horizontal ) {
					span.css( 'white-space', 'nowrap' );
				}
				else {
					$(o).css( 'height', '1px' );
					span.css( 'width', $(this).innerWidth() );
				}
				$(o).parent().append( span );
				var width = span.width() + $(this).autoResizable( 'option', 'space' );
				var height = span.height();
				span.remove();
				if( horizontal ) {
					$(o).css( 'width', width + 'px' );
				}
				else {
					$(o).css( 'height', this.scrollHeight + 'px' );
				}

				value = span = null;
				$(this).autoResizable( 'trigger', 'refresh' );
			} );
			return this;
		},

		/**
		* @context neovos-autoResizable
		* @return neovos-autoResizable
		*/
		destroy : function() {
			$(this).unbind( $this.refresh );
			$(this).unbind( 'neovos-autoResizable' );
			$(this).removeClass( 'AutoResizable' );
			return this;
		}
	} );

})(jQuery);
