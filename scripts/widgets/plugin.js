
(function($) {

	$.fn.extend( {
		toggler : function( yes, no, test, value ) {
			if( typeof value != 'undefined' ) {
				$(this).each( function() {
					if( value == 'toggle' ) {
						test.call( this ) ? no.call( this ) : yes.call( this );
					}
					else if( value && !test.call( this ) ) {
						yes.call( this );
					}
					else if( !value && test.call( this ) ) {
						no.call( this );
					}
				} );
				return this;
			}
			return test.call( this );
		}
	} );

	$.neovos = $.extend( { plugin : {

		add : function( name, defaults, content ) {

			var fn = {};
			fn[ name ] = function( options ) {
				switch( typeof options ) {
					case 'object' :
					case 'undefined' :
						this.each( function() {
							var opts = $.extend( true, {}, $.fn[name].defaults, options );
							$(this).data( 'neovos.' + name + '.options', opts );
							$(this).addClass( name.substr( 0, 1 ).toUpperCase() + name.substr( 1 ) );
							$.neovos[name].init.call( this, opts );
						} );
						return this;
						break;
					case 'string' :
						var parentArgs = arguments;
						if( options == 'create' ) {
							parentArgs[1] = $.extend( true, {}, $.fn[name].defaults, parentArgs[1] );
							parentArgs.length = Math.max(parentArgs.length, 2);
						}
						var array = options.split( '.' );
						var target = $.neovos[name];
						while( array.length ) {
							target = target[array.shift()];
						}
						if( target && !$.isFunction(target) && typeof target['init'] != 'undefined' ) {
							target = target.init;
						}
						var args = new Array();
						for( var i=1; i<parentArgs.length; i++ ) {
							args.push( parentArgs[i] );
						}
						return target.apply( this, args );
						break;
				}
			};
			$.fn.extend( fn );
			$.fn[name].defaults = defaults;
			$.neovos[ name ] = $.extend( true, {

				// Will be added to each plugin

				/**
				* @context Item
				* @type Recursive getter|setter
				* @return Item|mixed
				*/
				option : function( path, value ) {
					if( typeof value != 'undefined' ) {
						$(this).each( function() {
							var keys = path.split( '.' );
							var options = $(this).data( 'neovos.' + name + '.options' );
							var lastKey = keys.pop();
							for( var i=0; i<keys.length; i++ ) {
								if( typeof options[keys[i]] == 'undefined' ) {
									options[keys[i]] = {};
								}
								options = options[keys[i]];
							}
							options[lastKey] = value;

						} );
						return this;
					}
					var keys = path.split( '.' );
					var options = $(this).data( 'neovos.' + name + '.options' );
					if( !options ) {
						return;
					}
					for( var i=0; i<keys.length; i++ ) {
						if( typeof options[keys[i]] == 'undefined' ) {
							return;
						}
						options = options[keys[i]];
					}
					return options;
				},

				/**
				* @context ?
				* @type getter|setter
				* @return ?|mixed
				*/
				options : function( value ) {
					if( typeof value != 'undefined' ) {
						$(this).data( 'neovos.' + name + '.options', value );
						return this;
					}
					return $(this).data( 'neovos.' + name + '.options' );
				},

				/**
				* @context ?
				* @return ?
				*/
				trigger : function( event, param ) {
					var events = event.split( ',' );
					for( var i=0; i<events.length; i++ ) {
						$(this).triggerHandler( 'neovos-' + name + '.' + events[i], param );
					}
					return this;
				},

				/**
				* @context ?
				* @return ?
				*/
				bind : function( event, handler ) {
					if( typeof event == 'object' ) {
						for( var i in event ) {
							$.neovos[ name ].bind.call( this, i, event[i] );
						}
					}
					else {
						var prefix = 'neovos-' + name + '.';
						var events = prefix + event.replace( /,\s*/g, ',' + prefix );
						this.bind( events, handler );
						
					}
					return this;
				}

			},
			content, $.neovos[ name ] );
			return $.neovos[ name ];

		}

	} }, $.neovos );

} )(jQuery);
