
(function($) {

	var $archives = [
		'application/x-rar',
		'application/x-tar',
		'application/x-compressed-tar',
		'application/x-bzip-compressed-tar',
		'application/x-farchive',
		'application/x-darchive',
		'application/zip'
	];
	
	var $directories = [
		'application/x-directory',
		'application/x-computer',
		'application/x-harddrive',
		'application/x-desktop',
		'application/x-shares',
		'application/x-home',
		'application/x-not-regular-file',
		'application/x-trash',
		'application/x-share',
		'x-directory/normal'
	];

	var $zoho = [
		'application/msword',
		'application/vnd.ms-excel',
		'application/vnd.ms-powerpoint',
		'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
		'application/rtf',
		'application/vnd.oasis.opendocument',
		'text/html',
		'text/plain',
		'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
		'application/vnd.oasis.opendocument.spreadsheet',
		'text/csv',
		'application/vnd.ms-powerpoint',
		'application/vnd.openxmlformats-officedocument.presentationml.presentation',
		'application/vnd.oasis.opendocument.presentation',
		'application/pdf'
	];

	var $handlers = {

		group : {

			dblClick : function() {
				var data = $(this).item( 'data' );
				var window = $(this).closest( '.Window' );
				if( window.length ) {
					var itemContainer = $(window).itemWindow( 'itemContainer' );
					itemContainer.history( 'current', data );
					return;
				}
				var itemContainer = $('.ItemContainer[inode=' + data.inode + ']');
				if( itemContainer.length ) {
					itemContainer.itemContainer( 'select' );
					return;
				}

				var window = $(System.window.container).itemWindow( 'create', {
					itemContainer : {
						data : data
					}
				} );
				$(window).window( 'show' );
				$(window).window( 'select' );
			}

		},
		
		remote : {
			
			dblClick : function() {
				var data = $(this).item( 'data' );
				if( data.url ) {
					window.open( data.url );
				}
			}
		},
		
		zoho : {
			
			dblClick : function() {
				var w = window.open( 'Action_Fs_Opening' );
				var data = $(this).item( 'data' );
				$({}).tryJSON( 'Action_Fs_Zoho_Open', {
					path : $(data).item( 'data.path' )
				}, function( data ) {
					w.location.href = data.url;
				} );
			}
		},
		
		share : {
		
			dblClick : function() {
				var share = $(this).item( 'data' );
				if( share.user_id != Cache.user.user_id ) {
					return;
				}
				var window = $(System.window.container).publishWindow( 'create', {
					share : share
				} );
				$(window).window( 'show' );
				$(window).window( 'select' )
			}
		}
	};

	Cache = {
		data : {},
		children : {},
		status : {},
		events : {},
		user : {}
	};


	var $this = $.neovos.plugin.add( 'item', {
		data : {},
		template : '.Template > .Item',
		humanize : $.neovos.humanize,
		draggable : true,
		renameable : true,
		selectable : true,
		contextuable : true
	},
	{

		/**
		* @context Item
		* @return Item
		*/
		init : function( options ) {

			var data = options.data;
			var o = this;
			$(this).item( 'update', data );
			$(this).item( 'selectable', options.selectable );
			$(this).item( 'contextuable', options.contextuable );
			
			$('.Content', this).dblclick( $this.handlers.dblClick );
	
			$(this).item( 'trigger', 'init' );

			$('img', this).bind( 'error', function() {
				if( typeof this.trials == 'undefined' ) {
					this.trials = 1;
				}
				this.trials++;

				if( this.trials < 5 ) {
					var o = this;
					setTimeout( function() {
						var src = $(o).attr( 'src' );
						if( src.indexOf( '?' ) >= 0 ) {
							src += '&t=' + new Date().getTime();
							$(o).attr( 'src', src );
						}
					}, this.trials * 100 );
				}
			} );

			$('img', this).bind( 'load', function() {
				this.trials = 0;
			} );

			$(this).mousedown( function( e ) {
				//$(this).parent().trigger( e );
				return false;
			} );
			
			$(data).item( 'data.bind', {
				change : function( e, value ) {
					$(o).item( 'update', value );
				}
			} );

			return this;

		},


		/**
		* @context Item.data
		* @return Item
		*/
		create : function( options ) {
			var item = $(options.template).clone();
			var data = $(this).item( 'data' );
			options.data = data;
			$(item).item( options );
			return item;
		},

		icons : {
			'image' : 'image',
			'text' : 'text',
			'audio' : 'sound',
			'video' : 'video',
			'text/html' : 'html',
			'text/x-java' : 'source_java',
			'text/x-csrc' : 'source_c',
			'application' : 'document',
			'application/pdf' : 'pdf',
			'application/x-shockwave-flash' : 'video',
			'application/ms-word' : 'document',
			'application/vnd.ms-powerpoint' : 'show',
			'application/vnd.ms-excel' : 'spreadsheet',
			'application/x-empty' : 'document',
			'application/x-not-regular-file' : 'group',
			'x-directory' : 'group',
			'application/x-directory' : 'group',
			'application/x-desktop' : 'desktop',
			'application/x-computer' : 'computer',
			'application/x-harddrive' : 'harddrive',
			'application/x-home' : 'home',
			'application/x-shares' : 'shares',
			'application/zip' : 'archive',
			'application/x-java' : 'source_java',
			'application/x-compressed-tar' : 'archive',
			'application/x-bzip-compressed-tar' : 'archive',
			'application/x-tar' : 'archive',
			'application/x-rar' : 'archive',
			'application/x-farchive' : 'farchive',
			'application/x-darchive' : 'darchive',
			'application/x-remote-directory' : 'remote_directory',
			'application/x-share' : 'share',
			'application/x-secured-share' : 'images/mime/16/secured_share.png',
			'application/x-free-share' : 'images/mime/16/free_share.png',
			'application/x-remote' : function() {
				return $this.data.icon( $(this).item( 'data' ).remotemime );
			},
			'application/x-trash' : function() {
					return Desktop.trash.item.empty ? 'trash' : 'trash_full';
			},
			'*' : 'unknown'
		},


		/**
		* @context Item
		* @return Item (cloned)
		*/
		clone : function( item ) {
			var clone = $(this).clone();
			$(clone).item( 'data', $(this).item( 'data' ) );
			return clone;
		},

		data : {

			mimes : {

				'application/x-not-regular-file' : $handlers.group,
				'application/x-directory' : $handlers.group,
				'application/x-computer' : $handlers.group,
				'application/x-desktop' : $handlers.group,
				'application/x-harddrive' : $handlers.group,
				'application/x-home' : $handlers.group,
				'application/x-trash' : $handlers.group,
				'application/zip' : $handlers.group,
				'application/x-darchive' : $handlers.group,
				'application/x-rar' : $handlers.group,
				'application/x-tar' : $handlers.group,
				'application/x-compressed-tar' : $handlers.group,
				'application/x-bzip-compressed-tar' : $handlers.group,
				'application/x-remote-directory' : $handlers.group,
				'application/x-remote' : $handlers.remote,
				'application/x-share' : $handlers.group,
				'application/x-shares' : $handlers.group,
				'application/msword' : $handlers.zoho,
				'application/vnd.ms-excel' : $handlers.zoho,
				'application/vnd.ms-powerpoint' : $handlers.zoho,
				'application/vnd.openxmlformats-officedocument.wordprocessingml.document' : $handlers.zoho,
				'application/rtf' : $handlers.zoho,
				'application/vnd.oasis.opendocument' : $handlers.zoho,
				'text/html' : $handlers.zoho,
				'text/plain' : $handlers.zoho,
				'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' : $handlers.zoho,
				'application/vnd.oasis.opendocument.spreadsheet' : $handlers.zoho,
				'text/csv' : $handlers.zoho,
				'application/vnd.ms-powerpoint' : $handlers.zoho,
				'application/vnd.openxmlformats-officedocument.presentationml.presentation' : $handlers.zoho,
				'application/vnd.oasis.opendocument.presentation' : $handlers.zoho,
				'application/pdf' : $handlers.zoho,
				'application/x-free-share' : $handlers.share,
				'application/x-secured-share' : $handlers.share,

				'x-directory' : $handlers.group,

				'*' : {
					dblClick : function() {
						$(this).item( 'data.actions.download' );
					},
					getIcon : this.getIcon
				}

			},

			/**
			* @context Item|Item.data|{ inode }
			* @return Item.data
			*/
			fromCache : function() {
				var data = this.item( 'data' );
				if( typeof Cache.data[ data.inode ] == 'undefined' ) {
					Cache.data[ data.inode ] = data;
				}
				return Cache.data[ data.inode ];
			},
			
			
			/**
			* @context Item|Item.data
			* @return Item|Item.data
			*/
			publish : function() {
				this.each( function() {
					var data = $(this).item( 'data' );
					var window = $(System.window.container).publishWindow( 'create', {
						data : data
					} );
					$(window).window( 'show' );
					$(window).window( 'select' );
				} );
				return this;
			},
			
			/**
			* @context Item|Item.data
			* @return Item|Item.data
			*/
			unpublish : function() {
				this.each( function() {
					var data = $(this).item( 'data' );
					if( !confirm( System.i18n.get( 'fs.messages.areYouSureYouWantToUnpublishAllShares', data.name ) ) ) {
						return;
					}
					
					var query = {
						path : $(data).item( 'data.path' )
					}

					$({}).tryJSON( 'Action_Fs_Share_Delete', query, function( json ) {
						data = Cache.data[ data.inode ];
						var shared = json.shared;
						
						data.attributes.shares = json.shares;
						
						data.time = Tools.getTime();
						var modifs = {
							inode : data.inode,
							time : data.time,
							loaded : true
						};

						if( !shared ) {
							data.mime = 'application/x-directory';
							modifs.mime = data.mime;
						}
						$(data).item( 'data.trigger', 'change', modifs );
					} );
			
				} );
				return this;
			},
			
			/**
			* @context Item|Item.data
			* @return Item|Item.data
			*/
			leaveShare : function() {
				this.each( function() {
					var data = $(this).item( 'data' );
					if( !confirm( System.i18n.get( 'fs.messages.areYouSureYouWantToLeaveShare', data.name ) ) ) {
						return;
					}
					
					var query = {
						path : $(data).item( 'data.path' )
					}

					$({}).tryJSON( 'Action_Fs_Share_Leave', query, function( json ) {
						$(Cache.data[ data.inode ]).item( 'data.trigger', 'delete' );
					} );
			
				} );
				return this;
			},
			/**
			* @context Item|Item.data
			* @return Item|Item.data
			*/
			trigger : function( event, param, value ) {
				var events = event.split( ',' );
				var data = this.item( 'data' );
				for( var i=0; i<events.length; i++ ) {
					if( typeof Cache.events[ data.inode ] == 'undefined' ) {
						Cache.events[ data.inode ] = {};
					}
					$(Cache.events[data.inode]).triggerHandler( 'neovos-item-data.' + events[i], param, value );
				}
				return this;
			},

			/**
			* @context Item|Item.data
			* @return Item|Item.data
			*/
			bind : function( event, handler ) {
				if( typeof event == 'object' ) {
					for( var i in event ) {
						$.neovos.item.data.bind.call( this, i, event[i] );
					}
				}
				else {
					var data = this.item( 'data' );
					var prefix = 'neovos-item-data.';
					var events = prefix + event.replace( /,\s*/g, ',' + prefix );
					if( typeof Cache.events[ data.inode ] == 'undefined' ) {
						Cache.events[ data.inode ] = {};
					}
					$(Cache.events[data.inode]).bind( events, handler );
					
				}
				return this;
			},

			/**
			* @context Item|Item.data
			* @return Item|Item.data
			*/
			merge : function( time ) {
			
				//time = time || Tools.getTime();
				var data = $(this).item( 'data' );
	
				
				// If we are uploading a file
				if( data.status == 'transfering' ) {
				
					data.time = time || Tools.getTime();
					
					// If it has already been added, stop
					if( $( '.ItemContainer[inode=' + data.parent + '] .Item[name=' + escape(data.name) + ']' ).length ) {
						return;
					}
				
					if( data.pid ) {
						$(data).item( 'data.process', System.process.processes[ data.pid ] );
						delete data.pid;
					}
				
					// Add it to all parent item containers
					$( '.ItemContainer[inode=' + data.parent + ']' ).each( function() {
						var item = $(data).item( 'create' );
						$(this).itemContainer( 'items.add', item );
					} );
					
					// And stop
					return;
				}
				
				
				if( !data.loaded ) {
					return;
				}
				
				var o = Cache.data[ data.inode ] || data;
				if( data.time < o.time && !data.attributes ) {
					return;
				}
				var fullData = Cache.data[ this.inode ] = $.extend( o, data );
				var inode = $(fullData).item( 'data.parent.inode' );
				return this;
			},

			/**
			* @context Item|Item.data
			* @return Item|Item.data|Item.data.children
			*/
			children : function( json ) {
				var parent = this.item( 'data' );
				if( typeof json !== 'undefined' ) {
				
					if( !(json.status.hash || json.status.thumbs || json.status.attributes) ) {
						return false;
					}

					if( (Cache.data[ parent.inode ] != undefined) 
						&& (Cache.data[ parent.inode ].time > json.status.time) ) {
						return false;
					} 
					var inodes = [];
					var children = [];
					while( json.content.length ) {
						var data = json.content.shift();
						data = $(data).item( 'data.toCanonical', parent );
						data.time = json.status.time;
						data.loaded = true;
						inodes.push( data.inode );
						children.push( data );
						if( !Cache.data[ data.inode ] ) {
							Cache.data[ data.inode ] = data;
						}
					}
					Cache.children[ parent.inode ] = inodes;
					$('.ItemContainer[inode=' + parent.inode + ']').itemContainer( 'merge', inodes, children, json.status );
					$(this).item( 'data.status', json.status );
					return this;
				}
				return Cache.children[ parent.inode ] || false;
			},

			/**
			* @context Item|Item.data
			* @return Item|Item.data|Item.data.status
			*/
			status : function( json ) {
				var data = this.item( 'data' );
				if( typeof json !== 'undefined' ) {
					if( !Cache.status[ data.inode ] ) {
						Cache.status[ data.inode ] = json;
					}
					else {
						Cache.status[ data.inode ] = $.extend( Cache.status[ data.inode ], json );
					}
				}
				return Cache.status[ data.inode ] || false;
			},
			
			/**
			* @context Item|Item.data
			* @return bool
			*/
			isTransferable : function( data ) {
			
				var source = $(this).item( 'data' );
				return !((data.inode < 0 && data.inode != Desktop.trash.item.inode && data.inode != Desktop.item.inode) 
					|| ((data.context == 'local') && (source.context == 'local') ));
			},

			/**
			* @context Item|Item.data
			*/
			newFolder : function() {
				var data = this.item( 'data' );
				var name = System.i18n.get( 'fs.defaults.untitledFolder' );
				var process = System.process.create();
				$({}).tryJSON( 'Action_Fs_NewFolder', {
					pid : process.pid,
					name : name,
					path : $(data).item( 'data.path' )
				}, function( json ) {
				
					if( !$('.Item[inode=' + json.inode + ']').length ) {
						var d = {
							context : System.context.remote.selector,
							name : json.name,
							inode : json.inode,
							parent : data.inode,
							directory : true,
							mime : $directories[0],
							thumb : false,
							time : json.time
						};
						Cache.data[ json.inode ] = d;
						$('.ItemContainer[inode=' + data.inode + ']').each( function() {
							var item = $(d).item( 'create' );
							$(this).itemContainer( 'items.add', item );
						} );
						var items = $('.Item[inode=' + json.inode + '] .Name').renameable( 'start' );
					}

					System.process.finish( json );
				} );

			},

			/**
			* @context Item|Item.data
			* @return Item|Item.data|System.process
			*/
			process : function( value ) {
				if( typeof value != 'undefined' ) {
					this.each( function() {
						var item = $(this);
						var data = $(this).item( 'data' );
						if( value ) {
							$(value).bind( 'die', function() {
								data.time = this.time;
								$(data).item( 'data.process', false );
							} );
							if( !data.inode && item.is( '.Item' ) ) {
								item.attr( 'pid', value.pid );
							}
							$('.Item[inode=' + data.inode + ']').attr( 'pid', value.pid );
							data.process = value;
						}
						else {
							if( !data ) {
								return;
							}
							if( !data.inode && item.is( '.Item' ) ) {
								item.removeAttr( 'pid' );
							}
							if( data.inode ) {
								$('.Item[inode=' + data.inode + ']').removeAttr( 'pid' );
							}
							else {
								$('.ItemContainer[inode=' + data.parent + '] .Item[name=' + escape(data.name) + ']').removeAttr( 'pid' );
							}
							delete data.process;
						}
					} );
					if( this.length && value ) {
						var parent = this.item( 'data.parent' );
						$(parent).item( 'data.trigger', 'process-start', value );
					}
					return this;
				}
				return this.item( 'data' ).process || false;
			},

			actions : {

				/**
				* @context Item|Item.data
				* @return Item|Item.data
				*/
				'delete' : function() {

					var items = this;

					if( !items.length ) {
						return items;
					}
					
					var context = System.context.remote;
					var process = System.process.create( context );

					var query = { files : [], pid : process.pid };

					items.each( function() {
						var data = $(this).item( 'data' );
						 query.files.push( data.name );
					} ).item( 'data.process', process );

					$({}).tryJSON( 'Action_Fs_Delete', query, System.process.finish );

					return items;

				},
				
				/**
				* @context Item|Item.data
				* @return Item|Item.data
				*/
				suppress : function() {
				
					var items = this;
					
					if( items.length == 0 ) {
						return this;
					}
					
					var parent = items.item( 'data.parent' );
				
					// If we are already on trash, delete items
					if( parent.inode == Desktop.trash.item.inode ) {
						if( items.length == 1 ) {
							var message = System.i18n.get( 'fs.messages.deleteOneConfirmation', items.item( 'data' ).name );
						}
						else {
							var message = System.i18n.get( 'fs.messages.deleteSeveralConfirmation', items.length );
						}
						if( confirm( message ) ) {
							items.item( 'data.actions.delete' ).item( 'remove' );
						}
					}
					// Else, move to trash
					else {
						items.item( 'data.actions.move', Desktop.trash.item ).item( 'remove' );
					}
					
					return this;
				},

				/**
				* @context Item|Item.data
				* @return Item|Item.data
				*/
				move : function( dest ) {

					var items = this.filter( function() {
						return $(this).item( 'data.integrity.checkDestination', dest );
					} );

					if( !items.length ) {
						return items;
					}
					
					var context = System.context.remote;
					var process = System.process.create( context );

					var source = items.item( 'data.parent' );

					var query = {
						files : [],
						path : $(source).item( 'data.path' ),
						pid : process.pid,
						dest : $(dest).item( 'data.path' )
					};

					var dataList = [];
					items.each( function() {
						var data = $(this).item( 'data' );
						dataList.push( data );
						query.files.push( data.name );
					} ).item( 'data.process', process );

					$(dataList).item( 'data.parent', dest.inode ).each( function() {
						$('.Item[inode=' + this.inode + ']').each( function() {
							$(this).item('itemContainer').itemContainer('items.remove', this);
						} );
					} );

					$('.ItemContainer[inode=' + dest.inode + ']').each( function() {
						var ic = this;
						$(dataList).each( function() {
							var item = $(this).item( 'create' );
							item.item( 'select' );
							$(ic).itemContainer( 'items.add', item );
						} );
					} );

					$({}).scheduler( 'fifo', function() {
						$({}).tryJSON( 'Action_Fs_Rename', query, System.process.finish );
					} );
					return items;

				},

				/**
				* @context Item|Item.data
				* @return Item|Item.data
				*/
				rename : function( name ) {
			
					var data = $(this).item( 'data' );
					var process = System.process.create( System.context.remote );

					if( encodeURI(data.name).replace( /%C2/g, "" ).replace( /%A0/g, "%20" ) ==
						encodeURI(name).replace( /%C2/g, "" ).replace( /%A0/g, "%20" ) ) {
						return this;
					}

					var previousName = data.name;
					var path = $(data).item( 'data.path' );
					data.name = name;
					var dest = $(data).item( 'data.path' );

					$(data).item( 'data.process', process );

					var items = $('.Item[inode=' + data.inode + ']');
					items.item( 'name', name );
					items.each( function() {
						$(this).item( 'trigger', 'rename' );
					} );

					$(this).tryJSON( 'Action_Fs_Rename', {
						path : path,
						dest : dest,
						pid : process.pid
					}, System.process.finish );
				
					return this;

				},

				/**
				* @context Item|Item.data
				* @return Item|Item.data
				*/
				copy : function( dest ) {

					var items = this.filter( function() {
						return $(this).item( 'data.integrity.checkDestination', dest );
					} );

					if( !items.length ) {
						return items;
					}
					
					var process = System.process.create( System.context.remote );

					var query = {
						files : [],
						path : $(items.item( 'data.parent' )).item( 'data.path' ),
						pid : process.pid,
						dest : $(dest).item( 'data.path' )
					};

					items.each( function() {
						 query.files.push( $(this).item( 'data' ).name );
					} );

					$('.ItemContainer[inode=' + dest['inode'] + ']').each( function() {
						var ic = this;
						items.each( function() {
							var data = $.extend( {}, $(this).item( 'data' ) );
							delete data.inode;
							data.thumb = false;
							data.parent = dest.inode;
							delete data.attributes;
							var item = $(data).item( 'create' ).item( 'data.process', process );
							item.removeAttr( 'inode' );
							item.item( 'select' );
							$(ic).itemContainer( 'items.add', item );
						} );
					} );
	
					$(this).tryJSON( 'Action_Fs_Copy', query, System.process.finish );
					return items;
				},

				/**
				* @context Item|Item.data
				* @return Item|Item.data
				*/
				upload : function( dest ) {

					var items = this.filter( function() {
						return $(this).item( 'data.integrity.checkDestination', dest );
					} );

					if( !items.length ) {
						return items;
					}

					var context = System.context.local;
					var process = System.process.create( context );
					
					var query = { 
						files : [],
						inode : '' + dest.inode,
						path : $(items.item( 'data.parent')).item( 'data.path' ),
						pid : process.pid,
						dest : $(dest).item( 'data.path' )
					};

					items.each( function() {
						 query.files.push( $(this).item( 'data' ).name );
					} ).item( 'data.process', process );

					$('.ItemContainer[inode=' + dest['inode'] + ']').each( function() {
						var ic = this;
						items.each( function() {
							var data = $.extend( {}, $(this).item( 'data' ) );
							delete data.inode;
							data.parent = dest.inode;
							data.context = System.context.local.selector;
							var item = $(data).item( 'create' );
							item.removeAttr( 'inode' );
							item.item( 'select' );
							$(ic).itemContainer( 'items.add', item );
						} );
					} );

					$(this).tryJSON( context.protocol + 'Action_Fs_Upload', query, System.process.finish );
		
					$(context).setTimeout( System.process.refresh, 500 );
					return items;
				},

				/**
				* @context Item|Item.data
				* @return Item|Item.data
				*/
				download : function( dest ) {
				
					if( !dest ) {
						location.href = 'Action_Fs_Download?path=' + encodeURI($(this).item( 'data.path' ));
						return this;
					}

					var items = this.filter( function() {
						return $(this).item( 'data.integrity.checkDestination', dest );
					} );

					if( !items.length ) {
						return items;
					}
					
					var context = System.context.local;
					var process = System.process.create( context );

					var query = { 
						files : [],
						inode : '' + dest.inode,
						path : $(items.item( 'data.parent') ).item( 'data.path' ),
						pid : process.pid,
						dest : $(dest).item( 'data.path' )
					};

					items.item( 'data.process', process ).each( function() {
						var data = $(this).item( 'data' );
						 query.files.push( data.name + (data.directory ? '/' : '' ) );
					} );
				
					$('.ItemContainer[inode=' + dest['inode'] + ']').each( function() {
						var ic = this;
						items.each( function() {
							var data = $.extend( {}, $(this).item( 'data' ) );
							delete data.inode;
							data.thumb = false;
							data.parent = dest.inode;
							data.context = System.context.local.selector;
							var item = $(data).item( 'create' );
							item.removeAttr( 'inode' );
							item.item( 'select' );
							$(ic).itemContainer( 'items.add', item );
						} );
					} );

					$(this).tryJSON( context.protocol + 'Action_Fs_Download', query, System.process.finish );
		
					$(context).setTimeout( System.process.refresh, 500 );
					return items;
				},
				
				clipboard : {
				
					/**
					* @context Item|Item.data
					*/
					copy : function() {
						System.clipboard.set(
							System.clipboard.COPY,
							{ 'application/x-filelist' : this } );
					},
					
					/**
					* @context Item|Item.data
					*/
					cut : function() {
						System.clipboard.set(
							System.clipboard.CUT,
							{ 'application/x-filelist' : this } );
					},
					
					/**
					* @context Item|Item.data
					* Le contexte est celui du dossier dans lequel on colle
					*/
					paste : function() {
					
						var data = $(this).item( 'data' );
						if( data.context == 'local' ) {
							$(this).item( 'data.actions.clipboard.pasteFromNeovos' );
							return false;
						}
				
						var chooser = $(System.window.container).chooserWindow( 'create', {
							options : [
								{
									name : 'clipboard.chooser.localClipboard.name',
									description : 'clipboard.chooser.localClipboard.description',
									icon : 'local',
									action : function() {
										chooser.window( 'close' );
										$(data).item( 'data.actions.clipboard.pasteFromLocal' );
									}
								
								},
								{
									name : 'clipboard.chooser.remoteClipboard.name',
									description : 'clipboard.chooser.remoteClipboard.description',
									icon : 'remote',
									action : function() {
										chooser.window( 'close' );
										$(data).item( 'data.actions.clipboard.pasteFromNeovos' );
									}
								},
								{
									name : 'clipboard.chooser.intelliFile.name',
									description : 'clipboard.chooser.intelliFile.description',
									icon : 'intellifile',
									action : function() {
										chooser.window( 'close' );
										$(data).item( 'data.actions.clipboard.pasteFromIntelliFile' );
									}
								
								}
							],
							message : '<img src="images/paste.png" alt="Paste" />',
							remember : {
								message : false
							},
							window : {
								title : 'clipboard.chooser.paste'
							}
						} );
						chooser.window( 'show' ).window( 'center' ).window( 'target', true );
					},
					
					pasteFromNeovos : function() {
						var clipboard = System.clipboard.get();
						var data = $(this).item( 'data' );
						if( typeof clipboard.data[ 'application/x-filelist' ] == 'undefined' ) {
							return false;
						}
						var source = clipboard.data['application/x-filelist'];
						var sourceData = Cache.data[$(source).item( 'data' ).parent];
						if( !$(sourceData).item( 'data.isTransferable', data ) ) {
							return false;
						}
				
						if( data.context == 'local' ) {
							$(source).item( 'data.actions.download', data );
							return true;
						}

						if( sourceData.context == 'local' ) {
							$(source).item( 'data.actions.upload', data );
							return true;
						}
				
						switch( clipboard.action ) {
							case System.clipboard.COPY :
								$(source).item( 'data.actions.copy', data );
								break;
							case System.clipboard.CUT :
								$(source).item( 'data.actions.move', data );
								break;
						}
					},
					
					pasteFromLocal : function() {
					
						var context = System.context.local;
						var process = System.process.create( context );
						var data = $(this).item( 'data' );
				
						$(data).item( 'data.process', process );
				
						var query = { 
							inode : '' + data.inode,
							pid : process.pid,
							dest : $(data).item( 'data.path' )
						};

						$({}).tryJSON( context.protocol + 'Action_Fs_ClipboardUpload', query, System.process.finish );
					},
					
					pasteFromIntelliFile : function() {
					
						var context = System.context.local;
						var data = $(this).item( 'data' );
						$({}).tryJSON( context.protocol + 'Action_Fs_GetUrls', {}, function( urls ) {
				
							if( !urls.length ) {
								return;
							}
				
							var context = System.context.remote;
							var query = {
								path : $(data).item( 'data.path' ),
								urls : urls
							}
							$({}).tryJSON( context.protocol + 'Action_Fs_IntelliFile_Create', query, function() {
					
							} );
						} ); 
					
					}
				
				}
			},
			
			/**
			* @context Item|Item.data
			* @return Item|Item.data
			*/
			init : function( value ) {
				if( typeof value != 'undefined' ) {
					if( typeof value.inode == 'undefined' ) {
						$(this).data( 'data', value );
						$(this).removeData( 'inode' );
					}
					else {
						var data = $(this).item( 'data' );
						if( data && !data.inode ) {
							//value = $.extend( data, value );
							$(this).removeData( 'data' );
						}
						$(value).item( 'data.merge' );
						$(this).data( 'inode', value.inode );
					}
					return this;
				}
				if( $(this).hasClass( 'Item' ) ) {
					var inode = $(this).data( 'inode' );
					if( typeof inode != 'undefined' ) {
						return Cache.data[ inode ];
					}
					return $(this).data( 'data' );
				}
				return this.get(0);
			},

			/**
			* @context Item|Item.data
			* @type getter
			* @return boolean
			*/
			directory : function() {
				var data = $(this).item( 'data' );
				return data.directory;
			},


			/**
			* @context Item|Item.data
			* @type getter
			* @return string
			*/
			path : function() {
				var data = $(this).item( 'data' );
			
				var path = '';
				if( data && typeof data.path != 'undefined' ) {
					return data.path;
				}
				else {
					var target = data;
					while( target ) {
						if( target.inode <= 0 ) {
							if( target.path ) {
								path = '/' + target.path + path;
							}
							break;
						}
						path = '/' + target.name + ( !target.directory && path ? '!' : '' ) + path;
						target = $(target).item( 'data.parent' );
					}
				}
				if( $(data).item( 'data.directory' ) ) {
					path = path + '/';
				}
				return path.replace( /\/\/+/g, '/' );
			},

			/**
			* @context Item|Item.data
			* @type getter
			* @return string
			*/
			icon : function( mime ) {
			
				if( !mime ) {
				
					var data = $(this).item( 'data' );
					if( !data || (!data.mime && !data.thumb) ) {
						return 'images/mime/64/unknown.png';
					}
					
					if( typeof data.thumb == 'string' ) {
						return data.thumb;
					}

					if( data.thumb ) {
						return 'Action_Fs_GetThumb?path=' + $(data).item( 'data.path' );
					}
					
					mime = data.mime;
				
				}

				if( typeof $this.icons[ mime ] != 'undefined' ) {
					var icon = $this.icons[ mime ];
					if( $.isFunction( icon ) ) {
						icon = icon.call( this );
					}
					if( icon.match( /[^A-Za-z0-9-_]/ ) ) {
						return icon;
					} 
					return 'images/mime/64/' + icon + '.png';
				}

				var main = mime.substr( 0, mime.indexOf( '/' ) );
				if( typeof $this.icons[ main ] != 'undefined' ) {
					var icon = $this.icons[ main ];
					if( $.isFunction( icon ) ) {
						icon = icon.call( this );
					}
					if( icon.match( /[^A-Za-z0-9-_]/ ) ) {
						return icon;
					} 
					return 'images/mime/64/' + icon + '.png';
				}

				return 'images/mime/64/unknown.png';
			},


			/**
			* @context Item|Item.data
			* @return Item|Item.data
			*/
			handle : function( action ) {
				
				var data = $(this).item( 'data' );
				if( typeof $this.data.mimes[ data.mime ] != 'undefined' ) {
					if( typeof $this.data.mimes[ data.mime ][ action ] != 'undefined' ) {
						$(this).each( $this.data.mimes[ data.mime ][ action ] );
						return this;
					}
				}
		
				var main = data.mime.substr( 0, data.mime.indexOf( '/' ) );
				if( typeof $this.data.mimes[ main ] != 'undefined' ) {
					if( typeof $this.data.mimes[ main ][ action ] != 'undefined' ) {
						$(this).each( $this.data.mimes[ main ][ action ] );
						return this;
					}
				}

				$(this).each( $this.data.mimes[ '*' ][ action ] );

				return this;
			},

			parent : {

				/**
				* @context Item|Item.data
				* @type getter|setter
				* @return Item|Item.data|Item.parent.data
				*/
				init : function( inode ) {

					if( typeof inode != 'undefined' ) {
						this.each( function() {
							var data = $(this).item('data');
							if( data.parent && (data.parent != inode) ) {
								var cache = Cache.children[ data.parent ];
								var c = [];
								for( var i=0; i<cache.length; i++ ) {
									
									if( cache[i] != data.inode ) {
										c.push( cache[i] );
									}
								}
								Cache.children[ data.parent ] = c;

								if( typeof Cache.children[ inode ] != 'undefined' ) {
									Cache.children[ inode ].push( data.inode );
								}
							}
							data.parent = inode;
						} );
						return this;
					}

					var data = $(this).item( 'data' );
					return Cache.data[ data.parent ];
				},

				/**
				* @context Item|Item.data
				* @return inode
				*/
				inode : function() {
					var data = $($(this).item( 'data' )).item( 'data.toCanonical' );
					return data.parent;
				}
			},

			/**
			* @context Item|Item.data
			* @type getter
			* @return Item.data.context
			*/
			context : function() {
				var data = $(this).item( 'data' );
				return System.context[ data.context ];
			},
			
			/**
			* @context Item|Item.data
			* @return Item.data
			*/
			toCanonical : function( parent ) {

				var data = $(this).item( 'data' );
				if( data.inode == Desktop.computer.item.inode  ) {
					data = Desktop.computer.item;
				}
				if( data.inode == Desktop.trash.item.inode  ) {
					data = Desktop.trash.item;
				}
				if( data.inode == Desktop.item.inode ) {
					data = Desktop.item;
				}
				if( data.inode == Desktop.root.item.inode  ) {
					data = Desktop.root.item;
				}
				if( data.inode == Desktop.shares.item.inode  ) {
					data = Desktop.shares.item;
				}
				if( (data.inode != Desktop.computer.item.inode)
					&& (typeof parent != 'undefined') ) {

					data.parent = parent.inode;
				}
				data.name = System.i18n.get( data.name );

				return data;
			},


			integrity : {
		
				/**
				* @context Item|Item.data
				* @return boolean
				*/
				checkDestination : function( dest ) {

					
					var data = $(this).item( 'data' );
					var testedInodes = [];

					if( dest.inode == $(this).item('data.parent.inode') ) {
						return false;
					}

					if( data.inode <= 0 ) {
						return false;
					}
					while( dest ) {
						if( testedInodes[ dest.inode ] ) {
							return true;
						}
						testedInodes[ dest.inode ] = true;
						if( dest.inode == data.inode ) {
							return false;
						}
						dest = $(dest).item( 'data.parent' );
					}
					return true;
				},

				/**
				* @context Item|Item.data
				* @return boolean
				*/
				movable : function() {
					var data = this.item( 'data' );
					return !(data.inode < 0);
				}

			}
			
		},

		/**
		* @context Item
		* @return Item
		*/
		remove : function() {
			this.each( function() {
				$(this).item( 'itemContainer' ).itemContainer( 'items.remove', this );
			} );
			return this;
		},

		/**
		* @context Item
		* @type getter|setter|toggler
		* @return Item|boolean
		*/
		draggable : function( value ) {
			return $(this).toggler(
				function() {
					$(this).draggable( {
						helper : $this.tools.toHelper,
						handle : '> .Content',
						opacity : 0.5,
						zIndex: 100,
						delay : 100,
						revert : 'invalid',
						revertDuration : 300,
						cursorAt : { left : 50, top : 50 },
						start : function( e ) {
							window.focus();
						},
						stop : function() {
							if( $(this).hasClass( 'Deletable' ) ) {
								var window = $(this).closest( '.Window' );
								$('.Deletable').remove();
								window.itemWindow( 'refresh' );
							}
							$(this).draggable( 'option', 'revert', false );
						},
						appendTo : 'body > .ItemContainer'
					} );
				},
				function() {
					$(this).draggable( 'destroy' );
				},
				function() {
					return $(this).hasClass( 'ui-draggable' );
				},
				value );
		},


		/**
		* @context Item
		* @type getter|setter|toggler
		* @return Item|boolean
		*/
		contextuable : function( value ) {
			return $(this).toggler(
				function() {
					$('.Content', this).contextuable( {
						choices : function() {
							
							var choices = [];
							var items = $(this).closest( '.ItemContainer' ).itemContainer( 'items.selected' ).filter( ':not([pid])' );

							if( items.length == 0 ) {
								return choices;
							}

							var test = true;
							items.each( function() {
								var data = $(this).item( 'data' );
								if( ($.inArray( data.mime, $archives ) == -1) 
									&& (data.mime != 'application/x-remote-directory') ) {
									return test = false;
								}
							} );
							if( test ) {
								choices.push( {
									label : 'fs.menu.browse',
									action : function() {
										items.find( '.Content' ).dblclick();
									}
								} );
							}
							
							var test = true;
							items.each( function() {
								var data = $(this).item( 'data' );
								if( ($.inArray( data.mime, $directories ) == -1)
									&& (data.mime.indexOf( 'x-directory' ) !== 0)
									&& ($.inArray( data.mime, $zoho ) == -1 )
									&& (data.mime != 'application/x-remote')  ) {
									return test = false;
								}
							} );
							if( test ) {
								choices.push( {
									label : 'fs.menu.open',
									action : function() {
										items.find( '.Content' ).dblclick();
									}
								} );
							}
							
							if( (items.length == 1)
								 && !items.item( 'data.directory' ) ) {
								choices.push( {
									label : 'fs.menu.download',
									action : function() {
										items.item( 'data.actions.download' );
									}
								} );
								choices.push( {} );
							}
							
							var test = true;
							items.each( function() {
								var data = $(this).item( 'data' );
								if( data.inode < 0 || data.inode.match(/^[^0-9]/)  ) {
									return test = false;
								}
							} );
							if( test ) {
								choices.push( {
									label : 'fs.menu.publish',
									action : function() {
										items.item( 'data.publish' );
									}
								} );
							}
							
							var test = true;
							items.each( function() {
								var data = $(this).item( 'data' );
								if( !data.attributes || !data.attributes.shares  ) {
									return test = false;
								}
								var shared = false;
								for( var i in data.attributes.shares ) {
									var share = data.attributes.shares[i];
									if( share.user_id == Cache.user.user_id ) {
										shared = true;
									}
								}
								if( !shared ) {
									return test = false;
								}
							} );
							if( test ) {
								choices.push( {
									label : 'fs.menu.unpublish',
									action : function() {
										items.item( 'data.unpublish' );
									}
								} );
							}
							
							var test = true;
							items.each( function() {
								var data = $(this).item( 'data' );
								if( !data.attributes || !data.attributes.shares  ) {
									return test = false;
								}
								var shared = false;
								for( var i in data.attributes.shares ) {
									var share = data.attributes.shares[i];
									if( share.user_id != Cache.user.user_id ) {
										shared = true;
									}
								}
								if( !shared ) {
									return test = false;
								}
							} );
							if( test ) {
								choices.push( {
									label : 'fs.menu.leaveShare',
									action : function() {
										items.item( 'data.leaveShare' );
									}
								} );
							}

							var test = true;
							items.each( function() {
								var data = $(this).item( 'data' );
								if( data.mime != 'application/x-trash' ) {
									return test = false;
								}
							} );
							if( test ) {
								choices.push( {
									label : 'fs.menu.emptyTrash',
									action : function() {
										if( confirm( System.i18n.get( 'fs.messages.deleteConfirmation' ) ) ) {
											var process = System.process.create();
											$.post( 'Action_Fs_Delete', { pid : process.pid } );
										}
									}
								} );
							}
							
							if( items.length == 1 && $(this).item( 'renameable' ) ) {
								choices.push( {
									label : 'fs.menu.rename',
									action : function() {
										items.find( '.Name' ).renameable( 'start' );
									}
								} );
							}

							choices.push( {} );

							choices.push( {
								label : 'fs.menu.copy',
								action : function() {
									items.item( 'data.actions.clipboard.copy' );
								}
							} );

							choices.push( {
								label : 'fs.menu.cut',
								action : function() {
									items.item( 'data.actions.clipboard.cut' );
								}
							} );

							choices.push( {} );

							var test = true;
							items.each( function() {
								if( !$(this).item( 'deletable' ) ) {
									return test = false;
								}
							} );
							if( test ) {
								choices.push( {
									label : 'fs.menu.delete',
									action : function() {
										items.item( 'data.actions.suppress' );
									}
								} );
							}
							
							return choices;
						}
					} ).addClass( 'Contextuable' );
				},
				function() {
					$('.Content', this).contextuable( 'destroy' ).removeClass( 'Contextuable' );
				},
				function() {
					return $(this).hasClass( 'Contextuable' );
				},
				value );
		},

		/**
		* @context Item
		* @type getter|setter|toggler
		* @return Item|boolean
		*/
		selectable : function( value ) {
			return $(this).toggler(
				function() {
					$('.Content', this).mousedown( $this.handlers.mouseDown );
					//$('.Content', this).dblclick( $this.handlers.dblClick );
					$('.Content', this).click( $this.handlers.click );
					$(this).addClass( 'Selectable' );
				},
				function() {
					$('.Content', this).unbind( 'mousedown', $this.handlers.mouseDown );
					//$('.Content', this).unbind( 'dblclick', $this.handlers.dblClick );
					$('.Content', this).unbind( 'click', $this.handlers.click );
					$(this).removeClass( 'Selectable' );
				},
				function() {
					return $(this).hasClass( 'Selectable' );
				},
				value );
		},


		/**
		* @context Item
		* @type getter|setter|toggler
		* @return Item|boolean
		*/
		renameable : function( value ) {

			return $(this).toggler( 
				function() {
					$('.Name',this).renameable( {
						toValid : $this.tools.toValidName,
						startTimeout : function() {
							var item = $(this).closest( '.Item' );
							return item.hasClass( 'Selected' ) // this item is selected
							&& !item.hasClass( 'ui-newly-selected' ) // this item isn't selected by this click
							&& item.closest( '.ItemContainer' ).find( '> .Selected' ).length == 1; // no other selected item
						},
						startEdit : function() {
							var item = $(this).closest( '.Item' );
							return !$('input, textarea', this).length // not already editing
								&& item.item( 'data' ) // has item
								&& (typeof item.data( 'dblClick' ) == 'undefined' 
									|| (Tools.getTime() - item.data( 'dblClick' ) > 500  ) );
						},
						input : function() {
							return $(this).closest( '.ItemContainer' ).hasClass( 'ListView' );
						},
						start : function() {
							var editor = $('input,textarea', this);
							var name = editor.attr( 'value' );
							var index = name.lastIndexOf( '.' );
							if( index == -1 ) {
								index = name.length;
							}
							Tools.setSelectionRange( editor.get(0), 0, index );
						},
						update : function( e, name ) {
							var item = $(this).closest( '.Item' );
							$(item).item( 'data.actions.rename', name );
						},
						autoResizable : {
							space : 8
						}
					} );
				},
				function() {
					$('.Name', this).renameable( 'destroy' );
				},
				function() {
					return $('.Renameable',this).length;
				},
				value );
		},
		
		deletable : function() {
			var data = $(this).item( 'data' );
			return data && data.inode && data.inode > 0 && parseInt(data.inode) == data.inode;
		},

		tools : {

			/**
			* @context void
			* @return string
			*/
			toHTML : function( name, length ) {
				if( typeof length == 'undefined' ) {
					length = 27;
				}
				if( name.length > length) {
					name = name.substr( 0, length -2 ) + '<span class="Extra">' + name.substr( length-2, name.length ) + '</span><span class="Ellipsis">...</span>';
				}
				name = name.replace( /([_-])/g, '$1<span> </span>' );

				name = name.replace( /[^\s<>="]{10,}/g, function( v ) {
					return v.replace( /(.)/g, '$1<span> </span>' );
				} );
				return name.replace( / /g, '&nbsp;' ).replace( /span&nbsp;class/g, 'span class' );
			},


			/**
			* @context Item
			* @return string
			*/
			toValidName : function( name ) {
				name = $.trim(name);
				var itemContainer = $(this).closest( '.ItemContainer' );
				var item = itemContainer.find( '.Item[name=' + escape(name) + ']');
				if( item.length && item.get(0) != $(this).closest( '.Item' ).get(0) ) {
					var index = name.lastIndexOf( '.' );
					if( index == -1 ) {
						index = name.length;
					}
					var part1 = name.substr( 0, index );
					var part2 = name.substr( index );

					var i=1;
					while( itemContainer.find( '.Item[name=' + escape(part1 + ' (' + i + ')' + part2 ) + ']').length  ) {
						i++;
					}
					var newName = part1 + ' (' + i + ')' + part2;
					if( confirm( System.i18n.get( 'fs.messages.nameAlreadyExists', name, newName ) ) ) {
						return newName;
					}
					return '';
				}
				return $.trim(name);
			},

			/**
			* @context Item
			* @return Item
			*/
			toHelper : function( e ) {
				var nFiles = $('> .Selected:visible', $(this).closest( '.ItemContainer' ) ).length;
				var o = $(this).clone().addClass( 'ui-draggable-helper' );
				o.removeAttr( 'inode' ).removeAttr( 'name' );
				if( nFiles == 1 ) {
					return o;
				}
				else if( nFiles == 0 ) {
					return false;
				}
				else {
					$(o).item( 'icon', 'images/mime/64/multiple.png' );
					$(o).item( 'name', System.i18n.get( 'fs.messages.nItems', nFiles ) );
					return o;
				}
			}
		},

		/**
		* @context Item
		* @type getter
		* @return boolean
		*/
		renaming : function() {
			return $('input,textarea', this).length != 0;
		},

		/**
		* @context Item
		* @type getter|setter|toggler
		* @return Item|pid|false
		*/
		processing : function( value ) {
			return $(this).toggler( 'item', 'processing',
				function() {
					$(this).attr( 'pid', value );
				},
				function() {
					$(this).removeAttr( 'pid' );
				},
				function() {
					return $(this).attr( 'pid' );
				},
				value );
		},

		/**
		* @context Item
		* @type getter|setter
		* @return Item|string
		*/
		icon : function( value ) {
			if( typeof value != 'undefined' ) {
				var icon = $('.Icon img', this);
				var src = icon.attr( 'src' );
				if( src.substr( 0, value.length ) != value ) {
					icon.attr( 'src', value );
				}
				return this;
			}
			return $('.Icon img', this).attr( 'src' );
		},

		/**
		* @context Item
		* @type getter|setter
		* @return Item|string
		*/
		attribute : function( name, value ) {
			var selector = '.' + name.substr( 0, 1 ).toUpperCase() + name.substr( 1 );
			if( typeof value != 'undefined' ) {
				$(selector, this).html( value );
				return this;
			}
			return $(selector, this).html();
		},

		/**
		* @context Item
		* @type getter|setter
		* @return Item|string
		*/
		name : function( value ) {
			if( typeof value != 'undefined' ) {
				$(this).attr( 'name', escape( value ) );
				return $(this).item( 'attribute', 'name', $this.tools.toHTML( value ) );
			}
			return $(this).item( 'attribute', 'name' );
		},

		/**
		* @context Item
		* @return Item
		*/
		update : function( data ) {
			this.each( function() {
				var options = $(this).item( 'options' );
				if( !options ) {
					return;
				}
				$(this).item( 'data', data );
				$(this).attr( 'inode', data.inode );
				if( !$(this).item( 'renaming' ) ) {
					$(this).item( 'name', data.name );
				}
				if( typeof data.thumb != 'undefined' || typeof data.empty != 'undefined' || typeof data.mime != 'undefined' ) {
					$(this).item( 'icon', $(this).item( 'data.icon' ) );
				}
				if( typeof data.size != 'undefined' ) {
					$(this).item( 'attribute', 'size', $(this).item( 'option', 'humanize' ).size.toReadable( data.size ) );
				}
				if( data.process ) {
					$(this).attr( 'pid', data.process.pid );
				}
				if( data.context ) {
					$(this).item( 'draggable', true || options.draggable && $(this).item( 'data.integrity.movable' ) );
					$(this).item( 'renameable', options.renameable && $(this).item( 'data.integrity.movable' ) );
				}
				$(this).item( 'trigger', 'update' );
			} );
			return this;
		},
	
		unselect : function() {
			this.removeClass( 'Selected Focus Source' );
			$('.neovos-renameable-renaming').renameable( 'stop' );
			this.item( 'trigger', 'unselect' );
			
		},
	
		select : function( e ) {
			$(this).closest( '.ItemContainer' ).find( '.ui-newly-selected' ).removeClass( 'ui-newly-selected' );
			$(this).closest( '.ItemContainer' ).find( '> .Focus' ).removeClass( 'Focus' );
			$(this).addClass( 'Selected ui-newly-selected Focus' );
			$(this).item( 'trigger', 'select' );
		},

		/**
		* @internal
		*/
		handlers : {

			mouseDown : function( e ) {
	
				window.focus();
		
			//	e.stopPropagation();

				$(window).trigger( e );
	
				var ic = $(this).closest( '.ItemContainer' );
		
				ic.itemContainer( 'target', true );

				var item = $(this).closest( '.Item' ).get(0);
				$('.neovos-renameable-renaming').renameable( 'stop' );
		
				if( e.shiftKey ) {
					var source = ic.find( '> .Source' );
					if( !source.length ) {
						source = ic.find( '> .Focus' );
					}
					if( !source.length ) {
						source = ic.find( '> .Item:first' );
					}
					if( source.get(0) == item ) {
						$(this).item( 'select' );
						return false;
					}
					ic.itemContainer( 'items.unselect' );
					source.addClass( 'Source' );
					if( $(item).prevAll( '.Source' ).length ) {
						while( true ) {
							$(source).item( 'select' );
							if( source.get(0) == item ) {
								break;
							}
							source = source.next();
						}
					}
					else if( $(item).nextAll( '.Source' ).length ) {
						while( true ) {
							$(source).item( 'select' );
							if( source.get(0) == item ) {
								break;
							}
							source = source.prev();
						}
					}
			
					return;
				}
		
				if( !$(item).hasClass( 'Selected' ) ) {
					if( !e.ctrlKey ) {
						ic.itemContainer( 'items.unselect' );
					}
			
					$(item).item( 'select' );
			
				}

				return;
	
			},
	
			click : function( e ) {

				var item = $(this).closest( '.Item' );
				e.stopPropagation();
				if( e.ctrlKey ) {
					if( !$(item).hasClass( 'ui-newly-selected' ) ) {
						$(item).item( 'unselect' );
					}
		
				}
				$(item).removeClass( 'ui-newly-selected' );
				if( !e.ctrlKey && !e.shiftKey ) {
					$(item).item( 'itemContainer' ).itemContainer( 'items.unselectBut', item );
					$(item).addClass( 'Focus' );
				}
				return true;
	
			},

			dblClick : function() {
				var item = $(this).closest( '.Item' );
				item.data( 'dblClick', Tools.getTime() );
				item.item( 'data.handle', 'dblClick' );
			}

		},

		/**
		* @context Item
		* @type getter
		* @return ItemContainer
		*/
		itemContainer : function() {
			return $(this).closest( '.ItemContainer' );
		},

		destroy : function() {
			$(this).removeClass( 'Item' );
			$(this).unbind( 'neovos-item' );
			return this;
		}

	} );

} )(jQuery);
