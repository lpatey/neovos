
System.i18n.data = {

	window : {
		title : {
			sendFeedback : 'Send Feedback'
		}
	},

	itemWindow : {
		buttons : {
			previous : 'Previous',
			next : 'Next',
			parent : 'Parent',
			refresh : 'Refresh',
			thumbs : 'Thumbs',
			icons : 'Icons',
			list : 'List',
			desktop : 'Desktop'
		},
		
		columns : {
			parents : 'Parents',
			shares : 'Shares',
			preview : 'Preview',
			attributes : 'Attributes'
		}
	},
	
	publishWindow : {
		title : 'Publish',
		
		options : {
			title : 'Publish options',
			password : 'Password',
			shareName : 'Share name'
		},
		
		buttons : {
			cancel : 'Cancel',
			publish : 'Publish',
			unpublish : 'Unpublish'
		},
		
		messages : {
			areYouSureYouWantToUnpublish : 'Are you sure you want to unpublish %0 ?'
		}
	},

	itemConflictWindow : {
		title : {
			move : 'Move %0',
			copy : 'Copy %0',
			rename : 'Rename %0'
		},
		message : 'There is a conflict between the following files :'
	},

	processWindow : {
		processes : 'Processes',
		fileMessage : 'File : %0'
	},

	progressGroup : {
		messages : {
			waitingForProcess : 'Waiting for process...'
		}
	},

	fs : {

		defaults : {
			untitledFolder : 'New folder',
			untitledFile : 'New file'
		},

		files : {
			myComputer : 'My Computer',
			remoteHD : 'Remote HD',
			localHD : 'Local HD',
			remoteDesktop : 'Remote desktop',
			trash : 'Trash',
			shares:  'Shares'
		},

		attributes : {
			lastModified : 'Last modified',
			size : 'Size',
			name : 'Name',
			selectedItems : 'Selected items',
			format : 'Format',
			dimensions : 'Dimensions',
			colorspace : 'Colorspace',
			length : 'Length',
			fps : 'Fps',
			pages : 'Pages',
			author : 'Author',
			title : 'Title',
			owner : 'Owner',
			compression : 'Compression'
		},

		messages : {
			nameAlreadyExists : 'The name already exists. Do you want to rename %0 as %1 ?',
			deleteOneConfirmation : 'Are you sure you want to delete "%0" ?',
			deleteSeveralConfirmation : 'Are you sure you want to delete those %0 items ?',
			deleteConfirmation : 'Are you sure you want to empty the trash ?',
			areYouSureYouWantToUnpublishAllShares : 'Are you sure you want to unpublish all %0\'s shares ?',
			areYouSureYouWantToLeaveShare : 'Are you sure you want to leave %0 ?',
			nItems : '%0 items'
		},
		
		menu : {
			emptyTrash : 'Empty trash',
			rename : 'Rename',
			open : 'Open',
			browse : 'Browse',
			newFolder : 'New folder',
			copy : 'Copy',
			paste : 'Paste',
			cut : 'Cut',
			download : 'Download',
			publish : 'Publish',
			unpublish : 'Unpublish',
			leaveShare : 'Leave share',
			'delete' : 'Delete'
		}
	},
	
	clipboard : {
	
		chooser : {
		
			paste : 'Paste',
			localClipboard : {
				name : 'Local clipboard',
				description : 'To upload files previously selected on your local desktop'
			},
			remoteClipboard : {
				name : 'Remote clipboard',
				description : 'To manipulate files inside your neovos interface'
			},
			intelliFile : {
				name : 'IntelliFile',
				description : 'Manage urls as if they were files on your neovos interface'
			}
		}
	},
	
	user : {
		auth : {
			messages : {
				invalidCredential : 'Invalid credential'
			}
		}
	}



}
