
var Desktop = {

	computer : {

		item : {
			'name' : 'fs.files.myComputer',
			'parent' : -2,
			'path' : '',
			'mime' : 'application/x-computer',
			'inode' : -1,
			'canonic' : -1,
			'context' : 'local',
			'directory' : true,
			'thumb' : false,
			'loaded' : true
		}
	},
	
	shares : {
	
		item : {
			'name' : 'fs.files.shares',
			'parent' : -2,
			'path' : '/.neovos/shares',
			'mime' : 'application/x-shares',
			'inode' : -4,
			'canonic' : -4,
			'context' : 'remote',
			'directory' : true,
			'thumb' : false,
			'loaded' : true
		}
	
	},

	root : {
	
		item : {
			'name' : 'fs.files.remoteHD', 
			'parent' : -1,
			'context' : 'remote',
			'mime' : 'application/x-harddrive',
			'inode' : 0,
			'canonic' : 0,
			'directory' : true,
			'thumb' : false,
			'loaded' : true
		}
	},

	item : {
		'name' : 'fs.files.remoteDesktop', 
		'path' : '/Desktop',
		'parent' : 0,
		'context' : 'remote',
		'mime' : 'application/x-desktop',
		'inode' : -2,
		'canonic' : -2,
		'directory' : true,
		'thumb' : false,
		'loaded' : true
	},

	trash : {
		item : {
			'name' : 'fs.files.trash',
			'parent' : null,
			'mime' : 'application/x-trash',
			'inode' : -3,
			'canonic' : -3,
			'context' : 'remote',
			'path' : '/.neovos/trash',
			'directory' : true,
			'thumb' : false,
			'loaded' : true
		}
	},

	error : {

		invalidFilename : function() {
			// TODO
		},

		invalidPath : function() {
			// TODO
		},

		destinationExists : function() {
			// TODO
		}
	},

	itemContainer : {

		init : function() {

			$(this).itemContainer( 'bind', {
				'select' : function() {
					$('.Window.Selected').window( 'target', false );
				},
				'merge' : function() {
					$(this).each( Desktop.itemContainer.refresh );
				},
				'process-start' : function( e, process ) {
					System.process.window.processWindow( 'process', process );
					$(this).each( Desktop.itemContainer.refresh );
				},
				'items-add' : function() {
					$(this).each( Desktop.itemContainer.refresh );
				},
				'items-remove' : function() {
					$(this).each( Desktop.itemContainer.refresh );
				}
			} );

			$(this).sorter( 'bind', 'sort', function() {
				$(this).each( Desktop.itemContainer.refresh );
			} );
	
			$(this).each( Desktop.itemContainer.refresh );
		},

		refresh : function() {
			var height = $(this).height();
			var itemHeight = $(' > .Item:first', this).outerHeight() + 9;
			var itemWidth = $(' > .Item:first', this).outerWidth();
			var itemsPerColumn = Math.floor(height/itemHeight);
			$(' > .Item:visible:not(.ui-draggable-helper)', this).each( function( i ) {
				/*$(this).animate( {
					top : ((i%itemsPerColumn)*itemHeight) + 'px',
					left : (Math.floor(i/itemsPerColumn)*itemWidth) + 'px'
				} );*/
				$(this).css( 'top', ((i%itemsPerColumn)*itemHeight) + 'px' );
				$(this).css( 'left', (Math.floor(i/itemsPerColumn)*itemWidth) + 'px' );
			} );
		}

	},

	init : function() {	
	
		$(document).keydown( function( e ) {
			switch( e.keyCode ) {
				case $.ui.keyCode.TAB :
					if( e.ctrlKey || e.shiftKey || e.metaKey ) {
						e.preventDefault();
						e.stopImmediatePropagation();
						var window = $('body > .WindowContainer > .Window.Selected').next( '.Window:visible' );
						if( !window.length ) {
							window = $('body > .WindowContainer > .Window:visible:first' );
						}
						window.window( 'select' );
						return false;
					}
					break;
				case 'W'.charCodeAt(0) :
					if( e.ctrlKey || e.shiftKey || e.metaKey ) {
						var w = $('body > .WindowContainer .Window.Selected');
						if( w.window( 'closable' ) ) {
							w.window( 'close' );
						}
						return false;
					}
					break;
			}
		} );
	
		$('#MainBar > .SearchBox input', this).keyup( function() {
			var value = $(this).attr( 'value' ).toLowerCase();
			
			if( !value ) {
				$('.ItemContainer > .Item').show();
			}
			else {
				$('.ItemContainer > .Item').each( function() {
					if( $(this).attr( 'name' ).toLowerCase().indexOf( value ) != -1 ) {
						$(this).show();
					}
					else {
						$(this).hide();
					}
				} );
			}

			$('body > .ItemContainer').each( Desktop.itemContainer.refresh );
		} ).keydown( function( e ) {
			e.stopPropagation();
		} );

	
		$(window).bind( 'resize', function() {
			$('body > .ItemContainer').each( Desktop.itemContainer.refresh );
		} );
		
		$('body > .ItemContainer').each( Desktop.itemContainer.init );

	}
	
}

$(function() {
	Cache.data[ Desktop.item.inode ] = Desktop.item;
	Cache.data[ Desktop.root.item.inode ] = Desktop.root.item;
	Cache.data[ Desktop.shares.item.inode ] = Desktop.shares.item;
} );


