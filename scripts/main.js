
Modules = {
};

$(function() {

	$(this).each( WebBrowser.init );
	$(this).each( System.i18n.init );

	for( var k in Modules ) {
		$(this).each( Modules[k].init );
	}
	$(this).each( Desktop.init );

	$(this).each( System.init );

	$(Desktop.item).item( 'data.merge' );
	$(Desktop.computer.item).item( 'data.merge' );
	$(Desktop.root).item( 'data.merge' );
	$(Desktop.trash.item).item( 'data.merge' );

	$('body > .ItemContainer').itemContainer( {
		data : Desktop.item
	} );
	
	/*setTimeout( function() {
		var folderConflict = $(System.window.container).itemConflictWindow( 'create', {
			source : Cache.data[8462428],
			destination : Cache.data[8462428],
			type : $.neovos.itemConflict.types.COPY
		} );
		folderConflict.window( 'show' ).window( 'center' ).window( 'target', true );

		var folderConflict = $(System.window.container).itemConflictWindow( 'create', {
			source : Cache.data[8216577],
			destination : Cache.data[8216577],
			type : $.neovos.itemConflict.types.COPY
		} );
		folderConflict.window( 'show').window( 'center' ).window( 'target', true );

	}, 5000 );*/
	

} );
