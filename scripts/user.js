
var User = {

	ajax : new Array(),

	checkAuth : function( json, url, data, callback ) {

		User.ajax.push( {
			context : this,
			url : url,
			data : data,
			callback : callback
		} );

		var modal = Modal.create();
		modal.addClass( 'Action_User_CheckAuth' );
		$('body > .ModalContainer').append( modal );
		$('> .TitleBar > h3', modal).html( 'Authentication' );
		$('> .Content', modal ).load( 'Action_User_GetAuthWindow', function() {
	
			$('> .Content > form', modal).submit( function() {
				$( '> .More > .Loading', modal ).show();
				$('> .Content > form > .Message', this ).hide();
				$.post( 'Action_User_CheckAuth', Tools.getData.call( this ), User.handleAuth, 'json' );
				return false;
			} );

			$(modal).each( Modal.show );
			$('input[type=password]', this).focus();
		} );
		
	},

	handleAuth : function( json ) {
		var modal = $('body > .ModalContainer > .Action_User_CheckAuth' );
		modal.find( '> .More > .Loading' ).hide();
		if( !json['error'] ) {
			modal.each( Modal.hide );
			while( User.ajax.length ) {
				var ajax = User.ajax.shift();			
				$(ajax.context).tryJSON( ajax.url, ajax.data, ajax.callback );

			}
			
		}
		else {
			$('> .Content > form > .Message', modal ).html( System.i18n.getHTML(json['message']) ).show();
		}
	} 
}
