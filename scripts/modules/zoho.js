
(function($) {

	var $this = Modules.Zoho = {
	
		init : function() {
		
			$('.ShortCuts .Zoho.Icon').click( function() {
			
				var w = window.open( 'Action_Fs_Opening' );
				var format = $(this).attr( 'format' );
				$({}).tryJSON( 'Action_Fs_Zoho_New', {
					format : format,
					name : System.i18n.get( 'fs.defaults.untitledFile' ),
					path : $($('.ItemContainer.Selected').itemContainer( 'data' )).item( 'data.path' )
				}, function( data ) {
					w.location.href = data.url;
				} );
			} );
		}
	}
	
})(jQuery);
