
(function($) {

	var $this = Modules.Desktop = {
	
		init : function() {

			$('#MainBar', this).each( $this.bar.init );
		},
	
		bar : {
		
			data : null,
	
			init : function() {
			
				$this.bar.data = $('.Preview', this);

				$(System.window.container).window( 'bind', {
				
					create : function( e, window ) {

						$(window).window( 'bind', {
							
							show : function() {
								$this.bar.data.append( $(this).window( 'preview' ) );
							},
							
							hide : function() {
								$(this).window( 'preview' ).remove();
							}
						
						} );
					}
				} );
		
				var preview = $this.bar.data;
				preview.hover( $this.bar.preview.show, $this.bar.preview.hide );

				$('.ShortCuts .Slide.Icon', this).hover( $this.bar.preview.show, $this.bar.preview.hide );
				
				$('.ShortCuts .Logout.Icon', this).click( function() {
					location.href = 'Action_User_Logout';
				} );
			},
		
			preview : {
		
				show : function() {
					var preview = $this.bar.data;
					if( !preview.find( '.PreviewItem' ).length ) {
						return;
					}
					var timeout = preview.data( 'timeout' );
					if( timeout ) {
						clearTimeout( timeout );
					}
					preview.stop( true, true );
					preview.show();
					return true;
				},
			
				hide : function() {
					var preview = $this.bar.data;
					preview.data( 'timeout', setTimeout( function() {
						preview.removeData( 'timeout' );
						preview.fadeOut( function() {
						} );
					} ), 500 );
				}
			}
		}

	}


})(jQuery);

