
(function($) {

	var map = {
		'Action_Fs_GetContent' : 'FileListing',
		'Action_Fs_GetUrls' : 'UrlListing',
		'Action_Fs_Upload' : 'FileUpload',
		'Action_Fs_ClipboardUpload' : 'ClipboardUpload',
		'Action_Fs_Download' : 'FileDownload',
		'Action_Ps_GetProcesses' : 'GetProcesses',
		'Action_System_GetSynchronisationData' : 'GetSynchronisationData'
	}

	$(document).keydown( function( e ) {
		if( !e.alreadyTransfered ) {
			e.alreadyTransfered = true;
			$('.ItemContainer.Selected').trigger( e );
		}
	} );

	$.fn.extend({


		tryJSON : function( url, data, callback ) {

			var context = this;
			if( typeof data == 'function' ) {
				callback = data;
				data = {};
			}

			function tryJSONCallback( json ) {
				if( typeof json['error'] != 'undefined' ) {
					var array = json['error'].split( '.' );
					var handler = window;
					while( array.length ) {
						var key = array.shift()
						handler = handler[key];
					}
					$(context).each( function() {
						handler.call( this, json, url, data, callback );
					} );
				}
				else {
					if( callback ) {
						$(context).scheduler( 'fifo', function() {
							$(context).each( function() {
								callback.call( this, json );
							} );
						} );
					}
				}
				return null;
			}

			if( url.substr( 0, url.indexOf( ':' ) ) == 'local' ) {
				var name = map[url.substr( url.indexOf( ':' ) + 1 )];
				$.neovos.tryJSON.addRequest( {
					name : name,
					context : $,
					data : data,
					callback : tryJSONCallback
				} );
			}
			else {

				var data2 = {};
				$.neovos.tryJSON.addItem( data2, new Array(), data );
				$.post.call( $, url, data2, tryJSONCallback, 'json' );
			}

		},

		setTimeout : function( fn, delay ) {

			var o = this;

			function handler() {
				$(o).each( function() {
					fn.call( this );
				} );
			}
			return setTimeout( handler, delay );
		}

	} );

	$.neovos = $.extend( { tryJSON : {

		addItem : function( dest, keys, source ) {
			if( typeof source == 'object'  ) {
				for( var k in source ) {
					var copy = new Array();
					for( var k2 in keys ) {
						copy[k2] = keys[k2];
					}
					copy.push( k );
					$.neovos.tryJSON.addItem( dest, copy, source[k] );
				}
			}
			else {
				var key = keys.shift();
				if( keys.length ) {
					key += '[' + keys.join( '][' ) + ']';
				}
				dest[ key ] = source;
			}
		},

		addRequest : function( request ) {

			request.handler = function( json ) {
				request.callback.call( request.context, eval( '(' + json + ')' ) );
			} 

			if( !document.java ) {
				document.java = {
					requests : new Array()
				}
				$('body:first').tryJSON( 'Action_System_GetAppletParameters', function( json ) {
					var applet = '<applet code="com.neovos.connect.Main.class" archive="java/NeovosConnect.jar" mayscript="true">';
					for( var k in json ) {
						applet += '<param name="' + k + '" value="' + json[k] + '" />';
					}
					applet += '</applet>';
					$('body:first').append( applet );
					document.java.requests.push( request );
					$(System.context.local).setTimeout( System.synchronization.refresh, 1000 );

				} );
			}
			else {
				document.java.requests.push( request );
			}
		}


	} }, $.neovos );

} )( jQuery );
