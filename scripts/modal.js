
var Modal = {

	init : function() {

	},

	show : function() {
		var container = $(this).closest( '.ModalContainer' );
		if( !$('> .Modal:visible', container).length ) {
			$('> .Darkness', container).show();
		}
		$(this).fadeIn( 'fast' );
		

	},

	hide : function() {
		var container = $(this).closest( '.ModalContainer' );
		$(this).fadeOut( 'fast', function() {
			if( !$('> .Modal:visible', container).length ) {
				$('> .Darkness', container).fadeOut( 'fast' );
			}
			$(this).remove();
		} );
	},

	create : function() {
		var modal = $('<div class="Modal"></div>' );
		modal.html( '<div class="TitleBar"><h3></h3></div><div class="Content"></div><div class="More"></div>' );
		$('> .More', modal).html( '<p class="Loading"><img class="Loading" src="images/loading.gif"></p>' );
		modal.each( Modal.init );
		return modal;
	}


}
