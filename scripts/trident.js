
var WebBrowser = {

	init : function() {
	
		/* Disable selection under Internet Explorer */
		$(document).bind( 'selectstart', function( e ) {
			return $(e.target).attr( 'tagName' ).toLowerCase() == 'input' 
				||  $(e.target).attr( 'tagName' ).toLowerCase() == 'textarea';
		} );

	},

	itemContainer : {

		init : function() {
			$(this).itemContainer( 'bind', 'items-add', function( e, item ) {
				$('img', item).bind( 'dragstart', function( e ) {
					e.preventDefault();
				} );
				$(item).mousedown( function( e ) { e.stopPropagation(); } );
			} );
		}

	}

}
