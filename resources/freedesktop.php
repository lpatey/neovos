<?php

error_reporting( E_ALL );

$xml = new DOMDocument();
$xml->load('freedesktop.org.xml');

$xpath = new DOMXpath( $xml );
$xpath->registerNamespace( 'ns', 'http://www.freedesktop.org/standards/shared-mime-info' );

foreach( $xpath->evaluate( '//ns:glob' ) as $extension ) {
	$ext = substr( $extension->getAttribute( 'pattern' ), 2 );
	$mime = $extension->parentNode->getAttribute( 'type' );
	echo $ext . "\t" . $mime . "\n";
}

?>
