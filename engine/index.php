<?php

if( !preg_match( '/^[A-Za-z0-9_]+$/', $_GET['action'] ) ) {
	$_GET['action'] = 'Action_Portal_Index';
}

set_include_path( get_include_path() 
	. PATH_SEPARATOR . 'engine' 
	);


require_once 'Config.php';
require_once 'Core/Exception.php';
require_once 'Core/Logger.php';
require_once 'Bean/User.php';

session_start( isset($_GET['login']) ? $_GET['login'] : 'neovos' );
session_write_close();

umask( 0 );

$action = $_GET['action'];
$logger = Core_Logger::getLogger( Config::get( 'log', 'name' ) );

$path = str_replace( '_', '/', $action ) . '.php';

if( !file_exists( $path ) ) {
	$path = 'Action/Portal/404.php';
	$action = 'Action_Portal_404';
}

require_once $path;

$instance = new $action( $_REQUEST, $logger );

try {

	$return = $instance->prepend();
	if( $return !== null ) {
		echo json_encode( $return );
		exit(0);
	}

	$return = $instance->execute();
	if( $return !== null ) {
		echo json_encode( $return );
		exit(0);
	}

	$instance->view();

}
catch( Core_Exception $e ) {
	echo json_encode( array( 'error' => $e->getMessage() ) );
	exit(0);
}
