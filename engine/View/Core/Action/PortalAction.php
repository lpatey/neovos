<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
                      "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<link rel="stylesheet" type="text/css" href="/portal/styles/portal.css" media="screen" />
		<script type="text/javascript" src="/portal/scripts/jquery.js"></script>
		<script type="text/javascript" src="/portal/scripts/jquery.ui.js"></script>
		<script type="text/javascript" src="/portal/scripts/portal.js"></script>
		<link rel="shortcut icon" type="image/png" href="/portal/images/favicon.png" />
		<title><?php echo $this->_getTitle(); ?> </title>
	</head>
	<body>
	
	<div id="Container">
	<a href="/"><img class="Logo" src="/portal/images/portal/logo.png" /></a>

	<div id="Main">
		<div></div>
		<?php echo $this->_getContent(); ?>
		
		<div class="Footer">
		<div class="Copyright">&copy; Neovos 2010</div>
		
		<div class="Links">
			<a href="http://forums.neovos.com">forums</a> - 
			<a href="Action_Portal_Contact">contact</a>
		</div>
		
		<div style="clear: both"></div>
		</div>
	</div>
	</div>

	</body>
</html>
