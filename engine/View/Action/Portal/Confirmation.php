<div class="Panel">
<h2>Account created</h2>

<p>You've been signed in successfully.</p>

<p>Before log in, you will have to enabled your account. <br />
An activation email has been sent to</p>

<p style="text-align: center"><a href="mailto:<?php echo $this->_getEmail(); ?>"><?php echo $this->_getEmail(); ?></a></p>

</div>
