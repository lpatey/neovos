<div class="Add LeftPanel">

	<h2>Join share</h2>
	
	<p>Once you've added this share, it will be placed on your shares' folder.</p>
	
	<?php if( $this->_hasMessage() ) : ?>
		<div class="Error Message"><?php echo $this->_getMessage(); ?></div>
	<?php endif ?>
	
	<form method="post" action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>">
	<input type="hidden" name="action" value="Core_ShareAction" />
		<?php if( $this->_getShare()->hasPassword() ) : ?>
		<dl>
			<dt>Password:</dt>
			<dd><input type="password" name="password" value="" /><br />
		</dl>
		<div style="clear:both"></div>
		<?php endif ?>
		<div class="Button">Join share</div>
	</form>

</div>

<?php require 'View/Action/Portal/Share/RightPanel.php'; ?> 

<div style="clear:both"></div>
