<div class="LeftPanel">

	<h2>Authentication required</h2>
	
	<p>You need to authenticate to access this share.</p>
	
	<h3>Log in</h3>
	
	<form method="post" action="/Action_Portal_LogIn">
	<input type="hidden" name="action" value="Core_UserAction" />
	<input type="hidden" name="return" value="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" />
		<dl>
			<dt>Login:</dt>
			<dd><input type="text" name="login" value="<?php echo htmlentities($this->_getLogin()); ?>" /></dd>
			<dt>Password:</dt>
			<dd><input type="password" name="password" value="" /><br />
			<a href="/Action_Portal_Reset">Password forgotten ?</a></dd>
			<dt>&nbsp;</dt>
			<dd><input type="submit" value="Log in" /></dd>
		</dl>
	</form>
	<div style="clear:both"></div>

	<h3>Or sign in</h3>
	<form method="post" action="/Action_Portal_SignIn">
	<input type="hidden" name="action" value="Core_UserAction" />
	<input type="hidden" name="return" value="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" />
		<dl>
			<dt>Login:</dt>
			<dd><input type="text" name="login" value="<?php echo htmlentities($this->_getLogin()); ?>" />  <img src="" /></dd>
			<dt>Password:</dt>
			<dd><input type="password" name="password" value="" /></dd>
			<dt>Email:</dt>
			<dd><input type="text" name="email" value="<?php echo htmlentities($this->_getEmail()); ?>" />  <img src="" /></dd>
			<dt>&nbsp;</dt>
			<dd><input type="submit" value="Sign in" /></dd>
		</dl>
	</form>
	
	<div style="clear:both"></div>

</div>
<?php require 'View/Action/Portal/Share/RightPanel.php'; ?> 

<div style="clear:both"></div>
