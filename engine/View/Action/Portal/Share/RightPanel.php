<div class="RightPanel">

	<h2><?php echo $this->_getShare()->getName(); ?></h2>
	
	<h3>Preview</h3>
	<div class="Thumb IconView ItemContainer"></div>
	
	<h3>Informations</h3>
	<dl>
		<dt>Owner:</dt>
		<dd><?php echo $this->_getShare()->getOwner()->getLogin(); ?></dd>
		<dt>Restricted:</dt>
		<dd><?php echo $this->_getShare()->hasPassword() ? 'yes' : 'no'; ?></dd>
	</dl>
	
	<div class="Template">
			<div class="Item">
				<div class="Content">
					<div class="Shadow"></div>
					<div class="Icon"><div><div><img src="images/mime/64/unknown.png" /></div></div></div>
					<div class="Name"></div>
					<div class="Size"></div>
				</div>
				<div class="Unfloat"></div>
			</div>
	</div>
	
	
	<link rel="stylesheet" type="text/css" href="/portal/styles/widgets/item.css" media="screen" />
	<script type="text/javascript" src="/portal/scripts/neovos.enhanced.js"></script>
	<script type="text/javascript" src="/portal/scripts/tools.js"></script>
	<script type="text/javascript" src="/portal/scripts/system.js"></script>
	<script type="text/javascript" src="/portal/scripts/desktop.js"></script>
	<script type="text/javascript" src="/portal/scripts/widgets/plugin.js"></script>
	<script type="text/javascript" src="/portal/scripts/widgets/item.js"></script>
	<script type="text/javascript">
	var Cache = {
		data : {
			1 : <?php echo json_encode( $this->_getShare()->getData() ); ?>
		},
		events : {
		}
	}
	$(function() {
		var item = $(Cache.data[1]).item('create', {
			contextuable : false,
			selectable : false,
			draggable : false
		} );
		var img = item.find( 'img' );
		img.attr( 'src', '/portal/' + img.attr( 'src' ) );
		$('.ItemContainer').append( item );
		$('.ItemContainer *').unbind();
	} );
	</script>

</div>

