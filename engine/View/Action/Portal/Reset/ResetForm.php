<div class="MainForm">

<h2>Password reset</h2>

<p>You can now reset your password</p>

<?php if( $this->_hasMessages() ) : ?>
	<div class="Message">Please, correct the following errors :
	<ul>
		<?php foreach( $this->_getMessages() as $message ) : ?>
		<li><?php echo $message; ?></li>
		<?php endforeach ?>
	</ul>
	</div>
<?php endif ?>

<form method="post" action="Action_Portal_Reset?key=<?php echo htmlentities($_GET['key']); ?>">
<input type="hidden" name="action" value="PortalAction_Reset_Reset" />
<dl>

	<dt>New password:</dt>
	<dd><input type="password" name="password" value="" /></dd>
	<dt>Confirmation:</dt>
	<dd><input type="password" name="confirmation" value="" /></dd>
	<dt>&nbsp;</dt>
	<dd><input type="submit" value="Reset" /></dd>
</dl>

<div style="clear: both"></div>
</form>

</div>
