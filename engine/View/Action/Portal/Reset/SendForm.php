<div class="MainForm">

<h2>Password forgotten</h2>

<p>Enter login or email. An email with a password reset url will be sent.</p>

<?php if( $this->_hasMessages() ) : ?>
	<div class="Message">Please, correct the following errors :
	<ul>
		<?php foreach( $this->_getMessages() as $message ) : ?>
		<li><?php echo $message; ?></li>
		<?php endforeach ?>
	</ul>
	</div>
<?php endif ?>

<form method="post" action="Action_Portal_Reset">
<input type="hidden" name="action" value="PortalAction_Reset_Send" />
<dl>

	<dt>Login:</dt>
	<dd><input type="text" name="login" value="<?php echo htmlentities($this->_getLogin()); ?>" /></dd>
	<dt>Or email:</dt>
	<dd><input type="text" name="email" value="<?php echo htmlentities($this->_getEmail()); ?>" /></dd>
	<dt>&nbsp;</dt>
	<dd><input type="submit" value="Send email" /></dd>
</dl>

<div style="clear: both"></div>
</form>

</div>
