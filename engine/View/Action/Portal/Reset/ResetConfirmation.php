<div class="Panel">

<h2>Password reset</h2>

<p>Your password has been reset successfully.</p>

<p>You can now access your account at the following url</p>

<p style="text-align: center"><a href="<?php echo $this->_getUser()->getPublicUrl(); ?>"><?php echo $this->_getUser()->getPublicUrl(); ?></a></p>

</div>
