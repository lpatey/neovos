
<div class="MainForm">
	<h2>Neovos Log in</h2>
	
	<?php if( $this->_hasMessage() ) : ?>
		<div class="Message"><?php echo $this->_getMessage(); ?></div>
	<?php endif ?>

	<form method="post" action="Action_Portal_LogIn">
	<?php if( $this->_getReturn() ) : ?>
	<input type="hidden" name="return" value="<?php echo htmlentities($this->_getReturn()); ?>" />
	<?php endif ?>
	<input type="hidden" name="action" value="Core_UserAction" />
		<dl>
			<dt>Login:</dt>
			<dd><input type="text" name="login" value="<?php echo htmlentities($this->_getLogin()); ?>" /></dd>
			<dt>Password:</dt>
			<dd><input type="password" name="password" value="" /><br />
			<a href="Action_Portal_Reset">Password forgotten ?</a></dd>
			<dt>&nbsp;</dt>
			<dd><input type="submit" value="Log in" /></dd>
		</dl>
	</form>
	
	<div style="clear:both"></div>
</div>
