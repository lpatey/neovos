
<div class="Large MainForm">

	<h2>By email</h2>
	
	<?php if( $this->_hasMessage() ) : ?>
		<div class="Message"><?php echo $this->_getMessage(); ?></div>
	<?php endif ?>

	<form method="post" action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>">
		<input type="hidden" name="text" value="" />
	<dl>
		<dt>To:</dt>
		<dd><input type="text" readonly="readonly" name="contact[to]" value="" /></dd>
		<dt>Subject:</dt>
		<dd><input type="text" name="contact[subject]" value="<?php echo htmlentities($this->_getSubject()); ?>" /></dd>
		<dt>Message:<br /><em>(required)</em></dt>
		<dd><textarea name="contact[message]"><?php echo htmlentities($this->_getMessageContent()); ?></textarea></dd>
		<dt>From:</dt>
		<dd><input type="text" name="contact[from]" value="<?php echo htmlentities($this->_getFrom()); ?>" /></dd>
		<dt>&nbsp;</dt>
		<dd><input type="submit" value="Send" /></dd>
	</dl>
	<div style="clear: both"></div>
	</form>
</div>
<script type="text/javascript">
$('input[type=text][readonly]').attr( 'value', '<?php echo str_replace( '@', '\x40', $this->_getEmail() ); ?>' );
</script>

<div class="Large Panel">
	<h2>On forums</h2>
	<p>Don't hesitate to post messages on our forums :</p>
	<p><a href="http://forums.neovos.com">Go to  forums</a></p>
</div>
