
<div class="MainForm">
	<h2>Neovos Sign in</h2>
	
	<?php if( $this->_hasMessages() ) : ?>
		<div class="Message">Please, correct the following errors :
		<ul>
			<?php foreach( $this->_getMessages() as $message ) : ?>
			<li><?php echo $message; ?></li>
			<?php endforeach ?>
		</ul>
		</div>
	<?php endif ?>

	<form method="post" action="Action_Portal_SignIn" class="SignInForm">
	<?php if( $this->_getReturn() ) : ?>
	<input type="hidden" name="return" value="<?php echo htmlentities($this->_getReturn()); ?>" />
	<?php endif ?>
	<input type="hidden" name="action" value="Core_UserAction" />
		<dl>
			<dt>Login:</dt>
			<dd><input type="text" name="login" value="<?php echo htmlentities($this->_getLogin()); ?>" />  <img src="" /></dd>
			<dt>Password:</dt>
			<dd><input type="password" name="password" value="" /></dd>
			<dt>Email:</dt>
			<dd><input type="text" name="email" value="<?php echo htmlentities($this->_getEmail()); ?>" />  <img src="" /></dd>
			<dt>&nbsp;</dt>
			<dd><input type="submit" value="Sign in" /></dd>
		</dl>
	</form>
	
	<div style="clear:both"></div>
</div>
