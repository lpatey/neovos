<div class="Panel">

<?php if( $this->_isActivated() ) : ?>

<h2>Account activated</h2>

<p>Your account has been activated successfully.</p>

<p>You can now access your account at the following url</p>

<p style="text-align: center"><a href="<?php echo $this->_getUser()->getPublicUrl(); ?>"><?php echo $this->_getUser()->getPublicUrl(); ?></a></p>

<?php else : ?>

<h2>Activation failed</h2>

<p>I'm sorry, your activation key is invalid or has expired.</p>

<?php endif ?>

</div>
