<div class="Layout">

<h3>Administration panel</h3>

<table cellspacing="0" class="Table">
	<thead>
	<tr>
		<th>#</th>
		<th>Login</th>
		<th>Creation</th>
		<th>Status</th>
	</tr>
	</thead>
	<tbody>
	<?php foreach($this->_getUsers() as $user) : ?>
	<tr>
		<td><?php echo $user->getUserId(); ?></td>
		<td><?php echo $user->getLogin(); ?></td>
		<td><?php echo $user->getCreationTime(); ?></td>
		<td><?php echo $user->getStatus(); ?></td>
	</tr>
	<?php endforeach ?>
	</tbody>
</table>

</div>
