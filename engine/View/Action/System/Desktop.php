<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
                      "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<meta name="robots" content="noindex,nofollow"> 
		<link rel="stylesheet" type="text/css" href="styles/default.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="styles/widgets/item.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="styles/widgets/window/window.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="styles/widgets/window/chooserwindow.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="styles/widgets/window/itemconflictwindow.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="styles/widgets/window/itemwindow.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="styles/widgets/window/publishwindow.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="styles/widgets/contextuable.css" media="screen" />
		<script type="text/javascript" src="scripts/jquery.js"></script>
		<script type="text/javascript" src="scripts/jquery.ui.js"></script>
		<script type="text/javascript" src="scripts/jquery.enhanced.js"></script>
		<script type="text/javascript" src="scripts/tools.js"></script>
		<script type="text/javascript" src="scripts/modal.js"></script>
		<script type="text/javascript" src="scripts/desktop.js"></script>
		<script type="text/javascript" src="scripts/user.js"></script>
		<script type="text/javascript" src="scripts/image.js"></script>
		<script type="text/javascript" src="scripts/system.js"></script>
		<!--[if IE]>
		<script type="text/javascript" src="scripts/trident.js"></script>
		<![endif]-->
		<!--[if !IE]><-->
		<script type="text/javascript" src="scripts/nontrident.js"></script>
		<!--><![endif]-->
		<script type="text/javascript" src="scripts/main.js"></script>
		<script type="text/javascript" src="scripts/modules/zoho.js"></script>
		<script type="text/javascript" src="scripts/modules/desktop.js"></script>
		<script type="text/javascript" src="scripts/widgets/plugin.js"></script>
		<script type="text/javascript" src="scripts/widgets/scheduler.js"></script>
		<script type="text/javascript" src="scripts/widgets/navigation.js"></script>
		<script type="text/javascript" src="scripts/widgets/contextuable.js"></script>
		<script type="text/javascript" src="scripts/widgets/sorter.js"></script>
		<script type="text/javascript" src="scripts/widgets/autoresizable.js"></script>
		<script type="text/javascript" src="scripts/widgets/renameable.js"></script>
		<script type="text/javascript" src="scripts/widgets/window/window.js"></script>
		<script type="text/javascript" src="scripts/widgets/humanize.js"></script> 
		<script type="text/javascript" src="scripts/widgets/history.js"></script>
		<script type="text/javascript" src="scripts/widgets/item.js"></script>
		<script type="text/javascript" src="scripts/widgets/itemcontainer.js"></script>
		<script type="text/javascript" src="scripts/widgets/window/itemwindow.js"></script>
		<script type="text/javascript" src="scripts/widgets/window/publishwindow.js"></script>
		<script type="text/javascript" src="scripts/widgets/window/processwindow.js"></script>
		<script type="text/javascript" src="scripts/widgets/window/chooserwindow.js"></script>
		<script type="text/javascript" src="scripts/widgets/window/itemconflictwindow.js"></script>
		<script type="text/javascript" src="scripts/lang/en.js"></script>
		<link rel="shortcut icon" type="image/png" href="images/favicon.png" />
		<title>Neovos [Aquila]</title>
	</head>
	<body>

		<div class="Template">
			<div class="Item">
				<div class="Content">
					<div class="Shadow"></div>
					<div class="Icon"><div><div><img src="images/mime/64/unknown.png" /></div></div></div>
					<div class="Name"></div>
					<div class="Size"></div>
				</div>
				<div class="Unfloat"></div>
			</div>

			<div class="Window">
				<div class="Shadow"></div>
				<div class="TitleBar">
					<div class="Title"><span class="Text"></span> <a class="I18n Feedback" href="mailto:feedback@neovos.com">window.title.sendFeedback</a></div>
					<div class="Tools">
						<span class="Reduce">-</span>
						<span class="Maximize">+</span>
						<span class="Close">x</span>
					</div>
				</div>
				<div class="Content">
				</div>
				<div class="SubWindow">
					<div class="Shadow"></div>
					<div class="Content">
					</div>
				</div>
			</div>
			
			<div class="PublishWindowContent">
				<form method="post">
				<div class="PublishOptions">
				<h2 class="I18n">publishWindow.options.title</h2>
				<dl>
					<dt class="I18n">publishWindow.options.shareName</dt>
					<dd><input type="text" name="name" /></dd>
					<dt><label><input type="checkbox" name="enabled" /><span class="I18n">publishWindow.options.password</span></label></dt>
					<dd><input type="password" name="password" disabled="disabled" /></dd>
				</dl>
				</div>
				<div class="Thumb IconView ItemContainer">
				</div>
				<div class="Buttons">
				<input type="button" class="I18n Cancel" value="publishWindow.buttons.cancel" />
				<input type="button" class="I18n Unpublish" value="publishWindow.buttons.unpublish" />
				<input type="submit" class="I18n Publish" value="publishWindow.buttons.publish" />
				</div>
				</form>
			</div>

			<div class="ChooserWindowContent">
				<div class="Message"></div>
				<ul></ul>
			</div>

			<div class="ItemConflictWindowMessage">
				<p class="I18n">itemConflict.message</p>
				<div class="Thumb IconView ItemContainer">
					<div class="Source"></div>
					<div class="Destination"></div>
				</div>
			</div>

			<li class="ChooserWindowOption">
				<div class="Icon">
					<img src="" />
				</div>
				<div class="Text">
					<h4></h4>
					<p></p>
				</div>
			</li>

			<div class="Progress Group">
				<h5 class="I18n">progressGroup.messages.waitingForProcess</h5>
				<div class="Progress Bar">
					<div class="Loading"></div>
				</div>
			</div>

			<div class="ItemWindowContent">
				<div class="ToolBar">
					<div class="Disabled Previous Button">
						<img src="images/previous.png" alt="Previous" />
						<span class="I18n">itemWindow.buttons.previous</span>
					</div>
					<div class="Disabled Next Button">
						<img src="images/next.png" alt="Next" />
						<span class="I18n">itemWindow.buttons.next</span>
					</div>
					<div class="Parent Button">
						<img src="images/parent.png" alt="Parent" />
						<span class="I18n">itemWindow.buttons.parent</span>
					</div>
					<div class="Select">
						<div class="Disabled Thumbs Button">
							<img src="images/view_thumb.png" alt="Icons" />
							<span class="I18n">itemWindow.buttons.thumbs</span>
						</div>
						<div class="Icons Button">
							<img src="images/view_icon.png" alt="Icons" />
							<span class="I18n">itemWindow.buttons.icons</span>
						</div>
						<div class="List Button">
							<img src="images/view_detailed.png" alt="List" />
							<span class="I18n">itemWindow.buttons.list</span>
						</div>
						<div class="Unfloat"></div>
					</div>
					<div class="Refresh Button">
						<img src="images/refresh.png" alt="Refresh" />
						<span class="I18n">itemWindow.buttons.refresh</span>
					</div>
					<div class="Desktop Button">
						<img src="images/desktop.png" alt="Desktop" />
						<span class="I18n">itemWindow.buttons.desktop</span>
					</div>
					<div class="Unfloat"></div>
				</div>
				<div class="ColumnContainer">
					<div class="Left Column">
						<h3 class="I18n">itemWindow.columns.parents</h3>
						<div class="Uni Select ItemContainer ListView">
						</div>
					
						<h3 class="I18n">itemWindow.columns.shares</h3>
						<div class="Uni Select ItemContainer ListView">
						</div>
					</div>
				
					<div class="Middle Column">
						<div class="Scrollable">
							<div class="Multi Select ItemContainer Thumb IconView">

							</div>
						</div>
					</div>
				
					<div class="Right Column">
						<h3 class="I18n">itemWindow.columns.preview</h3>
						<div class="Preview">
							<img src="images/mime/64/group.png" />
						</div>
						<h3 class="I18n">itemWindow.columns.attributes</h3>
						<dl class="Attributes">
						</dl>
					</div>
				</div>
			</div>

		</div>
	
		<div class="Multi Select Thumb ItemContainer IconView Selected"></div>

		<div class="WindowContainer"></div>
		
		<div id="MainBar">
		
			<div class="Left ShortCuts">
				<img class="Icon Slide" src="images/shortcuts/demo.png" />
				<img class="Icon Zoho" format="doc" src="images/shortcuts/app_writer.png" />
				<img class="Icon Zoho" format="xls" src="images/shortcuts/app_sheet.png" />
			</div>
		
			<div class="SearchBox">
				<input type="text" name="search" />
				<img src="images/search.png" alt="Search" />
			</div>
			
			<div class="Right ShortCuts">
				<img class="Icon Logout" src="images/shortcuts/logout.png" />
			</div>
			
			<div class="Preview">
				<div class="Shadow"></div>
			</div>
		</div>

		<div class="ModalContainer">
			<div class="Darkness"></div>
		</div>

	
	</body>
</html>
