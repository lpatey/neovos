<?php

require_once 'Core/Action/UserAction.php';

class Action_System_GetSynchronisationData extends Core_Action_UserAction {

	public function execute() {

		$return = array();
		$return['contents'] = $this->_getContents();
		$return['processes'] = $this->_getProcesses();
		$return['trash']['empty'] = $this->_isTrashEmpty();
		$return['user'] = array( 'user_id' => intval($this->_getUser()->getUserId()) );
		$return['time'] = $this->_getTime();

		return $return;
	}

	private function _getContents() {

		$contents = array();
		$params = $this->_getParam( 'contents' );
		foreach( (array) $params as $param ) {

			try {
				$content = $this->_action( 'Fs_GetContent', $param );
				if( $content['data']['inode'] ) {
					$contents[ $content['data']['inode'] ] = $content;
				}
			}
			catch( Core_Exception $e ) {
			}
		}
		return $contents;

	}

	private function _getProcesses() {
		return $this->_action( 'Ps_GetProcesses' );
	}
	
	private function _isTrashEmpty() {
	
		$path = '/' . Config::get( 'fs', 'dir', 'trash' );
		$path = $this->_getDistinctPath( $path );
		$pathArg = Core_Fs::toShellArg( $path );
		
		$command = `ls --almost-all --ignore=".neovos" $pathArg`;
		return count( explode( "\n", $command ) ) == 1;
	}

}

