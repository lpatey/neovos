<?php

require_once 'Core/Action.php';

class Action_System_Desktop extends Core_Action {

	public function execute() {

		$result = $this->_action( 'Portal_CheckLogin', array( 'login' => $_GET['login'] ) );
		if( $result['valid'] ) {
			die( $this->_action( 'Portal_404' ) );
		}

		$this->_setViewEnabled( true );
	}


}

