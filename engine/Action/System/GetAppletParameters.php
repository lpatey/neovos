<?php

require_once 'Core/Action/UserAction.php';
require_once 'Config.php';

class Action_System_GetAppletParameters extends Core_Action_UserAction {

	public function execute() {
		$inode = Config::get( 'fs', 'inode' );
		return array(
			'dav.host' => 'dav.' . Config::get( 'server', 'main' ),
			'dav.login' => $this->_getUser()->getLogin(),
			'dav.password' => $this->_getUser()->getPassword(),
			'fs.desktop.inode' => $inode['desktop'],
			'fs.root.inode' => $inode['root'],
			'fs.myComputer.inode' => $inode['myComputer'],
			'sys.log' => 3
		);
	}

}
