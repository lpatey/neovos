<?php

require_once 'Core/Action/UserAction.php';

class Action_Ps_GetProcesses extends Core_Action_UserAction {

	public function execute() {

		$processDir = $this->_getUser()->getRoot() . '/' . Config::get( 'fs', 'dir', 'process' );
		
		if( !is_dir( $processDir ) ) {
			mkdir( $processDir, 0770, true );
		}

		$dh = opendir( $processDir );

		$processes = array();
		$processes['time'] = $this->_getTime();
		$processes['processes'] = array();

		while( $item = readdir( $dh ) ) {

			if( $item[0] != '.' ) {

				$path = $processDir . '/' . $item;
				while( !($content = file_get_contents( $path )) && file_exists( $path ) );

				if( $content ) {
					$processes['processes'][] = unserialize( $content );
				}
			}
		}


		closedir( $dh );

		return $processes;

	}

}
