<?php

require_once 'Core/Action/CliAction.php';
require_once 'Core/Fs.php';

class Action_Cli_CreateThumbs extends Core_Action_CliAction {

	public function execute() {
		$path = $this->_getParam( 'path' );

		Core_Fs::checkFolder( $path );

		foreach( Core_Fs::getRawFileList( $path ) as $line ) {
			$this->_createThumb( Core_Fs::fromShell($line), $path );
		}

	}

	private function _createThumb( $name, $path ) {

		$thumbDir = Config::get( 'fs', 'dir', 'thumb' );

		$filePath = $path . '/' . $name;
		$mimeType = Core_Fs::getMimeType( $filePath );
		$infos = explode( '/', $mimeType );
		$filePathArg = Core_Fs::toShellArg($filePath);
		$thumbPath = $path . '/' . $thumbDir . '/' . $name . '.png';
		$thumbPathArg = Core_Fs::toShellArg($thumbPath);
		$user = Config::get( 'server', 'user' );

		if( file_exists( $thumbPath ) && filemtime( $thumbPath ) > filemtime( $filePath ) ) {
			return true;
		}

		if( !$mimeType ) {
			return false;
		}

		$key = Core_Fs::toShellArg( '/desktop/gnome/thumbnailers/' . str_replace( '/', '@', $mimeType ) . '/command' );

		$value = Core_Fs::fromShell( `gconftool -g $key` );

		if( $value ) {
			$command = strtr( $value, array( '%i' => $filePathArg, '%o' => $thumbPathArg, '%u' => $filePathArg, '%s' => '70' ) );
			if( $infos[1] == 'pdf' ) {
				echo $command . "\n";
			}
			echo `$command 2>&1`;
			return true;
		}
		
		if( $infos[0] == 'image' ) {
			echo `convert -thumbnail x64 $filePathArg $thumbPathArg 2>&1`;
			return true;
		}
		
		/*if( $infos[1] == 'pdf' ) {
			echo `convert -thumbnail x64 {$filePathArg}[0] $thumbPathArg 2>&1`;
			return true;
		}*/

		if( $mimeType == 'application/x-ms-dos-executable' ) {
			$tempName = tempnam( '/tmp', 'ico' );
			$tempNameArg = Core_Fs::toShellArg( $tempName );
			echo `wrestool --extract --type=group_icon $filePathArg --output=$tempNameArg 2>&1`;
			$icons = explode( "\n", Core_Fs::fromShell( `icotool -l $tempNameArg` ) );

			$choice = array( 'palette-size' => 0, 'width' => 0 );
			foreach( $icons as $icon ) {
				$icon = $this->_extractIconInfos( $icon );
				if( $icon['width'] > $choice['width'] ) {
					$choice = $icon;
					continue;
				}
				if( $icon['width'] == $choice['width'] && $icon['palette-size'] > $choice['palette-size'] ) {
					$choice = $icon;
					continue;
				}
			}
			if( !$choice['index'] ) {
				return false;
			}

			$index = intval($choice['index']);
			echo `convert -thumbnail x64 ico:{$tempNameArg}[$index] $thumbPathArg 2>&1`;
			unlink( $tempName );
			chown( $thumbPath, $user );

			return true;
		}

		if( $infos[0] == 'video' ) {
			$fileInfos = pathinfo( $name );
			$ext = $fileInfos['extension'];
			$ext = ($ext ? '.' . $ext : $ext).'.png';
			echo `mtn $filePathArg -i -t -c 1 -r 1 -h 0 -w 70 -o "$ext" -O $thumbPathArg 2>&1`;
			chown( $thumbPath, $user );

			return true;
		}

		return false;
	}

	private function _extractIconInfos( $line ) {
		preg_match_all( '/\-{2}([a-z-]+)=(\S+)/is', $line, $matches, PREG_SET_ORDER );
		$return = array();
		foreach( $matches as $match ) {
			$return[ $match[1] ] = intval($match[2]);
		}
		return $return;
	}

}


