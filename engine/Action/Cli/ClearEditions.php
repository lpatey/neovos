<?php

require_once 'Core/Action/CliAction.php';

class Action_Cli_ClearEditions extends Core_Action_CliAction {

	public function execute() {

		$query = 'DELETE FROM editions WHERE DATE_ADD(`time`, INTERVAL 1 DAY) < NOW()';

		$this->_getDb()->query( $query );
	}

}
