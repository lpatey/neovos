<?php

require_once 'Core/Action/CliAction.php';

class Action_Cli_Nightly extends Core_Action_CliAction {

	public function execute() {

		$this->_action( 'Cli_ClearEditions' );
	}

}
