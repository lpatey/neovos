<?php

require_once 'Core/Action/UserAction.php';

class Action_User_Logout extends Core_Action_UserAction {

	public function execute() {
		
		session_start( $_GET['login'] );
		unset($_SESSION['user']);
		session_write_close();
		
		header( 'Location: ../' );
		exit(0);
	}
}
