<?php

require_once 'Core/Process/FileProcess.php';
require_once 'Core/Action/UserAction.php';
require_once 'Core/Fs.php';

class Action_Fs_Delete extends Core_Action_UserAction {

	private $_process;

	public function execute() {

		Core_Fs::checkRoot( $this->_getPath( '/' ) );
		
		$path = '/' . Config::get( 'fs', 'dir', 'trash' );
		$neovosDir = Config::get( 'fs', 'dir', 'neovos' );
		$pid = $this->_getParam( 'pid' );

		$process = new Core_Process_FileProcess( $pid );
		$this->_process = $process;


		if( $this->_hasParam( 'files' ) ) {
			$files = $this->_getParam( 'files' );
		}
		else {
			$files = Core_Fs::getRawFileList( $this->_getDistinctPath( $path, false ) );
		}

		$process->setFiles( $path, $files );

		foreach( $files as $file ) {
			$this->_delete( $path . '/' . $file );
		}

		$time = $this->_getTime();
		return array( 'pid' => $pid, 'time' => $time );

	}

	private function _delete( $source ) {

		$path = $this->_getDistinctPath( $source, false );

		$infos = pathinfo( $path );

		if( is_dir( $path ) && !is_link( $path ) ) {
			$dh = opendir( $path );
			if( !$dh ) {
				return;
			}
			while( $item = readdir( $dh ) ) {
				if( $item != '.' && $item != '..'  ) {
					$this->_delete( $source . '/' . $item );
				}
			}
			closedir( $dh );
			rmdir( $path );
		}
		else {
			$this->_process->addProgress( $path, filesize( $path ) );
			unlink( $path );
		}

	}
}


