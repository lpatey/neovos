<?php

require_once 'Core/Action/UserAction.php';

class Action_Fs_Rename extends Core_Action_UserAction {

	public function execute() {
		
		$path = $this->_getParam( 'path' );
		$dest = $this->_getParam( 'dest' );
		$pid = $this->_getParam( 'pid' );

		if( $this->_hasParam( 'files' ) ) {
			foreach( $this->_getParam( 'files' ) as $file ) {
				$this->_rename( $path . '/' . $file, $dest . '/' . $file );
			}
		}
		else {
			$this->_rename( $path, $dest );
		}

		$time = $this->_getTime();
		return array( 'pid' => $pid, 'time' => $time );

	}

	private function _rename( $source, $dest ) {

		$path = $this->_getDistinctPath( $source, false );

		$name = Core_Fs::basename( $dest );
		$dest = $this->_getPath( Core_Fs::dirname($dest) ) . '/' . $name;

		if( !$name || $name == Config::get( 'fs', 'dir', 'neovos' ) ) {
			throw new Core_Exception( 'Desktop.error.invalidFilename' );
		}

		if( file_exists( $dest ) ) {
			throw new Core_Exception( 'Desktop.error.destinationExists' );
		}

		$thumbPath = Core_Fs::getThumbnailPath( $path );
		$thumbDest = Core_Fs::getThumbnailPath( $dest );

		if( file_exists( $thumbPath ) ) {
			rename( $thumbPath, $thumbDest );
		}

		rename( $path, $dest );


	}
}


