<?php

require_once 'Action/Fs/Archive/GetContent.php';

class Action_Fs_Zip_GetContent extends Action_Fs_Archive_GetContent {

	protected function _getCommand( $pathArg ) {
		return `zipinfo --h --t -lm $pathArg`;
	}

	protected function _extractEntry( $line ) {
		preg_match( '/(?#
			perm)([^\s]+) +(?#
			a)([^\s]+) +(?#
			b)([^\s]+) +(?#
			size)([0-9]+) +(?#
			c)([^\s]+) +(?#
			compression)([^\s]+) +(?#
			d)([^\s]+) +(?#
			atime)([^\s]+ [^\s]+) +(?#
			name)([^\n]+)/s', $line, $matches );
		
		$return = array();
		$return['size'] = $matches[4];
		$return['atime'] = strtotime($matches[8]);
		$return['directory'] = $matches[1][0] == 'd';
		$return['attributes']['fs.attributes.compression'] = $matches[6];
		
		return array( $matches[9], $return );
	}
}
