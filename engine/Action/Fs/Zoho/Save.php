<?php

require_once 'Core/Action.php';

class Action_Fs_Zoho_Save extends Core_Action {

	public function execute() {

		$query = 'SELECT *
			FROM editions
			WHERE `edition_id` = :edition_id
			AND `key` = :key';

		$stmt = $this->_getDb()->prepare( $query );

		$id = $this->_getParam( 'id' );
		list($edition_id,$key) = explode( '-', $id );

		$stmt->execute( array(
			':edition_id' => $edition_id,
			':key' => $key
		) );

		if( $stmt->rowCount() == 0 ) {
			throw new Core_Exception( 'Invalid file' );
		}

		$edition = $stmt->fetch( PDO::FETCH_ASSOC );

		$query = 'SELECT *
			FROM users
			WHERE user_id = :user_id';

		$stmt = $this->_getDb()->prepare( $query );
		
		$stmt->execute( array( 
			':user_id' => $edition['user_id']
		) );

		$user = new Bean_User( $stmt->fetch( PDO::FETCH_ASSOC ) );

		$path = $user->getRoot() . $edition['path']; 
		
		move_uploaded_file( $_FILES['content']['tmp_name'], $path );

	}
}
