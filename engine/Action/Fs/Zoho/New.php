<?php

require_once 'Core/Action/UserAction.php';
require_once 'Core/Fs.php';

class Action_Fs_Zoho_New extends Core_Action_UserAction {

	private $_mimes = array(
		'doc' => 'application/msword',
		'xls' => 'application/excel'
	);

	public function execute() {
	
		$format = $this->_getParam( 'format' );
		$name = $this->_getParam( 'name' ) . '.' . $format;
		
		$path = $this->_getPath( $this->_getParam( 'path' ) );
		
		$name = Core_Fs::getAvailableName( $name, $path );

		return $this->_action( 'Fs_Zoho_Open', array(
			'mime' => $this->_mimes[ $format ],
			'path' => $this->_getParam( 'path' ),
			'name' => $name
		) );
	}
}
