<?php

require_once 'Core/Action/UserAction.php';

class Action_Fs_Zoho_Open extends Core_Action_UserAction {

	private $_urls = array(
		'writer' => 'http://export.writer.zoho.com/remotedoc.im',
		'sheet' => 'http://sheet.zoho.com/remotedoc.im',
		'show' => 'http://show.zoho.com/remotedoc.im',
		'viewer' => 'http://viewer.zoho.com/api/view.do'
	);
	
	private $_apps = array(
		'doc' => 'writer',
		'docx' => 'writer',
		'rtf' => 'writer',
		'txt' => 'writer',
		'html' => 'writer',
		'odt' => 'writer',
		'xls' => 'sheet',
		'xlsx' => 'sheet',
		'ods' => 'sheet',
		'csv' => 'sheet',
		'ppt' => 'show',
		'pps' => 'show',
		'odp' => 'show',
		'pdf' => 'viewer'
	);
	
	private $_formats = array(
		'application/msword' => 'doc',
		'application/vnd.openxmlformats-officedocument.wordprocessingml.document' => 'docx',
		'application/rtf' => 'rtf',
		'text/html' => 'html',
		'text/plain' => 'txt',
		'application/vnd.oasis.opendocument' => 'odt',
		'application/vnd.ms-excel' => 'xls',
		'application/vnd.ms-powerpoint' => 'ppt',
		'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' => 'xlsx',
		'application/vnd.oasis.opendocument.spreadsheet' => 'ods',
		'text/csv' => 'csv',
		'application/vnd.ms-powerpoint' => 'ppt',
		'application/vnd.openxmlformats-officedocument.presentationml.presentation' => 'pptx',
		'application/vnd.oasis.opendocument.presentation' => 'odp',
		'application/pdf' => 'pdf'
	);

	public function execute() {
	
		$relativePath = $this->_getParam( 'path' );
		$path = $this->_getPath( $relativePath );
		
		if( $this->_hasParam( 'name' ) ) {
			$relativePath .= '/' . basename( $this->_getParam( 'name' ) );
			$path .= '/' . basename( $this->_getParam( 'name' ) );
		}
		
		if( !file_exists( $path ) ) {
			$mime = $this->_getParam( 'mime' );
		}
		else {
			$mime = Core_Fs::getMimeType( $path );
		}
		
		$format = $this->_formats[ $mime ];
		
		if( !$format ) {
			throw new Core_Exception( 'Modules.Zoho.error.formatNotFound' );
		}

		$files = array();
		$posts = array(
			'apikey' => Config::get( 'zoho', 'apiKey' ),
			'skey' => Config::get( 'zoho', 'secretKey' )
		);
		
		if( $this->_apps[ $format ] == 'viewer' ) {

			$files[] = array(
				'name' => 'file',
				'type' => $mime,
				'file' => $path
			);

		}
		else {

			$key = md5( $path . microtime() );
		
			$query = 'INSERT INTO editions
				SET
					`user_id` = :user_id,
					`path` = :path,
					`key` = :key,
					`time` = NOW()';

			$params = array(
				':user_id' => $this->_getUser()->getUserId(),
				':path' => $relativePath,
				':key' => $key );

			$stmt = $this->_getDb()->prepare( $query );
			$stmt->execute( $params );

			$edition_id = $this->_getDb()->lastInsertId();
			$officialKey = $edition_id . '-' . $key;		
			
			if( file_exists( $path ) ) {
				$files[] = array(
					'name' => 'content',
					'type' => $mime,
					'file' => $path
				);
			}
			
			
			$posts[ 'format' ] = $format;
			$posts[ 'lang' ] = $this->_getUser()->getLang();
			$posts[ 'output' ] = 'url';
			$posts[ 'filename' ] = basename( $path );
			$posts[ 'saveurl' ] = $this->_getUser()->getPublicUrl() . '/Action_Fs_Zoho_Save';
			$posts[ 'id' ] = $officialKey;
		}

		$result = http_post_fields( $this->_urls[ $this->_apps[ $format ] ], $posts, $files );

		if( !preg_match( '/URL(?:=|"\s*:\s*")([^\n"]+)/i', $result, $matches ) ) {
			throw new Core_Exception( 'Modules.Zoho.error.invalidFile' );
		}

		return array(
			'url' => $matches[1]
		);
	}
	
}
