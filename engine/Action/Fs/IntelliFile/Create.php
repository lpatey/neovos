<?php

require_once 'Core/Action/UserAction.php';
require_once 'Core/Module/IntelliFile.php';
require_once 'Core/Fs.php';

class Action_Fs_IntelliFile_Create extends Core_Action_UserAction {

	public function execute() {
	
		$path = $this->_getPath( $this->_getParam( 'path' ) );
		$urls = $this->_getParam( 'urls' );

		foreach( $urls as $url ) {
			$data = Core_Module_IntelliFile::getData( $url );
			
			$data['name'] = Core_Fs::getAvailableName( $data['name'], $path );
			
			file_put_contents( $path . '/' . $data['name'], json_encode( $data ) );
		}
	}

}
