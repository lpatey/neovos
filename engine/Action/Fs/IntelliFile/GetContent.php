<?php

require_once 'Core/Module/IntelliFile.php';

class Action_Fs_IntelliFile_GetContent extends Core_Action_UserAction {

	public function execute() {
		
		$root = realpath( $this->_getUser()->getRoot() );
		
		if( !$root ) {
			return array( 'error' => 'Desktop.invalidPath' );
		}

		$path = $this->_getParam( 'path' );
		if( !strpos( $path, '!/' ) ) {
			$path .= '!/';
		}
		$prefix = substr( $path, strpos( $path, '!/' ) + 2 );
		$path = $this->_getPath( $path );

		$data = json_decode( file_get_contents( $path ), true );
		$url = $data['url'] . str_replace( '%2F', '/', rawurlencode($prefix ? $prefix . '/' : '') );

		$pathArg = Core_Fs::toShellArg( $path );
		$inode = $this->_getInode( $path );

		$return = array();
		$return['time'] = $this->_getTime();
		$return['data']['inode'] = $inode . ( $prefix ? '-' . md5( $prefix ) : '' );
		
		$return['content'] = array();
		$return['status'] = array();


		$command = file_get_contents( $url );
		$hash = md5( $command );
		$thumbs = md5('nothumb');

		$sameHash = $hash == $this->_getParam( 'hash' );
		$sameThumbs = $thumbs == $this->_getParam( 'thumbs' );
		$attributes = $this->_getParam( 'attributes' ) == 'true';

		if( $sameHash && $sameThumbs && $attributes ) {
			return $return;
		}
		else if( !$sameHash ) {
			$return['status']['hash'] = $hash;
			$return['status']['thumbs'] = $thumbs;
			$return['status']['attributes'] = $attributes = true;
			$this->_send( 'CreateThumbs', array( 'path' => $path ) );
		}
		else if( !$attributes ) {
			$return['status']['attributes'] = true;
		}
		else if( !$sameThumbs ) {
			$return['status']['thumbs'] = $thumbs;
		}
		
		preg_match_all( '/<a href="(?![a-z]+:\/\/)([^\/\?][^"]+)"/is', $command, $lines, PREG_SET_ORDER );

		foreach( $lines as $line ) {
			
			$array = $this->_getEntry( $line[1], $url, $inode );
			if( $array ) {
				$return['content'][] = $array;
			}
		}


		return $return;
	}

	private function _getEntry( $name, $parentUrl, $parentInode ) {

		$name = html_entity_decode( $name );
		$url = $parentUrl . $name;

		$return = array();
		$return['inode'] = $parentInode . '-' . md5($url);
		$return['context'] = 'remote';
		$return = array_merge( $return, Core_Module_IntelliFile::getData( $url ) );
		$return['name'] = $name;
		if( substr( $name, -1 ) == '/' ) {
			$return['name'] = substr( $name, 0, -1 );
		}
		$return['name'] = urldecode( $return['name'] );

		return $return;
	}
}
