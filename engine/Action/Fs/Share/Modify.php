<?php

require_once 'Core/Action/UserAction.php';

class Action_Fs_Share_Modify extends Core_Action_UserAction {

	public function execute() {

		$shareId = $this->_getParam( 'share_id' );

		$params = array(
			':user_id' => $this->_getUser()->getUserId(),
			':share_id' => $shareId,
			':name' => $this->_getParam( 'name' )
		);
		
		$password = $this->_getParam( 'password' );
		$passwordField = '';
		if( $password != 'keep-password' ) {
			$passwordField = 'password = :password,';
			$params[ ':password' ] = $password;
		}
		
		$query = 'UPDATE shares
			SET
				' . $passwordField . '
				name = :name
			WHERE share_id = :share_id
			AND user_id = :user_id';
				
		$stmt = $this->_getDb()->prepare( $query );
		$stmt->execute( $params );

		return array(
			'share_id' => $shareId
		);
	}
}
