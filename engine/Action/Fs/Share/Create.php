<?php

require_once 'Core/Action/UserAction.php';
require_once 'Core/Fs.php';

class Action_Fs_Share_Create extends Core_Action_UserAction {

	public function execute() {

		$path = $this->_getDistinctPath( $this->_getParam( 'path' ) );
		$shareDir = Config::get( 'fs', 'dir', 'share' );
		
		$sharePath = $this->_getUser()->getRoot() . '/' . $shareDir;
		
		$inode = $this->_getInode( $path );
		
		$query = 'INSERT INTO shares
			SET
				user_id = :user_id,
				name = :name,
				inode = :inode,
				password = :password,
				creation_time = NOW()';
				
		$password = $this->_getParam( 'password' );
				
		$stmt = $this->_getDb()->prepare( $query );
		$stmt->execute( array(
			':user_id' => $this->_getUser()->getUserId(),
			':name' => $this->_getParam( 'name' ),
			':inode' => $inode,
			':password' => ($password ? sha1($password) : null)
		) );
		
		$share_id = $this->_getDb()->lastInsertId();
		
		$dest = $sharePath . '/' . $share_id;
		
		rename( $path, $dest );
		symlink( $dest, $path );
		
		$sharesDir = Config::get( 'fs', 'dir', 'shares' );
		$sharesPath = $this->_getPath( $sharesDir );
		$name = $this->_getParam( 'name' );

		symlink( $dest, $sharesPath . '/' . Core_Fs::getAvailableName($name, $sharesPath) );
		
		return $this->_getShareById( $share_id );
	}
}
