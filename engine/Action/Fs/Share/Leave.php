<?php

require_once 'Core/Action/UserAction.php';
require_once 'Core/Fs/Share.php';

class Action_Fs_Share_Leave extends Core_Action_UserAction {

	public function execute() {
	return;
		if( $this->_hasParam( 'path' ) ) {
			$path = rtrim($this->_getPath( $this->_getParam( 'path' ), false ), '/' );
			while( is_link( $path ) ) {
				$dest = readlink( $path );
				if( !preg_match( '/([0-9]+)\/?$/is', $dest, $matches ) ) {
					return array( 'shared' => false );
				}
				if( $this->_removeShare( $matches[1] ) != array( 'ok' => 'ok' ) ) {
					return array( 'shared' => true, 'shares' => Core_Fs_Share::getShares( $path ) );
				}
			}
			return array( 'shared' => false );
		}
	}
	
	private function _removeShare( $shareId ) {
	
		
		$userDir = $this->_getUser()->getRoot();
	
		$query = 'DELETE
			FROM shares
			WHERE user_id = :user_id
			AND share_id = :share_id';
			
		$stmt = $this->_getDb()->prepare( $query );
		
		$stmt->execute( array(
			':share_id' => $shareId,
			':user_id' => $this->_getUser()->getUserId()
		) );

		if( $stmt->rowCount() == 0 ) {
			throw new Core_Exception( 'Share.invalidShare' );
		}

		$shareDir = Config::get( 'fs', 'dir', 'share' );
		$sharePath = $userDir . '/' . $shareDir;
		$dest = $sharePath . '/' . $shareId;
		
		$dh = opendir( $sharePath );
		while( $item = readdir( $dh ) ) {
			if( ($item != $shareId) 
				&& is_numeric( $item ) 
				&& is_link( $sharePath . '/' . $item) ) {
				
				$tmpSource = $sharePath . '/' . $item;
				$subDest = readlink( $tmpSource );
				preg_match( '/[0-9]+$/is', $subDest, $match );
				if( $match[0] == $shareId ) {
					if( is_link( $dest ) ) {
						$tmpDest = readlink( $dest );
						unlink( $tmpSource );
						symlink( $tmpDest, $tmpSource );
						unlink( $dest );
					}
					else {
						unlink( $tmpSource );
						rename( $dest, $tmpSource );
					}
					closedir( $dh );
					return array( 'ok' => 'ok' );
				}
			}
		}
		
		closedir( $dh );
		
		$userDirArg = Core_Fs::toShellArg( $userDir );
		$destArg = Core_Fs::toShellArg( $dest );
		
		$result = `find $userDirArg -lname $destArg 2>&1`;
		$result = current(explode( "\n", $result ));
		
		if( $result ) {
			unlink( $result );
			rename( $dest, $result );
		}
		
		return array( 'ok' => 'ok' );
	}

}
