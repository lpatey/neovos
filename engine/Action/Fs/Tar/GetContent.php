<?php

require_once 'Action/Fs/Archive/GetContent.php';

class Action_Fs_Tar_GetContent extends Action_Fs_Archive_GetContent {

	protected function _getCommand( $pathArg ) {
		return `tar -tvf $pathArg`;
	}

	protected function _extractEntry( $line ) {
		preg_match( '/(?#
			perm)([^\s]+) +(?#
			a)([^\s]+) +(?#
			size)([0-9]+) +(?#
			atime)([^\s]+ [^\s]+) +(?#
			name)([^\n]+)/s', $line, $matches );

		$return = array();
		$return['size'] = $matches[3];
		$return['atime'] = strtotime($matches[4]);
		$return['directory'] = $matches[1][0] == 'd';
		
		return array( $matches[5], $return );
	}
}
