<?php

class Action_Fs_Rar_GetContent extends Core_Action_UserAction {

	public function execute() {
		
		$root = realpath( $this->_getUser()->getRoot() );
		
		if( !$root ) {
			return array( 'error' => 'Desktop.invalidPath' );
		}

		$path = $this->_getParam( 'path' );
		if( !strpos( $path, '!/' ) ) {
			$path .= '!/';
		}
		$prefix = substr( $path, strpos( $path, '!/' ) + 2 );
		$path = $this->_getPath( $path );

		$pathArg = Core_Fs::toShellArg( $path );
		$inode = $this->_getInode( $path );

		$return = array();
		$return['time'] = $this->_getTime();
		$return['data']['inode'] = $inode . ( $prefix ? '-' . md5( $prefix ) : '' );
		
		$return['content'] = array();
		$return['status'] = array();

		$command = `unrar v $pathArg`;
		$hash = md5( $command );
		$thumbs = md5('nothumb');

		$sameHash = $hash == $this->_getParam( 'hash' );
		$sameThumbs = $thumbs == $this->_getParam( 'thumbs' );
		$attributes = $this->_getParam( 'attributes' ) == 'true';

		if( $sameHash && $sameThumbs && $attributes ) {
			return $return;
		}
		else if( !$sameHash ) {
			$return['status']['hash'] = $hash;
			$return['status']['thumbs'] = $thumbs;
			$return['status']['attributes'] = $attributes = true;
			$this->_send( 'CreateThumbs', array( 'path' => $path ) );
		}
		else if( !$attributes ) {
			$return['status']['attributes'] = true;
		}
		else if( !$sameThumbs ) {
			$return['status']['thumbs'] = $thumbs;
		}
		
		$lines = explode( "\n", $command );
		for( $i=0; $i<8; $i++ ) {
			array_shift( $lines );
		}
		for( $i=0; $i<4; $i++ ) {
			array_pop( $lines );
		}

		for( $i=0; $i<count($lines); $i+=2 ) {
			
			$array = $this->_getEntry( $lines[$i], $lines[$i+1], $prefix, $inode );
			if( $array ) {
				$return['content'][] = $array;
			}
		}


		return $return;
	}

	private function _getEntry( $fullPath, $line, $prefix, $parentInode ) {

		$matches = $this->_getLineMatches( $line );

		$path = $fullPath;

		if( substr( $path, 0, mb_strlen($prefix) ) != $prefix  ) {
			return array();
		}
		$path = substr( $path, mb_strlen($prefix) );

		if( (strpos( $path, '/' ) !== false) && substr( $path, strpos( $path, '/' ) + 1 ) ) {
			return array();
		}

		$path = substr( $path, -1, 1 ) == '/' ? substr( $path, 0, -1 ) : $path;

		$return = array();
		$return['name'] = $path;
		$return['inode'] = $parentInode . '-' . md5($fullPath);
		$return['size'] = $matches[1];
		$return['atime'] = strtotime($matches[4]);
		$return['directory'] = $matches[5][1] == 'D';
		$return['thumb'] = false;
		$return['mime'] = $return['directory'] ? 'application/x-darchive' : 'application/x-farchive';
		$return['context'] = 'remote';
		$return['attributes']['fs.attributes.compression'] = $matches[3];

		return $return;
	}

	private function _getLineMatches( $line ) {
		preg_match( '/ +(?#size)([^\s]+) +(?#packed)([^\s]+) +(?#compression)([^\s]+) +(?#atime)([^\s]+ [^\s]+) +(?#props)([^\s]+)/s', $line, $match );
		return $match;
	}
}
