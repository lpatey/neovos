<?php

require_once 'Core/Process/FileProcess.php';
require_once 'Core/Action/UserAction.php';
require_once 'Core/Fs.php';

class Action_Fs_Download extends Core_Action_UserAction {

	private $_process;

	public function execute() {

		$path = $this->_getPath( $this->_getParam( 'path' ) );
		
		$mime = Core_Fs::getMimeType( $path );
		
		header( 'Content-type: ' . $mime );
		header('Content-Disposition: attachment; filename="' . $this->_getValidName( $path ) . '"');
		echo file_get_contents( $path );
		exit(0);
	}
	
	private function _getValidName( $path ) {
		$name = Core_Fs::basename( $path );
		return addslashes($name);
	}

}
