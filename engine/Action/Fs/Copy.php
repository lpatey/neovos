<?php

require_once 'Core/Process/FileProcess.php';
require_once 'Core/Action/UserAction.php';

class Action_Fs_Copy extends Core_Action_UserAction {

	private $_process;

	public function execute() {

		$pid = $this->_getParam( 'pid' );
		$process = new Core_Process_FileProcess( $pid );
		$this->_process = $process;
		
		$path = $this->_getParam( 'path' );
		$dest = $this->_getParam( 'dest' );

		$files = array();
		if( $this->_hasParam( 'files' ) ) {
			$files = $this->_getParam( 'files' );
		}

		$process->setFiles( $this->_getPath( $path ), $files );

		if( $this->_hasParam( 'files' ) ) {
			foreach( $this->_getParam( 'files' ) as $file ) {
				$this->_copy( $path . '/' . $file, $dest . '/' . $file );
			}
		}
		else {
			$this->_copy( $path, $dest );
		}

		unset( $process );

		$time = $this->_getTime();
		return array( 'pid' => $pid, 'time' => $time );

	}

	private function _copy( $source, $dest ) {
		
		$path = $this->_getDistinctPath( $source );

		$name = basename( $dest );
		$destPath = $this->_getPath( dirname($dest) ) . '/' . $name;

		$neovosDir = Config::get( 'fs', 'dir', 'neovos' );

		if( !$name || $name == $neovosDir ) {
			throw new Core_Exception( 'Desktop.error.invalidFilename' );
		}

		if( file_exists( $dest ) ) {
			throw new Core_Exception( 'Desktop.error.destinationExists' );
		}

		if( is_dir( $path ) ) {
			mkdir( $destPath );
			$dh = opendir( $path );
			if( !$dh ) {
				return;
			}

			while( $item = readdir( $dh ) ) {
				if( $item != '.' && $item != '..' /*&& $item != $neovosDir*/ ) {
					$this->_copy( $source . '/' . $item, $dest . '/' . $item );
				}
			}
			closedir( $dh );
		}
		else {

			
			/*$thumbPath = Core_Fs::getThumbnailPath( $path );
			$thumbDest = Core_Fs::getThumbnailPath( $destPath );

			if( file_exists( $thumbPath ) ) {
				copy( $thumbPath, $thumbDest );
			}*/

			$fhSource = fopen( $path, 'r' );
			if( !$fhSource ) {
				return;
			}

			$fhDest = fopen( $destPath, 'w' );
			if( !$fhDest ) {
				return;
			}
			
			while( !feof( $fhSource ) ) {
				$data = fread( $fhSource, 1024 );
				$dataLength = fwrite( $fhDest, $data );
				$this->_process->addProgress( $path, $dataLength );
			}

			fclose( $fhSource );
			fclose( $fhDest );

			//copy( $path, $destPath );
		}

	}
}


