<?php

abstract class Action_Fs_Archive_GetContent extends Core_Action_UserAction {

	protected abstract function _getCommand( $path );
	protected abstract function _extractEntry( $line );
	

	public function execute() {
		
		$root = realpath( $this->_getUser()->getRoot() );
		
		if( !$root ) {
			return array( 'error' => 'Desktop.invalidPath' );
		}

		$path = $this->_getParam( 'path' );
		if( !strpos( $path, '!/' ) ) {
			$path .= '!/';
		}
		$prefix = substr( $path, strpos( $path, '!/' ) + 2 );
		$path = $this->_getPath( $path );

		$pathArg = Core_Fs::toShellArg( $path );
		$inode = $this->_getInode( $path );

		$return = array();
		$return['time'] = $this->_getTime();
		$return['data']['inode'] = $inode . ( $prefix ? '-' . md5( $prefix ) : '' );
		
		$return['content'] = array();
		$return['status'] = array();

		$command = $this->_getCommand( $pathArg );
		$hash = md5( $command );
		$thumbs = md5('nothumb');

		$sameHash = $hash == $this->_getParam( 'hash' );
		$sameThumbs = $thumbs == $this->_getParam( 'thumbs' );
		$attributes = $this->_getParam( 'attributes' ) == 'true';

		if( $sameHash && $sameThumbs && $attributes ) {
			return $return;
		}
		else if( !$sameHash ) {
			$return['status']['hash'] = $hash;
			$return['status']['thumbs'] = $thumbs;
			$return['status']['attributes'] = $attributes = true;
			$this->_send( 'CreateThumbs', array( 'path' => $path ) );
		}
		else if( !$attributes ) {
			$return['status']['attributes'] = true;
		}
		else if( !$sameThumbs ) {
			$return['status']['thumbs'] = $thumbs;
		}

		foreach( explode( "\n", $command ) as $line ) {
			if( !$line ) {
				continue;
			}
			$array = $this->_getEntry( $line, $prefix, $inode );
			if( $array ) {
				$return['content'][] = $array;
			}
		}


		return $return;
	}

	private function _getEntry( $line, $prefix, $parentInode ) {

		list($fullPath, $return) = $this->_extractEntry( $line );

		if( substr( $fullPath, 0, mb_strlen($prefix) ) != $prefix  ) {
			return array();
		}
		$path = substr( $fullPath, mb_strlen($prefix) );

		if( (strpos( $path, '/' ) !== false) && substr( $path, strpos( $path, '/' ) + 1 ) ) {
			return array();
		}

		$path = substr( $path, -1, 1 ) == '/' ? substr( $path, 0, -1 ) : $path;
		if( !$path ) {
			return array();
		}

		$return['name'] = $path;
		$return['inode'] = $parentInode . '-' . md5($fullPath);
		$return['thumb'] = false;
		$return['mime'] = $return['directory'] ? 'application/x-darchive' : 'application/x-farchive';
		$return['context'] = 'remote';

		return $return;
	}

}
