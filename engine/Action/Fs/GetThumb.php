<?php

require_once 'Core/Action/UserAction.php';

class Action_Fs_GetThumb extends Core_Action_UserAction {

	public function execute() {
		
		$path = $this->_getPath( $this->_getParam( 'path' ), true );

		header( 'Content-Type: image/png' );
		
		$thumbPath = Core_Fs::getThumbnailPath( $path );

		$content = @file_get_contents( $thumbPath );
		if( !$content ) {
			header( 'Cache-Control: no-cache, must-revalidate' );
		}
		echo $content;

		exit();

	}

}


