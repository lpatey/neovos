<?php

require_once 'Core/Action/UserAction.php';

class Action_Fs_NewFolder extends Core_Action_UserAction {
	
	public function execute() {
		
		$path = $this->_getParam( 'path' );
		$name = $this->_getParam( 'name' );
		
		$path = $this->_getDistinctPath( $path );
		
		if( file_exists( $path . '/' . $name ) ) {
			$i = 1;
			while( file_exists( $path . '/' . $name . ' (' . $i . ')' ) ) {
				$i++;
			}
			$name .= ' (' . $i . ')';
		}
		
		if( !mkdir( $path . '/' . $name, 0777 ) ) {
			throw new Core_Exception( 'Desktop.error.cantCreateDirectory' );
		}
		chown( $path . '/' . $name, Config::get( 'server', 'user' ) );

		$time = $this->_getTime();

		$pathArg = Core_Fs::toShellArg( $path . '/' . $name );
		$inode = preg_replace( '/ .*/is', '', `ls -id $pathArg` );

		return array(
			'inode' => (int) $inode,
			'time' => $time,
			'name' => $name,
			'pid' => $this->_getParam( 'pid' )
		);
		
	}
	
}
