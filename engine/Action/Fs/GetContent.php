<?php

require_once 'Core/Action/UserAction.php';
require_once 'Core/Fs.php';

class Action_Fs_GetContent extends Core_Action_UserAction {

	public function execute() {

		$root = realpath( $this->_getUser()->getRoot() );
		
		if( !$root ) {
			return array( 'error' => 'Desktop.invalidPath' );
		}

		$path = $this->_getPath( $this->_getParam( 'path' ), false );
		$realPath = realpath($path);

		$pathArg = Core_Fs::toShellArg( $path );
		$thumbPath = $path . '/' . Config::get( 'fs', 'dir', 'thumb' );
		$thumbPathArg = Core_Fs::toShellArg( $thumbPath );

		if( !is_dir( $path ) ) {
			switch( Core_Fs::getMimeType( $path ) ) {
				case 'application/zip' :
					return $this->_action( 'Fs_Zip_GetContent', $this->_getParams() );
				case 'application/x-rar' :
					return $this->_action( 'Fs_Rar_GetContent', $this->_getParams() );
				case 'application/x-tar' :
				case 'application/x-compressed-tar' :
				case 'application/x-bzip-compressed-tar' :
					return $this->_action( 'Fs_Tar_GetContent', $this->_getParams() );
				case 'application/x-remote-directory' :
					return $this->_action( 'Fs_IntelliFile_GetContent', $this->_getParams() );
				default :
					return array( 'error' => 'Desktop.invalidPath' );
			}
		}

		Core_Fs::checkFolder( $path );
		
		$return = array();
		//$return['time'] = $this->_getTime();
		$return['data']['inode'] = $this->_getInode( $path );
		
		$return['content'] = array();
		$return['status'] = array(
			'time' => $this->_getTime()
		);

		$command = `ls -l --almost-all --inode --quote-name --ignore-backups --time=ctime --full-time $pathArg`;
		$hash = md5( $command );

		$thumbs = `ls $thumbPathArg`;
		$thumbs = md5($thumbs);
		
		$fullHash = $hash . $thumbs;

		$sameHash = $hash == $this->_getParam( 'hash' );
		$sameThumbs = $thumbs == $this->_getParam( 'thumbs' );
		$attributes = $this->_getParam( 'attributes' ) == 'true';
		$cacheDir = Config::get( 'fs', 'dir', 'cache' );
		$cachePath = $realPath . '/' . $cacheDir;
		$cacheFile = 'fs' . ($sameHash ? '1' : '0') . ($sameThumbs ? '1' : '0' ) . ($attributes ? '1' : '0' );

		if( $sameHash && $sameThumbs && $attributes ) {
			return $return;
		}
		else {
		
			if( file_exists( $cachePath .'/hash' ) ) {
				$cacheHash = file_get_contents( $cachePath . '/hash' );
				if( $cacheHash == $fullHash ) {
					if( file_exists( $cachePath . '/' . $cacheFile ) ) {
						$return = @unserialize( file_get_contents( $cachePath . '/' . $cacheFile ) );
						if( $return ) {
							return $return;
						}
					}
				}
				else {
					unlink( $cachePath . '/fs100' );
					unlink( $cachePath . '/fs110' );
					unlink( $cachePath . '/fs101' );
					unlink( $cachePath . '/fs000' );
					unlink( $cachePath . '/fs010' );
				}
			}
		
			if( !$sameHash ) {
				$return['status']['hash'] = $hash;
				$return['status']['thumbs'] = $thumbs;
				$return['status']['attributes'] = $attributes = false;
				$this->_send( 'CreateThumbs', array( 'path' => $path ) );
			}
			else if( !$attributes ) {
				$return['status']['attributes'] = true;
			}
			else if( !$sameThumbs ) {
				$return['status']['thumbs'] = $thumbs;
			}
		}

		$lines = explode( "\n", $command );

		array_shift( $lines );
		array_pop( $lines );

		$trashDir = Config::get( 'fs', 'dir', 'trash' );
		$desktopDir = Config::get( 'fs', 'dir', 'desktop' );
		$inode = Config::get( 'fs', 'inode' );

		if( $realPath == $root . '/' . $desktopDir ) {

			Core_Fs::checkRoot( $root );

			$return['content'][] = array(
				'inode' => $inode['myComputer']
			);
			
			$return['content'][] = array(
				'inode' => $inode['shares']
			);

			if( !$attributes && $sameHash ) {
				$return['content'][] = array(
					'inode' => $inode['trash'],
					'children' => $this->_action( 'Fs_GetContent', array( 'path' => $trashDir ) )
				);
			}
			else {
				$return['content'][] = array(
					'inode' => $inode['trash']
				);
			}
		}

		foreach( $lines as $line ) {
			$array = $this->_updateAll( $line, $path, $sameHash, $sameThumbs, $attributes );
			if( $array ) {
				$return['content'][] = $array;
			}
		}
		
		usort( $return['content'], array($this, '_cmp') );
		
		file_put_contents( $cachePath . '/hash', $fullHash );
		file_put_contents( $cachePath . '/' . $cacheFile, serialize( $return ) );
				
		return $return;
	}

	private function _updateAll( $line, $path, $sameHash, $sameThumbs, $attributes ) {
		
		$matches = $this->_getLineMatches( $line );
		$matches[8] = Core_Fs::fromShell( $matches[8] );
		
		if( $matches[8] == Config::get( 'fs', 'dir', 'neovos' ) ) {
			return null;
		}

		$return = array();
		$return[ 'name' ] = $matches[8];
		
		$fullPath = $path . '/' . $return['name'];
		
		$return[ 'inode' ] = $this->_getInode( $fullPath, $matches[1] );

		if( !$sameHash ) {
			$return[ 'perm' ] = substr( $matches[2], 1 );
			$return[ 'count' ] = $matches[3] == 1 ? 0 : $matches[3] - 2;
			$return[ 'size' ] = $matches[6];
			$return[ 'atime' ] = $matches[7];
			$return[ 'directory' ] = ($matches[2][0] == 'd') || (($matches[2][0] == 'l') && is_dir($path . '/' . $return['name']) );
			$return[ 'context' ] = 'remote';
			
			if( is_link($fullPath) ) {
				//$return[ 'alias' ] = Core_Fs::fromShell( $matches[9] );
				$aliasPath = ($matches[9][0] == '/' ? '' : $path ) . '/' . Core_Fs::fromShell( $matches[9] );
				$return[ 'alias' ] = fileinode( $aliasPath );
			}
			$return[ 'canonic' ] = $return['alias'] ? $return['alias'] : $return['inode'];
		}

		if( !$return['inode'] ) {
			return array();
		}

		if( !$sameThumbs || !$sameHash || !$attributes ) {
			$return[ 'mime' ] = Core_Fs::getMimeType( $path . '/' . $return['name'] );
			if( !$return['mime'] ) {
				return array();
			}
		}

		if( !$sameThumbs || !$sameHash ) {
			$return[ 'thumb' ] = $this->_hasThumb( $return['name'], $path );
		}

		if( !$attributes && $sameHash ) {
			/*if( $matches[2][0] == 'd' ) {
				$return[ 'children' ] = $this->_action( 'Fs_GetContent', array( 'path' => $this->_getRelativePath( $path . '/' . $return['name'] ) ) );
			}*/
			$return[ 'attributes' ] = $this->_getAttributes( $return, $path );
		}

		if( $sameHash ) {
			unset( $return['mime'] );
		}
		
		if( substr( $matches[8], -5 ) == '.json' ) {
			$content = @json_decode( file_get_contents( $path . '/' . $matches[8] ), true );
			if( $content ) {
				$return = array_merge( $return, $content );
			}
		}

		return $return;
	}

	private function _getLineMatches( $line ) {
		preg_match( '/(?#
			inode)([0-9]+) +(?#
			perm)([^\s]+) +(?#
			count)([0-9]+) +(?#
			owner)([^\s]+) +(?#
			group)([^\s]+) +(?#
			size)([0-9]+) +(?#
			atime)([^"]+) +(?#
			name)"((?:\\\"|[^"])+)"(?#
			alias)(?: -> "((?:\\\"|[^"])+)")?/s', $line, $match );
		return $match;
	}

	private function _getAttributes( $file, $path ) {

		$infos = explode( '/', $file['mime'] );
		$filePath = $path . '/' . $file['name'];

		$filePathArg = Core_Fs::toShellArg($filePath);
		$attributes = array();
		
		if( Core_Fs_Share::isShared( $filePath ) ) {
			$attributes[ 'shares' ] = $this->_getShares( $filePath );
		}

		if( $infos[0] == 'image' ) {
			$return = Core_Fs::fromShell(`identify -verbose $filePathArg`);
			$attributes[ 'fs.attributes.format' ] = preg_replace( '/([^\s]+).*/is', '$1', $this->_getAttribute( 'Format', $return ) );
			$attributes[ 'fs.attributes.dimensions' ] = $this->_getAttribute( 'Resolution', $return );
			$attributes[ 'fs.attributes.colorspace' ] = $this->_getAttribute( 'Colorspace', $return );
			return $attributes;
		}

		if( $infos[0] == 'video' ) {
			$return = Core_Fs::fromShell(`mplayer -identify -vo null -ao null -frames 0 $filePathArg`);
			$attributes[ 'fs.attributes.format' ] = $this->_getAttribute( 'ID_VIDEO_FORMAT', $return, '=' );
			$attributes[ 'fs.attributes.length' ] = $this->_getHumanSeconds( $this->_getAttribute( 'ID_LENGTH', $return, '=' ) );
			$width = intval($this->_getAttribute( 'ID_VIDEO_WIDTH', $return, '=' ));
			$height = intval($this->_getAttribute( 'ID_VIDEO_HEIGHT', $return, '=' ));
			$attributes[ 'fs.attributes.dimensions' ] = $width . 'x' . $height;
			$attributes[ 'fs.attributes.fps' ] = intval($this->_getAttribute( 'ID_VIDEO_FPS', $return, '=' ));
			return $attributes;
		}

		if( $infos[1] == 'pdf' ) {
			$return = Core_Fs::fromShell(`pdfinfo $filePathArg`);
			$attributes[ 'fs.attributes.author' ] = $this->_getAttribute( 'Author', $return );
			$attributes[ 'fs.attributes.pages' ] = $this->_getAttribute( 'Pages', $return );
			$dim = $this->_getAttribute( 'Page size', $return );
			if( preg_match( '/(.*?)\(([^\)]+)\)/s', $dim, $matches ) ) {
				$attributes[ 'fs.attributes.format' ] = $matches[2];
				$dim = trim( $matches[1] );
			}
			$attributes[ 'fs.attributes.dimensions' ] = $dim;
			return $attributes;
		}

		return $attributes;
	}

	private function _getHumanSeconds( $seconds ) {
		$seconds = intval( $seconds );
		
		$return = '';

		$hours = intval( intval( $seconds ) / 3600 );
		if( $hours > 0 ) {
			$return .= $hours . 'h ';
		}


		$minutes = bcmod( (intval( $seconds ) / 60 ),60);
		if( $minutes > 0 ) {
			$return .= $minutes . 'min ';
		}
  
		$seconds = bcmod( intval( $seconds ), 60 );
		$return .= $seconds . 's';

		return trim( $return );
	}

	private function _getAttribute( $key, $return, $separator = ':' ) {
		if( preg_match( '/(?:^|\s)' . $key . ' *' . $separator . ' *([^\n]+)/is', $return, $match ) ) {
			return $match[1];
		}
		return null;
	}


	private function _hasThumb( $name, $path ) {

		$thumbPath = $path . '/' . Config::get( 'fs', 'dir', 'thumb' ) . '/' . $name . '.png';
		return file_exists( $thumbPath );
	}
	
	private function _getShares( $path ) {
		$shares = array();
		foreach( Core_Fs_Share::getShares( $path ) as $shareId ) {
			$shares[ $shareId ] = $this->_getShareById( $shareId );
			if( !$shares[ $shareId ] ) {
				unset( $shares[ $shareId ] );
			}
		}
		return $shares;
	}
	
	private function _cmp( $data1, $data2 ) {
	
		$name1 = mb_strtolower($data1['name']);
		$name2 = mb_strtolower($data2['name']);
		$inode1 = $data1['inode'];
		$inode2 = $data2['inode'];
		
		if( $inode1 < 0 ) {
			return -1;
		}
		if( $inode2 < 0 ) {
			return 1;
		}

		if( $name1 == $name2 ) {
			return 0;
		}
		return $name1 > $name2 ? 1 : -1;
	}

}



