<?php

require_once 'Core/Action/PortalAction.php';

class Action_Portal_Confirmation extends Core_Action_PortalAction {

	protected function _getEmail() {
		return $_SESSION['signIn']['email'];
	}

	public function execute() {
		$this->_setViewEnabled( true );
		$this->_setTitle( 'Confirmation' );
	}
}
