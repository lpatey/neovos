<?php

require_once 'Core/Action/PortalAction.php';

class Action_Portal_LogIn extends Core_Action_PortalAction {

	private $_message;

	public function execute() {
	
		if( isset( $_POST ) && $_POST['action'] == 'Core_UserAction' ) {
			if( $this->_logIn( $_POST ) ) {
			
				$return = $this->_getReturn();
				if( !$return ) {
					$return = '/' .  $_SESSION['user']->getLogin();
				}
				header( 'Location: ' . $return );
				exit(0);
			}
		}
		$this->_setViewEnabled( true );
		$this->_setTitle( 'Log in - Neovos' );
	}
	
	protected function _getReturn() {
		if( isset( $_REQUEST['return'] ) ) {
			return $_REQUEST['return'];
		}
		return '';
	}
	
	protected function _getLogin() {
		if( isset( $_POST['login'] ) ) {
			return $_POST['login'];
		}
		if( isset( $_SESSION['user'] ) ) {
			return $_SESSION['user']->getLogin();
		}
		return '';
	}
	
	protected function _hasMessage() {
		return $this->_message != null;
	}
	
	protected function _getMessage() {
		return $this->_message;
	}
	
	protected function _logIn( $params ) {

		unset( $_SESSION['user'] );
	
		$sql = 'SELECT user_id, email, login, status
				FROM users 
				WHERE login=:login 
				AND password=:password';

		$params = array( 
			':login' => $params['login'],
			':password' => sha1($params['password'])
		);
			
		$statement = $this->_getDb()->prepare( $sql );

		$statement->execute( $params );

		if( $statement->rowCount() == 0 ) {
			unset( $POST['password'] );
			$this->_getLogger()->warn( array( 
					'type' => 'auth',
					'message' => 'Authentication failed',
					'get' => $_GET,
					'post' => $_POST
				) );
		
			$this->_message = 'Invalid credentials';
			return false;
		}
		
		$user = $statement->fetch( PDO::FETCH_ASSOC );
		
		if( $user['status'] == 'pending' ) {
			$this->_message = 'Pending validation';
			return false;
		}
		
		if( $user['status'] == 'disabled' ) {
			$this->_message = 'Account disabled';
			return false;
		}

		session_start( $params['login'] );
		$_SESSION['user'] = new Bean_User( array_merge( array( 'password' => $params['password'] ), $user ) );
		session_write_close();
			
		$this->_getLogger()->info( array( 
			'type' => 'auth',
			'message' => 'Authentication succeeded',
			'user_id' => $_SESSION['user']->getUserId()
		) );
			
		return true;

	}
}
