<?php

require_once 'Core/Action/PortalAction.php';

class Action_Portal_Reset extends Core_Action_PortalAction {

	private $_user;
	private $_messages = array();
	private $_resetUrl;

	public function execute() {
		$this->_setViewEnabled( true );
		$this->_setTitle( 'Reset your password - Neovos' );
		
		if( $_POST ) {
			switch( $_POST['action'] ) {
				case 'PortalAction_Reset_Send' :
					$this->_validateSend( $_POST );
					break;
				case 'PortalAction_Reset_Reset' :
					$this->_validateReset( $_POST );
					break;
			}
		}
		else if( $this->_hasParam( 'key' ) ) {
			$this->_displayReset();
		}
		else {
			$this->_displaySend();
		}
	}
	
	protected function _getResetUrl() {
		return $this->_resetUrl;
	}
	
	protected function _getLogin() {
		if( isset( $_POST['login'] ) ) {
			return $_POST['login'];
		}
		return '';
	}
	
	protected function _getEmail() {
		if( isset( $_POST['email'] ) ) {
			return $_POST['email'];
		}
		return '';
	}
	
	protected function _hasMessages() {
		return count($this->_messages) > 0;
	}
	
	protected function _getMessages() {
		return $this->_messages;
	}
	
	protected function _getUser() {
		return $this->_user;
	}
	
	private function _validateSend( $params ) {
	
		$login = trim( $params['login'] );
		$email = trim( $params['email'] );
		
		if( !$login && !$email ) {
			$this->_messages[] = 'Login or email required';
			return $this->_displaySend();
		}
		
		if( $login && $email ) {
			$query = 'SELECT * FROM users
				WHERE login = :login
				AND email = :email';
			$params = array(
				':login' => $login,
				':email' => $email
			);
			$message = 'No user correspond to both login and email';
		}
		else if( $login ) {
			$query = 'SELECT * FROM users
				WHERE login = :login';
			$params = array(
				':login' => $login
			);
			$message = 'No user correspond to login';
		}
		else if( $email ) {
			$query = 'SELECT * FROM users
				WHERE email = :email';
			$params = array(
				':email' => $email
			);
			$message = 'No user correspond to email';
		}
		$stmt = $this->_getDb()->prepare( $query );
		$stmt->execute( $params );
		
		if( $stmt->rowCount() != 1 ) {
			$this->_messages[] = $message;
			
			$this->_getLogger()->warn( array( 
				'type' => 'sendreset',
				'message' => $message,
				'post' => $_POST,
				'get' => $_GET
			) );
			
			return $this->_displaySend();
		}
		
		$user = new Bean_User( $stmt->fetch( PDO::FETCH_ASSOC ) );
		$this->_user = $user;
		
		$tmp_key = md5( microtime(true) . __FILE__ );
		$this->_resetUrl = $user->getPublicUrl() . '/Action_Portal_Reset?key=' . $tmp_key;
		
		$query = 'UPDATE users
			SET tmp_key = :tmp_key
			WHERE user_id = :user_id';

		$stmt = $this->_getDb()->prepare( $query );
		$stmt->execute( array(
			':user_id' => $user->getuserId(),
			':tmp_key' => $tmp_key
		) );
		
		$this->_getLogger()->info( array( 
			'type' => 'sendreset',
			'user_id' => $user->getUserId()
		) );
		
		$this->_sendEmail( $user->getEmail(), '[Neovos] Password reset', 'PasswordReset', 'noreply' );

		$this->_setView( 'Action/Portal/Reset/SendConfirmation.php' );
		
	}
	
	private function _validateReset( $params ) {
	
		$password = $params['password'];
		$confirmation = $params['confirmation'];
		
		if( !$password ) {
			$this->_messages[] = 'Password required';
			return $this->_displayReset();
		}
	
		if( $password != $confirmation ) {
			$this->_messages[] = 'Password and confirmation does not match';
			return $this->_displayReset();
		}
		
		if( !$this->_checkKey() ) {
			return $this->_displayInvalidKey();
		}
		
		$query = "UPDATE users
			SET
				tmp_key = NULL,
				password = :password,
				status = :status
			WHERE
				user_id = :user_id
				AND status != 'disabled'";
				
		$stmt = $this->_getDb()->prepare( $query );
		$stmt->execute( array(
			':password' => sha1( $password ),
			':user_id' => $this->_user->getUserId(),
			':status' => 'enabled'
		) );
		
		$this->_getLogger()->info( array( 
			'type' => 'reset',
			'user_id' => $this->_user->getUserId()
		) );
	
		$this->_setView( 'Action/Portal/Reset/ResetConfirmation.php' );
	}
	
	private function _displayInvalidKey() {
		$this->_setView( 'Action/Portal/Reset/ResetFailed.php' );
		
		unset( $_POST['password'] );
		unset( $_POST['confirmation'] );
		$this->_getLogger()->warn( array( 
			'type' => 'reset',
			'message' => 'Invalid key',
			'post' => $_POST,
			'get' => $_GET
		) );
	}
	
	private function _displayReset() {

		if( !$this->_checkKey() ) {
			return $this->_displayInvalidKey();
		}
	
		$this->_setView( 'Action/Portal/Reset/ResetForm.php' );
	}
	
	private function _checkKey() {
	
		$query = 'SELECT *
			FROM users
			WHERE
				login = :login
				AND tmp_key = :tmp_key
				AND tmp_key IS NOT NULL
			LIMIT 0,1';
		
		$stmt = $this->_getDb()->prepare( $query );
		
		$stmt->execute( array(
			':login' => $_GET['login'],
			':tmp_key' => $_GET['key']
		) );
		
		if( $stmt->rowCount() == 0 ) {
			return false;
		}
		
		$this->_user = new Bean_User( $stmt->fetch( PDO::FETCH_ASSOC ) );
		
		return true;
	}
	
	private function _displaySend() {
		$this->_setView( 'Action/Portal/Reset/SendForm.php' );
	}
}

