<?php

require_once 'Core/Action/PortalAction.php';

class Action_Portal_SignIn extends Core_Action_PortalAction {

	private $_messages = array();
	private $_activationUrl;

	public function execute() {
	
		if( isset( $_POST ) && $_POST['action'] = 'Core_UserAction' ) {
			if( $this->_signIn( $_POST ) ) {
				$return = $this->_getReturn();
				if( !$return ) {
					$return = '/Action_Portal_Confirmation';
				}
				header( 'Location: ' . $return );
				exit(0);
			}
		}
		$this->_setViewEnabled( true );
		$this->_setTitle( 'Sign in - Neovos' );
	}
	
	protected function _getReturn() {
		if( isset( $_POST['return'] ) ) {
			return $_POST['return'];
		}
		return '';
	}
	
	protected function _getLogin() {
		if( isset( $_POST['login'] ) ) {
			return $_POST['login'];
		}
		return '';
	}
	
	protected function _getEmail() {
		if( isset( $_POST['email'] ) ) {
			return $_POST['email'];
		}
		return '';
	}
	
	protected function _getActivationUrl() {
		return $this->_activationUrl;
	}
	
	protected function _hasMessages() {
		return count($this->_messages) > 0;
	}
	
	protected function _getMessages() {
		return $this->_messages;
	}

	protected function _signIn( $params ) {

		unset( $_SESSION['user'] );

		$login = trim( $params['login'] );
		$email = trim( $params['email'] );
		
		$this->_checkLogin( $login );
		$this->_checkPassword( $params['password'] );
		$this->_checkEmail( $email );
		
		if( $this->_hasMessages() ) {
			return false;
		}

		$tmp_key = md5( microtime(true) . __FILE__ );
		$this->_activationUrl = 'http://' . Config::get( 'server', 'main' ) . '/' . $login . '/Action_Portal_Activation?key=' . $tmp_key;

		$sql = 'INSERT INTO users 
				SET
					`login` = :login,
					`email` = :email,
					`password` = :password,
					`creation_time` = NOW(),
					`tmp_key` = :tmp_key,
					`status` = :status';

		$params = array( 
			':login' => $login,
			':email' => $email,
			':password' => sha1($params['password']),
			':tmp_key' => $tmp_key,
			':status' => 'pending'
		);
			
		$statement = $this->_getDb()->prepare( $sql );

		$statement->execute( $params );

		if( $statement->rowCount() == 0 ) {
			return false;
		}
		
		unset( $_POST['password'] );
		unset( $_POST['confirmation'] );
		$this->_getLogger()->info( array( 
				'type' => 'signin',
				'message' => 'Sign in succeeded',
				'post' => $_POST,
				'user_id' => $this->_getDb()->lastInsertId()
			) );
		
		
		$this->_sendEmail( $email, '[Neovos] Account creation', 'AccountCreation', 'noreply' );
		
		session_start( 'neovos' );
		$_SESSION['signIn'] = array(
			'login' => $login,
			'email' => $email
		);
		session_write_close();

		return true;

	}
	
	private function _checkLogin( $login ) {
		if( !$login ) {
			$this->_messages[] = 'Login required';
			return false;
		}
		
		if( preg_replace( '/[^A-Za-z0-9\-._]/', '', $login ) != $login ) {
			$this->_messages[] = 'Login must contains only alphanumerical characters or - . _';
			return false;
		}
		
		if( mb_strlen( $login ) < 4 ) {
			$this->_messages[] = 'Login must have more than 3 characters';
			return false;
		}
		
		if( mb_strlen( $login ) > 50 ) {
			$this->_messages[] = 'Login must have less than 50 characters';
			return false;
		}
		
		if( $this->_action( 'Portal_CheckLogin', array( 'login' => $login ) ) == array( 'valid' => false ) ) {
			$this->_messages[] = 'Login already used';
			return false;
		}
		
		return true;
	}
	
	private function _checkEmail( $email ) {
		if( !$email ) {
			$this->_messages[] = 'Email required';
			return false;
		}
		
		if( !preg_match( '/[a-z0-9!#$%&\'*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&\'*+\/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/', $email ) ) {
			$this->_messages[] = 'Invalid email';
			return false;
		}
		
		return true;
	}
	
	private function _checkPassword( $password ) {
		if( !$password ) {
			$this->_messages[] = 'Password required';
			return false;
		}
		
		return true;
	}
}
