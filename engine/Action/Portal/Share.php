<?php

require_once 'Core/Action/PortalAction.php';
require_once 'Bean/Share.php';

class Action_Portal_Share extends Core_Action_PortalAction {

	private $_share;
	private $_message;
	
	protected function _getMessage() {
		return $this->_message;
	}
	
	protected function _hasMessage() {
		return $this->_message != null;
	}

	protected function _getShare() {
		if( $this->_share === null ) {
			$share_id = $this->_getParam( 'share_id' );
			$query = 'SELECT users.*, shares.*
					FROM shares 
					LEFT JOIN users ON users.user_id = shares.user_id
					WHERE 
						share_id = :share_id
					AND login = :login';
			$stmt = $this->_getDb()->prepare( $query );
			$stmt->execute( array(
				':share_id' => $share_id,
				':login' => $this->_getParam( 'login' )
			) );
			
			if( !$stmt->rowCount() ) {
				return $this->_share = false;
			}
			
			$data = $stmt->fetch( PDO::FETCH_ASSOC );
			
			$this->_share = new Bean_Share( $data, new Bean_User( $data ) );
			
		}
		return $this->_share;
	}

	protected function _getLogin() {
		if( isset( $_POST['login'] ) ) {
			return $_POST['login'];
		}
		return '';
	}
	
	protected function _getEmail() {
		if( isset( $_POST['email'] ) ) {
			return $_POST['email'];
		}
		return '';
	}

	public function execute() {
	
		$this->_setViewEnabled( true );
		if( !$this->_getShare() ) {
			$this->_message = 'Share doesn\'t exist or has been removed';
			$this->_setView( 'Action/Portal/Error.php' );
			return;
		}
	
		if( $_SESSION['user'] ) {
			if( $_POST['action'] == 'Core_ShareAction' ) {
				if( $this->_validate( $_POST ) ) {
					header( 'Location: /' . $_SESSION['user']->getLogin() );
					exit(0);
				}
			}
			$this->_setView( 'Action/Portal/Share/JoinShare.php' );
		}
		else {
			$this->_setView( 'Action/Portal/Share/LogIn.php' );
		}
	}
	
	private function _validate( $post ) {
	
		$share = $this->_getShare();
		
		if( $share->hasPassword() ) {
			if( !$post['password'] ) {
				$this->_message = 'Password required';
				return false;
			}
			if( sha1($post['password']) != $share->getPassword() ) {
				$this->_message = 'Invalid password';
				return false;
			}
		}
		
		$user = $_SESSION['user'];
		$sharesDir = Config::get( 'fs', 'dir', 'shares' );
		$sharesPath = $user->getRoot() . '/' . $sharesDir;
		$name = $share->getName();
		
		$this->_getLogger()->info( array(
			'type' => 'joinshare',
			'share' => $share,
			'user_id' => $user->getUserId()
		) );

		symlink( $share->getPath(), $sharesPath . '/' . Core_Fs::getAvailableName($name, $sharesPath) );
		
		return true;
	}
}
