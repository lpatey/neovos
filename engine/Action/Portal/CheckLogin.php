<?php

require_once 'Core/Action.php';

class Action_Portal_CheckLogin extends Core_Action {

	public function execute() {
	
		$login = $this->_getParam( 'login' );
		
		$sql = 'SELECT user_id
				FROM users 
				WHERE login=:login
				LIMIT 0,1';

		$params = array( 
			':login' => $login
		);

		$statement = $this->_getDb()->prepare( $sql );

		$statement->execute( $params );

		return array( 'valid' => $statement->rowCount() == 0 );
	}
}
