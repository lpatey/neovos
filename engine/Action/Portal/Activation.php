<?php

require_once 'Core/Action/PortalAction.php';

class Action_Portal_Activation extends Core_Action_PortalAction {

	private $_user;
	private $_isActivated = false;

	protected function _getUser() {
		return $this->_user;
	}
	
	public function _isActivated() {
		return $this->_isActivated;
	}

	public function execute() {
	
		$this->_setViewEnabled( true );
		$this->_setTitle( 'Activation' );

		$this->_isActivated = $this->_activate( $_GET['login'], $_GET['key'] );
	}
	
	private function _activate( $login, $tmp_key ) {
	
		$sql = 'SELECT user_id, email, login, status
				FROM users 
				WHERE login = :login 
				AND tmp_key = :tmp_key
				AND tmp_key IS NOT NULL';

		$params = array( 
			':login' => $login,
			':tmp_key' => $tmp_key
		);

		$statement = $this->_getDb()->prepare( $sql );

		$statement->execute( $params );

		if( $statement->rowCount() == 0 ) {
			$this->_getLogger()->warn( array( 
				'type' => 'activate',
				'message' => 'Activation failed',
				'post' => $_POST,
				'get' => $_GET
			) );
			return false;
		}

		$user = new Bean_User( $statement->fetch( PDO::FETCH_ASSOC ) );
		
		$sql = 'UPDATE users
			SET
				tmp_key = NULL,
				status = :status
			WHERE
				user_id = :user_id
				AND status != \'disabled\'';
		$statement = $this->_getDb()->prepare( $sql );

		$statement->execute( array( 
			':user_id' => $user->getUserId(),
			':status' => 'enabled' ) );
			
		$this->_getLogger()->info( array( 
			'type' => 'activate',
			'message' => 'Activation succeeded',
			'user_id' => $user->getUserId()
		) );
		
		$root = Config::get( 'data', 'path' ) . '/' . $login;

		$cwd = getcwd();
		chdir( '../resources/userdefault' );
		`find . | grep -v '/\.svn' | cpio -dump $root`;
		chdir( $cwd );
		
		$this->_user = $user;
		
		session_start( $login );
		$_SESSION['user'] = $user;
		session_write_close();
		
		return true;
	}
}

