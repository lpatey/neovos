<?php

require_once 'Core/Action/PortalAction.php';

class Action_Portal_404 extends Core_Action_PortalAction {

	public function execute() {
		$this->_setViewEnabled( true );
	}
}
