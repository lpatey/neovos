<?php

require_once 'Core/Action/PortalAction.php';

class Action_Portal_Contact extends Core_Action_PortalAction {

	private $_message;

	public function execute() {
		$this->_setViewEnabled( true );
		
		if( isset( $_POST['contact'] ) ) {
			if( $this->_validate( $_POST['contact'] ) ) {
				$this->_setView( 'Action/Portal/Contact/Confirmation.php' );
				return;
			}
		}
		
		$this->_setView( 'Action/Portal/Contact/Form.php' );
	}
	
	public function _getEmail() {
		return Config::get( 'emails', 'contact', 'from' ); 
	}
	
	protected function _getSubject() {
		if( isset( $_POST['contact']['subject'] ) ) {
			return $_POST['contact']['subject'];
		}
		return '';
	}
	
	protected function _getMessageContent() {
		if( isset( $_POST['contact']['message'] ) ) {
			return $_POST['contact']['message'];
		}
		return '';
	}
	
	protected function _getFrom() {
		if( isset( $_POST['contact']['from'] ) ) {
			return $_POST['contact']['from'];
		}
		return '';
	}
	
	public function _hasMessage() {
		return $this->_message != null;
	}
	
	public function _getMessage() {
		return $this->_message;
	}
	
	private function _validate( $post ) {
		if( !$post['message'] ) {
			$this->_message = "Message can't be empty.";
			return false;
		}

		if( $post['test'] ) {
			$this->_getLogger()->warn( array(
				'type' => 'bot',
				'post' => $_POST,
				'get' => $_GET
			) );
			return true;
		}
		
		$to = $this->_getEmail();
		if( $post['to'] != $to ) {
			$this->_getLogger()->warn( array(
				'type' => 'hack',
				'post' => $_POST,
				'get' => $_GET
			) );
		}
		
		$admins = Config::get( 'admins' );
		$adminsIn = "'" . array_shift($admins) . "'";
		foreach( $admins as $admin ) {
			$adminsIn .= ",'" . $admin . "'";
		}

		$query = 'SELECT email
			FROM users
			WHERE login IN(' . $adminsIn . ')';
		$stmt = $this->_getDb()->prepare( $query );
		$stmt->execute( array() );

		$this->_getLogger()->info( array(
			'type' => 'message',
			'message' => $post
		) );
		
		while( $contact = $stmt->fetch( PDO::FETCH_ASSOC ) ) {
			$this->_sendEmail( $contact['email'], '[Neovos] Website message', 'WebsiteMessage', 'contact' );
		}

		return true;
	}
}
