<?php

require_once 'Core/Action/AdminAction.php';
require_once 'Bean/User.php';

class Action_Admin_Index extends Core_Action_AdminAction {

	private $_users;

	public function execute() {
		$this->_setViewEnabled( true );
	}
	
	protected function _getUsers() {
		if( !is_array($this->_users) ) {
			$this->_users = array();
			$query = 'SELECT * FROM users';
			$stmt = $this->_getDb()->prepare( $query );
			$stmt->execute( array() );
			while( $result = $stmt->fetch( PDO::FETCH_ASSOC ) ) {
				$this->_users[] = new Bean_User( $result );
			}
		}
		return $this->_users;
	}
}
