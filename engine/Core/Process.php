<?php

class Core_Process {

	public $PROCESS_DIR = '.neovos/process';
	private $_pid;
	private $_file;

	public function __construct( $pid = null ) {

		ignore_user_abort(true);
		set_time_limit(0);

		if( !$pid ) {
			$pid = microtime(true);
		}

		$this->_pid = $pid;

		$user = $_SESSION['user'];

		$processDir = $user->getRoot() . '/' . $this->PROCESS_DIR;

		if( !is_dir( $processDir ) ) {
			mkdir( $processDir );
		}

		$this->_file = tempnam( $processDir, 'process' );
		
	}

	public function write( $data = array() ) {
		$processData = array(
			'pid' => $this->_pid
		);
		file_put_contents( $this->_file, serialize( array_merge( $processData, $data ) ) );
	}


	public function __destruct() {
		unlink( $this->_file );
	}

	public function getPid() {
		return $this->_pid;
	}

}

