<?php

require_once 'Config.php';

class Core_Fs_Share {

	public static function isShared( $path ) {
	
		if( !is_link( $path ) ) {
			return false;
		}
		
		$shareDir = preg_quote( Config::get( 'fs', 'dir', 'share' ), '/' );
		return preg_match( '/' . $shareDir . '\/[0-9]+\/?$/is', readlink( $path ) );
	}
	
	public static function isSharedDestination( $path ) {
	
		$shareDir = preg_quote( Config::get( 'fs', 'dir', 'share' ), '/' );
		return preg_match( '/' . $shareDir . '\/[0-9]+\/?/is', $path );
	}
	
	public static function getShares( $path ) {
		$shares = array();
		while( is_link( $path ) ) {
			$path = readlink( $path );
			preg_match( '/([0-9]+)\/?$/is', $path, $matches );
			$shares[] = $matches[1];
		}
		return $shares;
	}

}

