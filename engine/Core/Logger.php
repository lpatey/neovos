<?php

require_once 'Config.php';

class Core_Logger {

	const ERROR = 0;
	const WARNING = 5;
	const INFO = 10;
	const DEBUG = 15;
	
	private static $_logs = array();
	
	private $_name;
	
	private function __construct( $name ) {
		$this->_name = $name;
	}
	
	public static function getLogger( $name ) {
		if( !isset( self::$_logs[ $name ] ) ) {
			self::$_logs[ $name ] = new self( $name );
		}
		return self::$_logs[ $name ];
	}
	
	public function err( $log ) {
		return $this->log( self::ERROR, $log );
	}
	
	public function warn( $log ) {
		return $this->log( self::WARNING, $log );
	}
	
	public function info( $log ) {
		return $this->log( self::INFO, $log );
	}
	
	public function debug( $log ) {
		return $this->log( self::DEBUG, $log );
	}

	public function log( $level, $log ) {

		if( $level > Config::get( 'log', 'level' ) ) {
			return;
		}
		
		$extension = '.';
		
		$logPath = Config::get( 'log', 'path' );
		$exts = explode( ',', Config::get( 'log', 'extensions', $level ) );
		
		$data = array(
			'level' => $level,
			'ip' => $_SERVER['REMOTE_ADDR'],
			'time' => microtime(true),
			'agent' => $_SERVER['HTTP_USER_AGENT'],
			'log' => $log );
		
		$data = $level . ' ' . serialize( $data ) . "\n";
		
		foreach( $exts as $ext ) {
			file_put_contents( $logPath . '/' . $this->_name . trim($ext), $data, FILE_APPEND );
		}
	}
}
