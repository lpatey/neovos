<?php

require_once 'Core/Db.php';

abstract class Core_Action {

	private $_params;
	private $_viewEnabled = false;
	private $_fifo;
	private $_logger;

	public function __construct( $params, $logger ) {
		$this->_params = $params;
		$this->_logger = $logger;
	}

	public function prepend() {
	}

	public abstract function execute();

	protected function _action( $action, $params = array() ) {
		$action = 'Action_' . $action;
		require_once str_replace( '_', '/', $action ) . '.php';
		$instance = new $action( $params, $this->_logger );

		$return = $instance->prepend();
		if( $return !== null ) {
			return $return;
		}

		$return = $instance->execute();
		if( $return !== null ) {
			return $return;
		}

		$instance->view();
	}

	public function view() {
		$view = $this->_getView();
		if( $this->_viewEnabled ) {
			require $view;
		}
	}

	protected function _getTime() {
		return microtime(true) * 1000;
	}

	protected function & _getConf() {
		$args = func_get_args();
		return call_user_func_array( array( 'Config', 'get' ), $args );
	}

	protected function _setViewEnabled( $flag ) {
		$this->_viewEnabled = $flag;
	}

	protected function _hasParam( $name ) {
		return isset( $this->_params[ $name ] );
	}

	protected function _getParam( $name ) {
		return $this->_params[ $name ];
	}

	protected function _getParams() {
		return $this->_params;
	}
	
	protected function _sendEmail( $email, $subject, $content, $from ) {
	
		ob_start();
		require 'View/Email/' . $content . '.php';
		$mailContent = ob_get_clean();
		
		$headers = '';
		foreach( Config::get( 'emails', $from ) as $key => $value ) {
			$headers .= ucfirst($key) .': ' . $value . "\r\n";
		}

		mail( $email, $subject, $mailContent, $headers );
		
		return $this;
	}

	protected function _getFifo() {
		if( !$this->_fifo ) {
			$this->_fifo = fopen( Config::get( 'daemon', 'fifo' ), 'a' );
			stream_set_blocking( $this->_fifo, false );
		}
		return $this->_fifo;
	}

	protected function _send( $action, $params = array() ) {
		$fh = $this->_getFifo();
		fputs( $fh, $action . ' ' . serialize( $params ) . "\n" );
	}

	protected function _getDb() {
		return Core_Db::getDb();
	}

	protected function _getView() {
		return 'View/' . str_replace( '_', '/', get_class($this) ) . '.php';
	}
	
	protected function _getLogger() {
		return $this->_logger;
	}

	public function __destruct() {
		if( $this->_fifo ) {
			fclose( $this->_fifo );
		}
	}
}
