<?php

class Core_Db {

	protected static $_db;

	public static function getDb() {
		if( self::$_db == null ) {
			$db = Config::get( 'db' );
			self::$_db = new PDO( $db['adapter'] . ':dbname=' . $db['dbname'] . ';host=' . $db['host'], $db['user'], $db['password'] );
		}
		return self::$_db;
	}

}

