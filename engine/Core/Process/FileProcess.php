<?php

require_once 'Core/Process.php';

class Core_Process_FileProcess extends Core_Process {

	public $STATE_PRECOMPUTING = 1;
	public $STATE_COMPUTING = 2;

	private $_progress;
	private $_filesCount = 0;
	private $_totalSize = 0;

	public function __construct( $pid = null ) {
		parent::__construct( $pid );
		$this->write( array( 'status' => $this->STATE_PRECOMPUTING ) );
	}

	public function setFiles( $path, $files = array() ) {

		$size = 0;
		$count = 0;
		
		if( $files ) {
			foreach( $files as $file ) {
				$count += $this->_getCount( $path . '/' . $file );
				$size += $this->_getSize( $path . '/' . $file  );
			}
		}
		else {
			$count += $this->_getCount( $path );
			$size += $this->_getSize( $path  );
		}
		
		
		$this->setFilesCount( $count );
		$this->setTotalSize( $size );
	}

	public function setFilesCount( $count ) {
		$this->_filesCount = $count;
	}

	public function setTotalSize( $size ) {
		$this->_totalSize = $size;
	}

	public function addProgress( $file, $progress ) {
		$this->setProgress( $file, $this->_progress + $progress );
	}

	public function setProgress( $file, $progress ) {
		$this->_progress = $progress;
		$this->write( array( 
			'status' => $this->STATE_COMPUTING,
			'progress' => $progress,
			'totalSize' => $this->_totalSize,
			'file' => basename($file)
		) );
	}

	private function _getSize( $path ) {
		$pathArg = Core_Fs::toShellArg( $path );
		return intval( `du -sb $pathArg` );
	}

	private function _getCount( $path ) {
		$pathArg = Core_Fs::toShellArg( $path );
		return intval( `file $pathArg | wc -l` );
	}

}
