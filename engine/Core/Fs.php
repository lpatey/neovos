<?php

require_once 'Config.php';
require_once 'Core/Fs/Share.php';

class Core_Fs {

	public static function getThumbnailPath( $path ) {
		$dir = dirname( $path ) . '/' . Config::get( 'fs', 'dir', 'thumb' );
		$name = basename( $path ) . '.png';
		return $dir . '/' . $name;
	}

	public static function getMimeType( $path ) {

		if( substr( $path, -5 ) == '.json' ) {
			$content = json_decode(file_get_contents( $path ), true);
			if( $content ) {
				return $content['mime'];
			}
		}
		
		if( is_dir( $path ) && Core_Fs_Share::isShared( $path ) ) {
			return 'application/x-share';
		}
		
		$filePathArg = self::toShellArg($path);
		$infos = self::fromShell( `gnomevfs-info $filePathArg 2>&1` );
		if( preg_match( '/(?:^|\s)MIME type *: *([^\n]+)/is', $infos, $match ) ) {
			return $match[1];
		}
		return null;
	}

	public static function checkFolder( $path ) {

		$neovosDir = Config::get( 'fs', 'dir', 'neovos' );
		$thumbDir = Config::get( 'fs', 'dir', 'thumb' );
		$cacheDir = Config::get( 'fs', 'dir', 'cache' );
		$user = Config::get( 'server', 'user' );

		if( !is_dir( $path . '/' . $neovosDir ) ) {
			mkdir( $path . '/' . $neovosDir, 0770, true );
			chown( $path . '/' . $neovosDir, $user );
		}

		if( !is_dir( $path . '/' . $thumbDir ) ) {
			mkdir( $path . '/' . $thumbDir, 0770, true );
			chown( $path . '/' . $thumbDir, $user );
		}
		
		if( !is_dir( $path . '/' . $cacheDir ) ) {
			mkdir( $path . '/' . $cacheDir, 0770, true );
			chown( $path . '/' . $cacheDir, $user );
		}
	}

	public static function checkRoot( $path ) {

		$neovosDir = Config::get( 'fs', 'dir', 'neovos' );
		$trashDir = Config::get( 'fs', 'dir', 'trash' );
		$shareDir = Config::get( 'fs', 'dir', 'share' );
		$sharesDir = Config::get( 'fs', 'dir', 'shares' );
		$user = Config::get( 'server', 'user' );

		if( !is_dir( $path . '/' . $neovosDir ) ) {
			mkdir( $path . '/' . $neovosDir, 0770, true );
			chown( $path . '/' . $neovosDir, $user );
		}

		if( !is_dir( $path. '/' . $trashDir ) ) {
			mkdir( $path. '/' . $trashDir, 0770, true );
			chown( $path . '/' . $trashDir, $user );
		}
		
		if( !is_dir( $path. '/' . $shareDir ) ) {
			mkdir( $path. '/' . $shareDir, 0770, true );
			chown( $path . '/' . $shareDir, $user );
		}
		
		if( !is_dir( $path. '/' . $sharesDir ) ) {
			mkdir( $path. '/' . $sharesDir, 0770, true );
			chown( $path . '/' . $sharesDir, $user );
		}
	}
	
	public static function getNewName( $name, $i ) {
		$infos = pathinfo( $name );
		$name = $infos['filename'] . ' (' . $i . ')';
		return $name . ($infos['extension'] ? '.' . $infos['extension'] : '');
	}
	
	public static function getAvailableName( $name, $path ) {
		if( file_exists( $path . '/' . $name ) ) {
			$i = 1;
			while( file_exists( $path . '/' . self::getNewName( $name, $i ) ) ) {
				$i++;
			}
			$name = self::getNewName( $name, $i );
		}
		return $name;
	}

	public static function getRawFileList( $path ) {
		$pathArg = self::toShellArg( $path );
		$command = `ls --almost-all --ignore=".neovos" --ignore-backups $pathArg`;
		$lines = explode( "\n", $command );
		array_pop( $lines );
		return $lines;
	}

	public static function toShellArg( $path ) {
		$path = str_replace( '\'', '\'\\\'\'', $path );
		return "'$path'";
	}

	public static function fromShell( $output ) {
		$output = str_replace( '\\"', '"', $output );
		$output = preg_replace( '/\\\\([0-9]{3}|.)/e', '"\\\$1"', $output );
		return $output;
	}
	
	public static function basename( $path ) {
		$path = rtrim( $path, DIRECTORY_SEPARATOR );
		if( ($pos = strrpos( $path, DIRECTORY_SEPARATOR )) !== false ) {
			return substr( $path, $pos + 1 );
		}
		return $path;
	}
	
	public static function dirname( $path ) {
		$path = rtrim( $path, DIRECTORY_SEPARATOR );
		if( $pos = strrpos( $path, DIRECTORY_SEPARATOR ) ) {
			return substr( $path, 0, $pos );
		}
		return '';
	}

	public static function checkConflicts( $source, $dest, $choices ) {
		if( isset($choices[ '*' ]) ) {
			return array();
		}
		$conflicts = array();
		if( !file_exists( $dest ) ) {
			return array();
		}
		if( !is_dir( $dest ) ) {
			if( isset( $options[$dest] ) ) {
				return array();
			}
			return array( $dest );
		}
		if( !isset( $options[$dest] ) ) {
			return $options[$dest];
		}
		return $conflicts;
	}
}
