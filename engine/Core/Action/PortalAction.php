<?php

require_once 'Core/Action.php';

abstract class Core_Action_PortalAction extends Core_Action {

	private $_title = null;
	private $_view = null;
	private $_content = null;
	
	protected function _setTitle( $title ) {
		$this->_title = $title;
	}
	
	protected function _getTitle() {
		return $this->_title;
	}
	
	protected function _getView() {
		$this->_getContent();
		return 'View/' . str_replace( '_', '/', get_class() ) . '.php';
	}
	
	protected function _setView( $view ) {
		$this->_view = $view;
	}
	
	protected function _getContent() {
		if( $this->_content === null ) {
			$view = $this->_view == null ? parent::_getView() : 'View/' . $this->_view;
			ob_start();
			require $view;
			$this->_content = ob_get_clean();
		}
		return $this->_content;
	}

}
