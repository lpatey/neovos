<?php

require_once 'Core/Action.php';
require_once 'Bean/User.php';
require_once 'Core/Fs.php';
require_once 'Config.php';

abstract class Core_Action_UserAction extends Core_Action {

	private $_user;

	public function prepend() {
		parent::prepend();

		if( isset( $_POST ) && $_POST['action'] == 'Core_UserAction' ) {

			$sql = 'SELECT user_id, email, login
				FROM users 
				WHERE login=:login 
				AND password=:password
				AND status=:status';

			$params = array( 
				':login' => $_GET['login'],
				':password' => sha1($_POST['password']),
				':status' => 'enabled'
			);
			
			$statement = $this->_getDb()->prepare( $sql );

			$statement->execute( $params );

			if( $statement->rowCount() == 0 ) {
				unset( $POST['password'] );
				$this->_getLogger()->warn( array( 
					'type' => 'auth',
					'message' => 'Authentication failed',
					'get' => $_GET,
					'post' => $_POST
				) );

				return array_merge( $this->_fail(), array( 
					'message' => 'user.auth.messages.invalidCredential' 
				) );
			}

			session_start( $_GET['login'] );
			$_SESSION['user'] = new Bean_User( array_merge( 
					array( 'password' => $_POST['password'] ), 
					$statement->fetch( PDO::FETCH_ASSOC ) ) );
			session_write_close();
			
			$this->_getLogger()->info( array( 
				'type' => 'auth',
				'message' => 'Authentication succeeded',
				'user_id' => $_SESSION['user']->getUserId()
			) );
			
		}

		if( !isset( $_SESSION['user'] ) 
			|| $_SESSION['user']->getLogin() != $_GET['login'] ) {
			
			unset( $_SESSION['user'] );
			return $this->_fail();
		}
		
		$this->_getLogger()->debug( array(
			'type' => get_class($this),
			'post' => array( $_POST ),
			'get' => array( $_GET ),
			'user_id' => $_SESSION['user']->getUserId()
		) );

		$this->_user = $_SESSION['user'];
		
	}

	protected function _getPath( $path, $followSymlinks = true ) {

		$root = realpath( $this->_getUser()->getRoot() );
		
		if( !$root ) {
			return array( 'error' => 'Desktop.error.invalidPath' );
		}

		if( strpos( $path, '!/' ) !== false ) {
			$path = substr( $path, 0, strpos( $path, '!/' ) );
		}

		$realPath = realpath( $root . '/' . $path );

		if( substr( $realPath, 0, mb_strlen($root) ) != $root ) {
			if( !Core_Fs_Share::isSharedDestination( $realPath ) ) {
				throw new Core_Exception( 'Desktop.error.invalidPath' );
			}
		}
		
		if( $followSymlinks ) {
			return $realPath;
		}
		
		return $root . '/' . $path;
	}

	protected function _getRelativePath( $path ) {

		$root = realpath( $this->_getUser()->getRoot() );
		
		if( !$root ) {
			return array( 'error' => 'Desktop.error.invalidPath' );
		}

		$path = realpath( $path );

		return substr( $path, mb_strlen($root) );
	}

	protected function _getDistinctPath( $path, $followSymlinks = true ) {
		$path = $this->_getPath( $path, $followSymlinks );
		if( $path == realpath( $this->_getUser()->getRoot() ) ) {
			throw new Core_Exception( 'Desktop.error.invalidPath' );
		}
		return $path;
	}

	protected function _fail() {
		return array( 'error' => 'User.checkAuth' );
	}

	protected function _getUser() {
		return $this->_user;
	}
	
	protected function _getShareById( $shareId ) {
		$query = '
			SELECT shares.*, users.user_id, users.login
			FROM shares
			LEFT JOIN users ON shares.user_id = users.user_id
			WHERE share_id = :share_id';
		$stmt = $this->_getDb()->prepare( $query );
		$stmt->execute( array( ':share_id' => $shareId ) );
		if( $stmt->rowCount() == 0 ) {
			return array();
		}
		$share = $stmt->fetch( PDO::FETCH_ASSOC );;
		$share['mime'] = 'application/x-' . ($share['password' ] ? 'secured' : 'free' ) . '-share';
		$share['inode'] = 'x-share-' . $shareId;
		$share['thumb'] = false;
		return $share;
	}

	protected function _getInode( $path, $defaultInode = null ) {

		$inode = Config::get( 'fs', 'inode' );
		$dir = Config::get( 'fs', 'dir' );
		
		$path = rtrim( $path, '/' );
		$realPath = realpath( $path );
		
		if( !is_link( $path ) ) {

			$root = realpath( $this->_getUser()->getRoot() );

			if( $realPath == $root ) {
				return $inode['root'];
			}

			if( $realPath == $root . '/' . $dir['desktop'] ) {
				return $inode['desktop'];
			}

			if( $realPath == $root . '/' . $dir['trash'] ) {
				return $inode['trash'];
			}
		
			if( $realPath == $root . '/' . $dir['shares'] ) {
				return $inode['shares'];
			}

		}

		if( $defaultInode !== null ) {
			return $defaultInode;
		}


		$pathArg = Core_Fs::toShellArg( $path );
		$line = `ls -Llid $pathArg`;
		
		return intval($line);
		return fileinode( $path );
	}
}
