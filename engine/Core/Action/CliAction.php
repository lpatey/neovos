<?php

require_once 'Core/Action.php';

abstract class Core_Action_CliAction extends Core_Action {

	public function prepend() {
		parent::prepend();
		if( !$GLOBALS['argv'] ) {
			return false;
		}
	}
}
