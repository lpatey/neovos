<?php

require_once 'Core/Action/PortalAction.php';

abstract class Core_Action_AdminAction extends Core_Action_PortalAction {

	private $_message;
	
	protected function _getMessage() {
		return $this->_message;
	}

	public function prepend() {
		
		parent::prepend();
		
		if( !$_SESSION['user'] ) {
			header( 'Location: /Action_Portal_LogIn?return=' . $_SERVER['PHP_SELF'] );
			exit(0);
		}
		
		if( !in_array($_SESSION['user']->getLogin(), Config::get( 'admins' ) ) ) {
			$this->_message = 'Must be logged in with an admin account';
			$this->_setViewEnabled( true );
			$this->_setView( 'Action/Portail/Error.php' );
			$this->view();
			exit(0);
		}
	}
}
