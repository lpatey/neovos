<?php

class Core_Module_IntelliFile {

	public static function getData( $url ) {
	
		if( !self::_isValidUrl( $url ) ) {
			return array();
		}
	
		$prefix = 'http://www.youtube.com/watch';
		if( substr( $url, 0, strlen($prefix) ) == $prefix ) {
			return self::_getYouTubeVideo( $url );
		}
		
		$prefix = 'http://www.dailymotion.com/video/';
		if( substr( $url, 0, strlen($prefix) ) == $prefix ) {
			return self::_getDailymotionVideo( $url );
		}

		if( preg_match( '!http://[^/]+facebook.com!is', $url ) ) {
			return self::_getFacebookPage( $url );
		}


		if( preg_match( '!http://[a-z]+.wikipedia.org/wiki/!', $url ) ) {
			return self::_getWikipediaPage( $url );
		}
		
		return self::_getAnyDocument( $url );
	}
	
	private static function _getYouTubeVideo( $url ) {
	
		$content = self::_fileGetContents( $url );
		preg_match( '/v=([^&]+)/', $url, $match );
		preg_match( "/VIDEO_USERNAME': '((?:\\\.|[^'])+)'/", $content, $match2 );
		preg_match( "/VIDEO_TITLE': '((?:\\\.|[^'])+)'/is", $content, $match3 );

		$return = array();
		$return['key'] = $match[1];
		$return['type'] = 'IntelliFile';
		$return['handler'] = 'youtube';
		$return['url'] = $url;
		$return['mime'] = 'application/x-remote';
		$return['remotemime'] = 'application/x-shockwave-flash';
		$return['thumb'] = 'http://i1.ytimg.com/vi/' . $match[1] . '/default.jpg';
		$return['attributes']['fs.attributes.owner'] = stripslashes($match2[1]);
		$return['attributes']['fs.attributes.title'] = stripslashes($match3[1]);
		$return['name'] = self::_toName( $return['attributes']['fs.attributes.title'] ) . '.flv.json';
		
		return $return;
	}
	
	private static function _getDailymotionVideo( $url ) {
	
		$content = self::_fileGetContents( $url );
		preg_match( '/<link rel="thumbnail" type="image\/jpeg" href="([^"]+)"/', $content, $match );
		preg_match( '/info_userLogin", "([^"]+)"/', $content, $match2 );
		preg_match( '/info_videoTitle", "([^"]+)"/', $content, $match3 );

		$return = array();
		$return['type'] = 'IntelliFile';
		$return['handler'] = 'dailymotion';
		$return['url'] = $url;
		$return['mime'] = 'application/x-remote';
		$return['remotemime'] = 'application/x-shockwave-flash';
		$return['thumb'] = 'http://www.dailymotion.com' . $match[1];
		$return['attributes']['fs.attributes.owner'] = urldecode($match2[1]);
		$return['attributes']['fs.attributes.title'] = urldecode($match3[1]);
		$return['name'] = self::_toName( $return['attributes']['fs.attributes.title'] ) . '.flv.json';
		
		return $return;
	}

	private static function _getWikipediaPage( $url ) {

		$content = self::_fileGetContents( $url );

		preg_match( '/<h1[^>]*>(.*?)<\/h1>/is', $content, $titleMatch );

		if( preg_match( '/<table class="infobox.*?<\/table>/is', $content, $thumbMatch ) 
			&& preg_match( '/<img [^>]*?src="([^"]+)"/', $thumbMatch[0], $thumbMatch2 ) ) {
			$thumb = $thumbMatch2[1];
		}
		else {
			$thumb = 'http://upload.wikimedia.org/wikipedia/fr/b/bc/Wiki.png';
		}

		$return = array();
		$return['type'] = 'IntelliFile';
		$return['handler'] = 'wikipedia';
		$return['url'] = $url;
		$return['mime'] = 'application/x-remote';
		$return['remotemime'] = 'text/html';
		$return['thumb'] = $thumb;
		$return['attributes']['fs.attributes.title'] = html_entity_decode( $titleMatch[1], ENT_QUOTES, 'UTF-8' );
		$return['name'] = self::_toName( $return['attributes']['fs.attributes.title'] ) . '.html.json';
		
		return $return;

	}

	private static function _getFacebookPage( $url ) {

		$content = self::_fileGetContents( $url );

		if( !preg_match( '/<h1[^>]*>(.*?)<.?\/h1>/is', $content, $titleMatch ) ) {
		}

		if( preg_match( '/<img src=."([^"]+)."[^>]*?id=."profile_pic."/', $content, $thumbMatch )
			||preg_match( '!"(/profile/[^"]+)"!is', $content, $thumbMatch) ) {
			if( $thumbMatch[1][0] == '/' ) {
				$thumbMatch[1] = 'http://www.facebook.com' . $thumbMatch[1];
			}
			$thumb = stripslashes($thumbMatch[1]);
		}
		else {
			$thumb = 'http://bases.stanford.edu/images/facebook-logo.jpg';
		}

		$title = trim( strip_tags( stripslashes( $titleMatch[1] ) ) );

		$return = array();
		$return['type'] = 'IntelliFile';
		$return['handler'] = 'facebook';
		$return['url'] = $url;
		$return['mime'] = 'application/x-remote';
		$return['remotemime'] = 'text/html';
		$return['thumb'] = $thumb;
		$return['attributes']['fs.attributes.title'] = $title;
		$return['name'] = self::_toName( $return['attributes']['fs.attributes.title'] ) . '.html.json';
		
		return $return;

	}

	private static function _fileGetContents( $url ) {

		$opts = array(
			'http' => array(
				'method' => 'GET',
				'header' => array(),
				'user_agent' => $_SERVER['HTTP_USER_AGENT']
 				)
			); 

		$context = stream_context_create( $opts );

		return @file_get_contents( $url, false, $context );
	}

	private static function _getHtmlDirectory( $url, $return, $content ) {

		$return['mime'] = 'application/x-remote-directory';
		return $return;
	}

	private static function _getHtmlPage( $url, $return ) {

		$content = self::_fileGetContents( $url );

		if( preg_match( '/<title>\s*Index of /i', $content ) ) {
			return self::_getHtmlDirectory( $url, $return, $content );
		}

		if( !preg_match( '/<h1[^>]*>(.*?)<\/h1>/is', $content, $titleMatch ) ) {
			preg_match( '/<title[^>]*>(.*?)<\/title>/is', $content, $titleMatch );
		}

		$title = self::_toValidString( strip_tags($titleMatch[1]) );
		$title = html_entity_decode( $title, ENT_QUOTES, 'UTF-8' );
		$return['attributes']['fs.attributes.title'] = $title;

		$return['thumb'] = 'http://open.thumbshots.org/image.pxf?url=' . urlencode($url);


		$return['name'] = self::_toName( $return['attributes']['fs.attributes.title'] ) . '.html.json';
		
		return $return;
	}
	
	private static function _getAnyDocument( $url ) {
		
		$fh = @fopen( $url, 'r' );
		if( !$fh ) {
			return array();
		}

		$meta = stream_get_meta_data( $fh );
		fclose( $fh );
		
		$name = parse_url($url, PHP_URL_PATH);
		$name = basename( $name );
		if( !$name ) {
			$name = parse_url($url, PHP_URL_HOST);
		}
		
		$return = array();
		$return['type'] = 'IntelliFile';
		$return['handler'] = 'anyfile';
		$return['url'] = $url;
		$return['mime'] = 'application/x-remote';
		foreach( $meta['wrapper_data'] as $value ) {
			if( preg_match( '/Content-Type\s*:\s*([^;\s]+)/is', $value, $match ) ) {
				$return['remotemime'] = $match[1];
			}			
			if( preg_match( '/Content-Length\s*:\s*([^;\s]+)/is', $value, $match ) ) {
				$return['size'] = $match[1];
			}
		
		}
		$return['thumb'] = false;
		$return['name'] = urldecode($name). '.json';
		
		if( $return['remotemime'] == 'text/html' ) {
			return self::_getHtmlPage( $url, $return );
		}


		return $return;
	}
	
	private static function _isValidUrl( $url ) {
		return '127.0.0.1' != gethostbyname( parse_url( $url, PHP_URL_HOST ) );
	}
	
	private static function _toName( $url ) {
		return substr( $url, 0, 100 );
	}

	private static function _toValidString( $str ) {

		if( !self::_isUtf8( $str ) ) {
			$str = utf8_encode( $str );
		}
		$str = preg_replace( '/\s+/', ' ', $str );

		$str = trim( $str );
		return $str;
	}

	private static function _isUtf8( $str ) {
		return preg_match('%^(?:
                [\x09\x0A\x0D\x20-\x7E]              # ASCII
                | [\xC2-\xDF][\x80-\xBF]             # non-overlong 2-byte
                |  \xE0[\xA0-\xBF][\x80-\xBF]        # excluding overlongs
                | [\xE1-\xEC\xEE\xEF][\x80-\xBF]{2}  # straight 3-byte
                |  \xED[\x80-\x9F][\x80-\xBF]        # excluding surrogates
                |  \xF0[\x90-\xBF][\x80-\xBF]{2}     # planes 1-3
                | [\xF1-\xF3][\x80-\xBF]{3}          # planes 4-15
                |  \xF4[\x80-\x8F][\x80-\xBF]{2}     # plane 16
                )*$%xs',
                $str );
	}
}
