
###################################################################
# 1 -                 Apache configuration                        #
###################################################################

- Create a link of apache-site.conf into your apache directory 
and include it.
- Change the DocumentRoot to the new root

###################################################################
# 2 -                  MySQL configuration                        #
###################################################################

- Execute the database setup script neovos.sql
- Execute the data example script neovos.data.sql


###################################################################
# 3 -                  Tasks configuration                        #
###################################################################

- Configure cron to execute
	/neovosat Nighty
- Start neovosd dameon with
	/nohup /neovosd
