-- phpMyAdmin SQL Dump
-- version 2.11.8.1deb5+lenny3
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- G�n�r� le : Dim 28 F�vrier 2010 � 16:59
-- Version du serveur: 5.0.51
-- Version de PHP: 5.2.6-1+lenny6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Base de donn�es: `aquila`
--

-- --------------------------------------------------------

--
-- Structure de la table `editions`
--

CREATE TABLE IF NOT EXISTS `editions` (
  `edition_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `path` varchar(500) NOT NULL,
  `key` varchar(250) NOT NULL,
  `time` datetime NOT NULL,
  PRIMARY KEY  (`edition_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `shares`
--

CREATE TABLE IF NOT EXISTS `shares` (
  `share_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `name` varchar(250) NOT NULL,
  `inode` int(10) unsigned NOT NULL,
  `password` varchar(250) DEFAULT NULL,
  `creation_time` datetime NOT NULL,
  PRIMARY KEY (`share_id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `login` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL,
  `tmp_key` VARCHAR( 250 ) NULL,
  `creation_time` datetime NOT NULL,
  `status` enum('enabled','disabled','pending') NOT NULL DEFAULT 'pending',
  PRIMARY KEY (`user_id`),
  KEY `email` (`email`),
  KEY `login` (`login`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

