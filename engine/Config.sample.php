<?php


class Config {

	private static $_config = array(

		'admins' => array(
			'gilleluc',
			'ludovic'
		),
		
		'log' => array(
			'path' => '/var/log/aquila',
			'name' => 'aquila',
			'extensions' => array(
				Core_Logger::ERROR => '.log, .err',
				Core_Logger::WARNING => '.log, .warn',
				Core_Logger::INFO => '.log, .info',
				Core_Logger::DEBUG => '.log, .debug'
			),
			'level' => 15
		),

		'db' => array( 
			'adapter' => 'mysql',
			'host' => 'localhost',
			'user' => 'aquila',
			'password' => '',
			'dbname' => 'aquila'
		),

		'emails' => array(
			'noreply' => array(
				'from' => 'noreply@neovos.com',
                                'x-priority' => 1
			),
			'contact' => array(
				'from' => 'contact@neovos.com',
                                'x-priority' => 1
			)
		),
	
		'data' => array(
			'path' => '/home/aquila'
		),
		
		'daemon' => array(
			'fifo' => '/tmp/neovosfifo',
			'log' => 'daemon'
		),
		
		'task' => array(
			'log' => 'nighty'
		),

		'fs' => array(
			'dir' => array(
				'thumb' => '.neovos/thumbs',
				'neovos' => '.neovos',
				'trash' => '.neovos/trash',
				'share' => '.neovos/share',
				'process' => '.neovos/process',
				'desktop' => 'Desktop',
				'shares' => 'Shares',
				'cache' => '.neovos/cache'
			),
			'inode' => array(
				'myComputer' => -1,
				'root' => 0,
				'desktop' => -2,
				'trash' => -3,
				'shares' => -4
			)
		),
		
		'server' => array(
			'main' => 'neovos.com',
			'admin' => 'admin.neovos.com',
			'user' => 'www-data'
		),
		
		'zoho' => array(
			'apiKey' => '74aa01681949cb2939bca9cf186df35b',
			'secretKey' => '21391322737975cafe467fb6dfbef584'
		)

	);


	public static function & get() {
		$value = &self::$_config;		
		foreach( func_get_args() as $arg ) {
			$value = &$value[ $arg ];
		}
		return $value;
	}

}
