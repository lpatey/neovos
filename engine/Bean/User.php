<?php

require_once 'Config.php';

class Bean_User {

	private static $_user;
	private $_userId;
	private $_root;
	private $_email;
	private $_login;
	private $_password;
	private $_status;
	private $_creationTime;

	public function __construct( $data ) {
		$this->_userId = $data[ 'user_id' ];
		$this->_login = $data['login'];
		$this->_email = $data[ 'email' ];
		$this->_password = $data[ 'password' ];
		$this->_creationTime = $data['creation_time'];
		$this->_status = $data['status'];
		self::$_user = $this;
	}

	public function getUser() {
		return self::$_user;
	}

	public function getUserId() {
		return $this->_userId;
	}
	
	public function getStatus() {
		return $this->_status;
	}
	
	public function getCreationTime() {
		return $this->_creationTime;
	}

	public function getEmail() {
		return $this->_email;
	}

	public function getPassword() {
		return $this->_password;
	}
	
	public function getLang() {
		return 'en';
	}
	
	public function getLogin() {
		return $this->_login;
	}

	public function getPublicUrl() {
		return 'http://' . Config::get( 'server', 'main' ) . '/' . $this->_login;
	}

	public function getRoot() {
		if( !$this->_root ) {
			$path = Config::get( 'data', 'path' ) . '/' . $this->_login;
			$this->_root = realpath( $path );
		}
		return $this->_root;
	}

}
