<?php

require_once 'Core/Fs.php';

class Bean_Share {

	private $_name;
	private $_shareId;
	private $_owner;
	private $_password;

	public function __construct( $data, $owner ) {
		$this->_name = $data['name'];
		$this->_shareId = $data['share_id'];
		$this->_password = $data['password'];
		$this->_owner = $owner;
	}
	
	public function hasPassword() {
		return $this->_password != null;
	}
	
	public function getPassword() {
		return $this->_password;
	}

	public function getShareId() {
		return $this->_shareId;
	}

	public function getName() {
		return $this->_name;
	}
	
	public function getData() {
		$path = $this->getPath();
		return array(
			'name' => $this->getName(),
			'mime' => Core_Fs::getMimeType( $path ),
			'inode' => 1,
			'loaded' => true,
			'thumb' => false,
			'directory' => is_dir( $path )
		);
	}
	
	public function getOwner() {
		return $this->_owner;
	}
	
	public function getPath() {
		return $this->getOwner()->getRoot() . '/' . Config::get( 'fs', 'dir', 'share' ) . '/' . $this->getShareId();
	}
}
